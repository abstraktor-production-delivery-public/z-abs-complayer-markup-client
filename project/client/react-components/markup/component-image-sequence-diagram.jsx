
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ComponentImageSequenceDiagram extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderTitle(title, x) {
    if(0 !== title) {
      return (
        <text x={x} y={ComponentImageSequenceDiagram.TITLE_TOP} textAnchor="middle" style={{fontSize:ComponentImageSequenceDiagram.TITLE_FONT_SIZE,fontWeight:'bold'}}>
          {title}
        </text>
      );
    }
  }
  
  renderNodes(nodes, height, titleHeight, config) {
    const svgNodes = nodes.map((node, index) => {
      const lineNumberBias = config.lineNumbers ? ComponentImageSequenceDiagram.LINE_NUMBER_BIAS : 0;
      const x = config.widthBias + index * config.nodeWidth + lineNumberBias;
      return (
        <g key={index}>
          <text x={x} y={titleHeight + config.heightBias + ComponentImageSequenceDiagram.TEXT_BIAS_MSG_Y} style={{fontSize:'12px',fontWeight:'bold',textAnchor:'middle'}}>
            {node.name}
          </text>
          <line x1={x} y1={config.heightBias + titleHeight} x2={x} y2={config.heightBias + height + titleHeight} style={{stroke:'#666',strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
        </g>
      );
    });
    return (
      <g>
        {svgNodes}
      </g>
    );
  }
  
  renderMessageLine(message, x1, x2, y, config) {
    let xa;
    let xb;
    if('normal-bi-directional' === message.messageType || 'part-bi-directional' === message.messageType) {
      if(x1 < x2) {
        xa = x1 + ComponentImageSequenceDiagram.LINE_BIAS;
        xb = x2 - ComponentImageSequenceDiagram.LINE_BIAS;
      }
      else {
        xa = x1 - ComponentImageSequenceDiagram.LINE_BIAS;
        xb = x2 + ComponentImageSequenceDiagram.LINE_BIAS;
      }
    }
    else if('long-comment' === message.messageType) {
      if(x1 < x2) {
        xa = x1 + ComponentImageSequenceDiagram.NODE_WIDTH_HALF;
        xb = x2 - ComponentImageSequenceDiagram.NODE_WIDTH_HALF;
      }
      else {
        xa = x1 - ComponentImageSequenceDiagram.NODE_WIDTH_HALF;
        xb = x2 + ComponentImageSequenceDiagram.NODE_WIDTH_HALF;
      }
      y = y - config.nodeCommentHeight / 2 + 4;
    }
    else {
      if(x1 < x2) {
        xa = x1 + ComponentImageSequenceDiagram.NODE_WIDTH_HALF;
        xb = x2 - ComponentImageSequenceDiagram.LINE_BIAS;
      }
      else {
        xa = x1 - ComponentImageSequenceDiagram.NODE_WIDTH_HALF;
        xb = x2 + ComponentImageSequenceDiagram.LINE_BIAS;
      }
    }    
    let type = `seq_dia_protocol_${message.type}`;
    if('part' === message.messageType || 'part-bi-directional' === message.messageType) {
     type += ' seq_message_part';
    }
    return (
      <line className={type} x1={xa} y1={y} x2={xb} y2={y} style={{strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
    );
  }
  
  renderTriangleEndPoints(x1, x2, y) {
    let xa1, xa2;
    let xb1, xb2;
    if(x1 < x2) {
      xa1 = x1 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb1 = x1 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
      xa2 = x2 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb2 = x2 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    else {
      xa1 = x1 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb1 = x1 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
      xa2 = x2 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb2 = x2 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    const trinagle1 = `${xa1},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa1},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb1},${y}`;
    const trinagle2 = `${xa2},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa2},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb2},${y}`;
    return (
      <g>
        <polygon points={trinagle1}/>
        <polygon points={trinagle2}/>
      </g>
    );
  }
  
  renderTriangleEndPoint(x1, x2, y) {
    let xa;
    let xb;
    if(x1 < x2) {
      xa = x2 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    else {
      xa = x2 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    const trinagle = `${xa},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb},${y}`;
    return (
      <polygon points={trinagle}/>
    );
  }
  
  renderConnectEndPoints(message, x1, x2, y) {
    let xa;
    let xb;
    if(x1 < x2) {
      xa = x2 - ComponentImageSequenceDiagram.ARROW_CONNECTION_BASE;
      xb = x2 - ComponentImageSequenceDiagram.ARROW_CONNECTION_TOP;
    }
    else {
      xa = x2 + ComponentImageSequenceDiagram.ARROW_CONNECTION_BASE;
      xb = x2 + ComponentImageSequenceDiagram.ARROW_CONNECTION_TOP;
    }
    const trinagle = `${xa},${y - ComponentImageSequenceDiagram.ARROW_CONNECTION_HEIGHT} ${xa},${y + ComponentImageSequenceDiagram.ARROW_CONNECTION_HEIGHT} ${xb},${y}`;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <g>
        <polygon points={trinagle} />
        <circle cx={x1} cy={y} r="4"/>
        <circle cx={x2} cy={y} r="4"/>
        <circle cx={x1} cy={y} r="3" className={className} />
        <circle cx={x2} cy={y} r="3" className={className} />
      </g>
    );    
  }
  
  renderDisconnectEndPoints(x1, x2, y) {
    let xa;
    let xb;
    if(x1 < x2) {
      xa = x2 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    else {
      xa = x2 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    const trinagle = `${xa},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb},${y}`;
    const x1Plus = x1 + ComponentImageSequenceDiagram.CROSS_LENGTH;
    const x1Minus = x1 - ComponentImageSequenceDiagram.CROSS_LENGTH;
    const x2Plus = x2 + ComponentImageSequenceDiagram.CROSS_LENGTH;
    const x2Minus = x2 - ComponentImageSequenceDiagram.CROSS_LENGTH;
    const yPlus = y + ComponentImageSequenceDiagram.CROSS_LENGTH;
    const yMinus = y - ComponentImageSequenceDiagram.CROSS_LENGTH;
    return (
      <g>
        <polygon points={trinagle} />
        <line x1={x1Minus} y1={yPlus} x2={x1Plus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.DISCONNECT_WIDTH}} />
        <line x1={x1Plus} y1={yPlus} x2={x1Minus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.DISCONNECT_WIDTH}} />
        <line x1={x2Minus} y1={yPlus} x2={x2Plus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.DISCONNECT_WIDTH}} />
        <line x1={x2Plus} y1={yPlus} x2={x2Minus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.DISCONNECT_WIDTH}} />
      </g>
    );    
  }
  
  renderStartEndPoint(message, x, y) {
    const ya = y;
    const xa = x - ComponentImageSequenceDiagram.TRIANGLE_BASE + ComponentImageSequenceDiagram.TRIANGLE_BASE_2_3;
    const xb = x - ComponentImageSequenceDiagram.TRIANGLE_TOP + ComponentImageSequenceDiagram.TRIANGLE_BASE_2_3;
    const trinagle = `${xa},${ya - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa},${ya + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb},${ya}`;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <g>
        <polygon className={className} points={trinagle} />
      </g>
    );
  }
  
  renderStopEndPoint(message, x, y) {
    const length = 8;
    const lengthHalf = length / 2;
    const xa = x - lengthHalf;
    const ya = y - lengthHalf;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <g>
        <rect className={className} x={xa} y={ya} width={length} height={length}/>
      </g>
    );
  }
  
  renderPauseEndPoint(message, x, y) {
    const length = 8;
    const lengthHalf = length / 2;
    const xa = x - lengthHalf;
    const ya = y - lengthHalf;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_empty`;
    const classNamePause = `markup_seq_end_point seq_dia_protocol_${message.type}_pause`;
    return (
      <g>
        <rect className={className} x={xa} y={ya} width={length} height={length}/>
        <line className={classNamePause} x1={xa+2} x2={xa+2} y1={ya} y2={ya+8} />
        <line className={classNamePause} x1={xa+6} x2={xa+6} y1={ya} y2={ya+8} />
      </g>
    );
  }
  
  renderNewEndPoint(message, x, y) {
    return (
      <g>
        <circle cx={x} cy={y} r="4" style={{stroke:'Black',strokeWidth:1,fill:'White'}} />
      </g>
    );
  }
  
  renderDeleteEndPoint(message, x, y) {
    const length = 4;
    const lengthHalf = length / 2;
    const xa = x - lengthHalf;
    const xb = x + lengthHalf;
    const ya = y - lengthHalf;
    const yb = y + lengthHalf;
    return (
      <g>
        <circle cx={x} cy={y} r="4" style={{stroke:'Black',strokeWidth:1,fill:'White'}} />
        <line x1={xa} x2={xb} y1={yb} y2={ya} style={{stroke:'Black',strokeWidth:1}}/>
        <line x1={xa} x2={xb} y1={ya} y2={yb} style={{stroke:'Black',strokeWidth:1}}/>
      </g>
    );
  }
  
  renderCommentEndPoint(message, x, y) {
    const length = 6;
    const slope = 1;
    const lengthHalf = length / 2;
    const xa = x - lengthHalf;
    const xb = x + lengthHalf;
    const ya = y - lengthHalf;
    const yb = y + lengthHalf;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <g>
        <circle cx={x} cy={y} r="4" style={{stroke:'Black',strokeWidth:1,fill:'White'}} />
        <line x1={xa-1+slope} x2={xb-1-slope} y1={yb} y2={ya} style={{stroke:'Black',strokeWidth:1}}/>
        <line x1={xa+1+slope} x2={xb+1-slope} y1={yb} y2={ya} style={{stroke:'Black',strokeWidth:1}}/>
      </g>
    );
  }
  
  renderVerifyEndPoint(message, x, y) {
    const ax1 = x;
    const ax2 = x - 2;
    const ay1 = y + 2;
    const ay2 = y;
    const bx1 = x - 0.5;
    const bx2 = x + 2;
    const by1 = y + 2;
    const by2 = y - 2;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <g>
        <circle cx={x} cy={y} r="4" style={{stroke:'Green',strokeWidth:1,fill:'#d0e9c6'}} />
        <line x1={ax1} x2={ax2} y1={ay1} y2={ay2} style={{stroke:'Green',strokeWidth:1}}/>
        <line x1={bx1} x2={bx2} y1={by1} y2={by2} style={{stroke:'Green',strokeWidth:1}}/>
      </g>
    );
  }
  
  renderNewBrowserEndPoint(message, x, y) {
    const r = 2;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <>
        <g transform={`translate(${x-5} ${y-4}) scale(0.1 0.1)`}>
          <path className="gui_seq_dia_browser_svg" d="M 84.24 34.662 c 5.993 -13.379 7.386 -25.143 3.819 -32.273 c -0.097 -0.193 -0.254 -0.35 -0.447 -0.447 C 80.481 -1.626 68.717 -0.234 55.337 5.76 c -0.018 0.008 -0.033 0.021 -0.05 0.03 c -6.605 2.964 -13.045 6.812 -18.59 11.048 l -12.576 0.306 c -1.434 0.035 -2.838 0.449 -4.061 1.197 L 2.531 29.06 c -1.463 0.894 -2.359 2.452 -2.397 4.167 c -0.038 1.715 0.79 3.311 2.212 4.268 l 13.132 8.839 l -2.22 10.701 c -0.068 0.33 0.034 0.672 0.272 0.91 L 32.056 76.47 c 0.189 0.189 0.444 0.293 0.707 0.293 c 0.067 0 0.136 -0.007 0.203 -0.021 l 10.693 -2.218 l 8.817 13.101 c 0.937 1.392 2.484 2.213 4.157 2.213 c 0.036 0 0.074 0 0.111 -0.001 c 1.714 -0.037 3.273 -0.934 4.168 -2.397 l 10.717 -17.53 c 0.748 -1.221 1.161 -2.625 1.197 -4.061 l 0.305 -12.506 z" />
          <line className="gui_seq_dia_browser_fuel" x1="20" x2="0" y1="58" y2="78" />
          <line className="gui_seq_dia_browser_fuel" x1="26" x2="0" y1="64" y2="90" />
          <line className="gui_seq_dia_browser_fuel" x1="32" x2="12" y1="70" y2="90" />
        </g>
        <circle className={`seq_dia_protocol_${message.type}_small`} cx={x} cy={y} r={r} style={{stroke:'black',strokeWidth:0.5}} />
      </>
    );
  }
  
  renderPageEndPoint(message, x, y) {
    const length = 6;
    const lengthHalf = length / 2;
    x = x - lengthHalf - 2;
    y = y - lengthHalf - 2;
    return (
      <g transform={`translate(${x} ${y}) scale(0.02 0.02)`}>
        <path className="gui_seq_dia_page" fill="#8fbcef" d="m285.37 85.136 1.3221-76.683 101.47 101.8-80.98-1.3221z" />
        <path fill="White" d="m5.742 26.302 18.84-16.527 262.11-1.3221-1.3221 76.683 21.815 23.798 80.98 1.3221-0.99159 368.21-17.518 18.675-338.79 4.1316-23.137-20.823z" />
        <path className="gui_seq_dia_page" fill="#227ce0" d="m398.96 110.75c-0.018-0.185-0.05-0.365-0.081-0.545-0.011-0.06-0.016-0.122-0.028-0.182-0.043-0.215-0.098-0.425-0.159-0.632-7e-3 -0.025-0.012-0.052-0.02-0.077-0.065-0.213-0.141-0.421-0.224-0.625-8e-3 -0.021-0.015-0.043-0.023-0.064-0.081-0.195-0.173-0.384-0.269-0.57-0.016-0.031-0.029-0.063-0.045-0.094-0.093-0.173-0.196-0.339-0.301-0.504-0.027-0.042-0.05-0.086-0.077-0.127-0.103-0.154-0.216-0.3-0.33-0.446-0.037-0.048-0.07-0.098-0.109-0.145-0.142-0.173-0.294-0.338-0.45-0.498-0.015-0.015-0.027-0.031-0.042-0.046l-104-104c-0.018-0.018-0.038-0.033-0.057-0.051-0.156-0.153-0.317-0.301-0.486-0.44-0.055-0.045-0.113-0.083-0.169-0.126-0.138-0.107-0.275-0.214-0.42-0.311-0.051-0.034-0.105-0.062-0.156-0.095-0.156-0.099-0.312-0.197-0.475-0.284-0.036-0.019-0.074-0.035-0.111-0.053-0.181-0.093-0.365-0.183-0.554-0.262-0.024-0.01-0.049-0.017-0.074-0.027-0.202-0.081-0.406-0.157-0.616-0.221-0.027-8e-3 -0.054-0.013-0.081-0.021-0.206-0.06-0.415-0.115-0.628-0.158-0.063-0.013-0.128-0.018-0.192-0.029-0.177-0.031-0.354-0.062-0.536-0.08-0.248-0.025-0.498-0.038-0.749-0.038h-248c-21.78 0-39.5 17.72-39.5 39.5v432c0 21.78 17.72 39.5 39.5 39.5h320c21.78 0 39.5-17.72 39.5-39.5v-360c0-0.251-0.013-0.501-0.038-0.749zm-103.96-85.145 78.394 78.394h-53.894c-13.509 0-24.5-10.99-24.5-24.5zm64.5 470.39h-320c-13.509 0-24.5-10.99-24.5-24.5v-432c0-13.51 10.991-24.5 24.5-24.5h240.5v64.5c0 21.78 17.72 39.5 39.5 39.5h64.5v352.5c0 13.51-10.991 24.5-24.5 24.5z" />
        <g transform="translate(-55.998 .001)">
          <path d="m210.79 328.21v33.164l-117.89-51.094v-28.359l117.89-51.094v33.398l-82.148 31.641z" />
          <path d="m283.68 230.12-32.812 135.12q-1.6406 6.6797-2.9297 10.547-1.1719 3.8672-3.75 5.9766-2.4609 2.2266-7.2656 2.2266-11.953 0-11.953-10.312 0-2.6953 2.3438-13.477l32.695-135.12q2.5781-10.781 4.9219-14.766 2.3438-3.9844 9.1406-3.9844 5.8594 0 8.9062 2.8125 3.1641 2.8125 3.1641 7.7344 0 3.6328-2.4609 13.242z" />
          <path d="m418.1 310.28-117.89 51.328v-33.164l82.383-32.344-82.383-31.875v-32.93l117.89 50.859z" />
        </g>
      </g>
    );
  }
  
  renderClickEndPoint(message, x, y) {
    const r = 2.4;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <>
        <circle className={`seq_dia_protocol_${message.type}_small`} cx={x} cy={y} r={r} style={{stroke:'black',strokeWidth:0.5}} />
        <g transform={`translate(${x - 5} ${y - 7.5}) scale(0.13 0.13)`}>
          <path style={{stroke:'Black',strokeWidth:4,strokeDasharray:'none',strokeLinecap:'butt',strokeLinejoin:'miter',strokeMiterlimit:10,fill:'White',fillRule:'nonzero',opacity:1,strokeLinecap:'round',transform:'rotate(45deg)'}} d="M 66.437 24.873 c 0.382 -1.923 -0.02 -3.815 -1.129 -5.329 c -1.108 -1.514 -2.791 -2.467 -4.739 -2.685 c -1.918 -0.215 -3.853 0.314 -5.455 1.487 l -32.62 23.888 l 0.161 -8.456 c 0.036 -1.889 -0.682 -3.638 -2.023 -4.924 c -1.388 -1.332 -3.25 -1.999 -5.258 -1.878 c -4.003 0.242 -7.319 3.62 -7.394 7.529 L 7.399 65.128 C 7.276 70.599 8.867 75.753 12 80.031 c 4.799 6.553 12.408 9.97 20.359 9.969 c 5.88 0 11.948 -1.868 17.205 -5.718 l 13.652 -9.998 c 3.344 -2.45 4.212 -6.972 1.935 -10.081 c -1.108 -1.513 -2.791 -2.467 -4.739 -2.684 c -0.013 -0.001 -0.025 -0.001 -0.038 -0.002 l 0.359 -0.263 c 1.601 -1.173 2.69 -2.86 3.064 -4.751 c 0.382 -1.922 -0.02 -3.816 -1.129 -5.329 c -1.108 -1.513 -2.791 -2.467 -4.739 -2.685 c -0.351 -0.04 -0.702 -0.046 -1.052 -0.036 c 0.446 -0.746 0.771 -1.56 0.942 -2.42 c 0.381 -1.922 -0.021 -3.815 -1.129 -5.329 c -1.154 -1.576 -2.899 -2.488 -4.774 -2.689 l 11.458 -8.391 C 64.974 28.451 66.063 26.764 66.437 24.873" />
        </g>
      </>
    );
  }
  
  renderTypeEndPoint(message, x, y) {
    const r = 2.4;
    const className = `markup_seq_end_point seq_dia_protocol_${message.type}_small`;
    return (
      <>
        <line x1={x-8} x2={x} y1={y} y2={y} style={{stroke:'black',strokeWidth:0.5}}/>
        <circle className={`seq_dia_protocol_${message.type}_small`} cx={x} cy={y} r={r} style={{stroke:'black',strokeWidth:0.5}} />
        <g transform={`translate(${x-16} ${y-6}) scale(0.02 0.02)`}>
          <rect className="gui_seq_dia_type" x="0" width="480" rx="2" y="200" height="200" ry="2" />
          <path d="M251.2,193.5v-53.7c0-5.8,4.7-10.5,10.5-10.5h119.4c21,0,38.1-17.1,38.1-38.1s-17.1-38.1-38.1-38.1H129.5 c-5.4,0-10.1,4.3-10.1,10.1c0,5.8,4.3,10.1,10.1,10.1h251.6c10.1,0,17.9,8.2,17.9,17.9c0,10.1-8.2,17.9-17.9,17.9H261.7 c-16.7,0-30.3,13.6-30.3,30.3v53.3H0v244.2h490V193.5H251.2z M232.2,221.5h15.6c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1 h-15.6c-5.4,0-10.1-4.3-10.1-10.1C222.1,225.8,226.7,221.5,232.2,221.5z M203.4,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C213.5,321,208.8,325.7,203.4,325.7z M213.5,352.9 c0,5.4-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6C208.8,342.8,213.5,347.5,213.5,352.9z M203.4,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C213.5,283.7,208.8,288,203.4,288z M186.3,221.5h15.6c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1h-15.6 c-5.4,0-10.1-4.3-10.1-10.1S180.8,221.5,186.3,221.5z M140.4,221.5H156c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1h-15.6 c-5.4,0-10.1-4.3-10.1-10.1C130.3,225.8,134.9,221.5,140.4,221.5z M138.8,268.1h15.6c5.4,0,10.1,4.3,10.1,10.1 c0,5.8-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1C128.7,272.4,133.4,268.1,138.8,268.1z M138.8,305.5h15.6 c5.4,0,10.1,4.3,10.1,10.1c0,5.4-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1C128.7,310.1,133.4,305.5,138.8,305.5z M138.8,342.8h15.6c5.4,0,10.1,4.3,10.1,10.1c0,5.4-4.3,10.1-10.1,10.1h-15.6c-5.4,0-10.1-4.3-10.1-10.1 C128.7,347.5,133.4,342.8,138.8,342.8z M94.5,221.5h15.6c5.4,0,10.1,4.3,10.1,10.1s-4.3,10.1-10.1,10.1H94.5 c-5.4,0-10.1-4.3-10.1-10.1S89.1,221.5,94.5,221.5z M89.4,268.1H105c5.4,0,10.1,4.3,10.1,10.1c0,5.8-4.3,10.1-10.1,10.1H89.4 c-5.4,0-10.1-4.3-10.1-10.1C79.3,272.4,84,268.1,89.4,268.1z M89.4,305.5H105c5.4,0,10.1,4.3,10.1,10.1c0,5.4-4.3,10.1-10.1,10.1 H89.4c-5.4,0-10.1-4.3-10.1-10.1C79.7,310.1,84,305.5,89.4,305.5z M89.4,342.8H105c5.4,0,10.1,4.3,10.1,10.1 c0,5.4-4.3,10.1-10.1,10.1H89.4c-5.4,0-10.1-4.3-10.1-10.1C79.7,347.5,84,342.8,89.4,342.8z M56,400.4H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C65.7,395.7,61.4,400.4,56,400.4z M56,363H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C65.7,358.4,61.4,363,56,363z M56,325.7H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C65.7,321,61.4,325.7,56,325.7z M56,288H40.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1C66.1,283.7,61.4,288,56,288z M56,241.3H40.4 c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1H56c5.4,0,10.1,4.3,10.1,10.1S61.4,241.3,56,241.3z M252.8,400.4H89.4 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h163.3c5.4,0,10.1,4.3,10.1,10.1C262.9,395.7,258.2,400.4,252.8,400.4z M252.8,363h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C262.9,358.4,258.2,363,252.8,363z M252.8,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C262.9,321,258.2,325.7,252.8,325.7z M252.8,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C262.9,283.7,258.2,288,252.8,288z M302.2,400.4h-15.6 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C311.9,395.7,307.6,400.4,302.2,400.4z M302.2,363h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C311.9,358.4,307.6,363,302.2,363z M302.2,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C311.9,321,307.6,325.7,302.2,325.7z M302.2,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C312.3,283.7,307.6,288,302.2,288z M312.3,241.3h-15.6 c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1S317.7,241.3,312.3,241.3z M351.2,400.4h-15.6 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C361.3,395.7,356.6,400.4,351.2,400.4z M351.2,363h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C361.3,358.4,356.6,363,351.2,363z M351.2,325.7h-15.6c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C361.3,321,356.6,325.7,351.2,325.7z M351.2,288h-15.6c-5.4,0-10.1-4.3-10.1-10.1 c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C361.3,283.7,356.6,288,351.2,288z M357.8,241.3h-15.6 c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1S363.6,241.3,357.8,241.3z M400.6,400.4H385 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C410.3,395.7,406,400.4,400.6,400.4z M400.6,363H385c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C410.3,358.4,406,363,400.6,363z M400.6,325.7H385c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6 c5.4,0,10.1,4.3,10.1,10.1C410.3,321,406,325.7,400.6,325.7z M400.6,288H385c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1 h15.6c5.4,0,10.1,4.3,10.1,10.1C410.7,283.7,406,288,400.6,288z M403.7,241.3h-15.6c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1 h15.6c5.4,0,10.1,4.3,10.1,10.1C413.8,237,409.5,241.3,403.7,241.3z M449.6,400.4H434c-5.4,0-10.1-4.3-10.1-10.1 c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C459.7,395.7,455,400.4,449.6,400.4z M449.6,363H434 c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C459.7,358.4,455,363,449.6,363z M449.6,325.7 H434c-5.4,0-10.1-4.3-10.1-10.1c0-5.4,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1C459.7,321,455,325.7,449.6,325.7z M449.6,288H434c-5.4,0-10.1-4.3-10.1-10.1c0-5.8,4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 C459.7,283.7,455,288,449.6,288z M449.6,241.3H434c-5.4,0-10.1-4.3-10.1-10.1s4.3-10.1,10.1-10.1h15.6c5.4,0,10.1,4.3,10.1,10.1 S455,241.3,449.6,241.3z" />
        </g>
      </>
    );
  }
  
  renderMessageEndPoints(message, x1, x2, y) {
    if('normal' === message.messageType || 'part' === message.messageType) {
      return this.renderTriangleEndPoint(x1, x2, y);
    }
    else if('normal-bi-directional' === message.messageType || 'part-bi-directional' === message.messageType) {
      return this.renderTriangleEndPoints(x1, x2, y);
    }
    else if('connect' === message.messageType) {
      return this.renderConnectEndPoints(message, x1, x2, y);
    }
    else if('disconnect' === message.messageType) {
      return this.renderDisconnectEndPoints(x1, x2, y);
    }
  }
  
  renderMessageEndPoint(message, x, y) {
    if('start' === message.messageType) {
      return this.renderStartEndPoint(message, x, y);
    }
    else if('stop' === message.messageType) {
      return this.renderStopEndPoint(message, x, y);
    }
    else if('pause' === message.messageType) {
      return this.renderPauseEndPoint(message, x, y);
    }
    else if('new' === message.messageType) {
      return this.renderNewEndPoint(message, x, y);
    }
    else if('delete' === message.messageType) {
      return this.renderDeleteEndPoint(message, x, y);
    }
    else if('comment' === message.messageType) {
      return this.renderCommentEndPoint(message, x, y);
    }
    else if('verify' === message.messageType) {
      return this.renderVerifyEndPoint(message, x, y);
    }
    else if('browser' === message.messageType) {
      return this.renderNewBrowserEndPoint(message, x, y);
    }
    else if('page' === message.messageType) {
      return this.renderPageEndPoint(message, x, y);
    }
    else if('click' === message.messageType) {
      return this.renderClickEndPoint(message, x, y);
    }
    else if('type' === message.messageType) {
      return this.renderTypeEndPoint(message, x, y);
    }
  }
  
  renderMessageEventData(message, x1, y, config) {
    let x = x1 - ComponentImageSequenceDiagram.TEXT_EVENT_BIAS_X;
    let infoText = message.data ? message.data : null;
    let text = '';
    if('verify' === message.messageType) {
      text = 'VERIFY';
    }
    if('browser' === message.messageType) {
      text = 'browser';
    }
    else if('page' === message.messageType) {
      text = 'page';
    }
    else if('click' === message.messageType) {
      text = 'click';
      x -= 6;
    }
    else if('type' === message.messageType) {
      text = 'type';
      x -= 6;
    }
    const anchor = 'end';
    return (
      <>
        <text x={x} y={y + (config.nodeEventHeight / 2) + ComponentImageSequenceDiagram.TEXT_BIAS_EVENT_Y} style={{fontSize:'10px',fontWeight:'bold',textAnchor:anchor}}>
          {text}
        </text>
        <text x={x1 + ComponentImageSequenceDiagram.TEXT_EVENT_BIAS_X} y={y + (config.nodeEventHeight / 2) + ComponentImageSequenceDiagram.TEXT_BIAS_EVENT_Y} style={{fontSize:'10px',fontWeight:'bold'}}>
          {infoText}
        </text>
      </>
    );
  }
  
  renderMessageData(message, x1, x2, y, config) {
    if('long-comment' === message.messageType) {
      let xa = 0;
      let xb = 0;
      let width = 0;
      let textX = 0;
      const TextY = y - config.nodeCommentHeight / 2 + ComponentImageSequenceDiagram.TEXT_BIAS_COMMENT_Y;
      y = y - config.nodeCommentHeight + ComponentImageSequenceDiagram.COMMENT_BIAS_Y;
      if(x1 < x2) {
        xa = x1 + ComponentImageSequenceDiagram.COMMENT_BIAS_X;
        xb = x2 - ComponentImageSequenceDiagram.COMMENT_BIAS_X;
        width = xb - xa;
        textX = xa + width / 2;
      }
      else {
        xa = x1 - ComponentImageSequenceDiagram.COMMENT_BIAS_X;
        xb = x2 + ComponentImageSequenceDiagram.COMMENT_BIAS_X;
        width = xa - xb;
        textX = xb + width / 2;
      }
      return (
        <g>
          <rect className={`seq_dia_protocol_${message.type}_dimond`} x={xa} y={y} width={width} height={config.nodeCommentHeight - 4}/>
          <text x={textX} y={TextY} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'middle'}}>
            {message.data}
          </text>
        </g>
      );
    }
    else {
      let x = 0;
      let anchor = '';
      if(x1 < x2) {
        x = x1 + ComponentImageSequenceDiagram.TEXT_MESSAGE_BIAS_X;
        anchor = 'start';
      }
      else {
        x = x1 - ComponentImageSequenceDiagram.TEXT_MESSAGE_BIAS_X;
        anchor = 'end';
      }
      return (        
        <text x={x} y={y + ComponentImageSequenceDiagram.TEXT_BIAS_MSG_Y} style={{fontSize:'10px',fontWeight:'bold',textAnchor:anchor}}>
          {message.data}
        </text>
      );
    }
  }
  
  renderMessages(messages, titleHeight, config) {
    let y = titleHeight + config.heightBias + config.nodeEventHeight;
    const lineNumberBias = config.lineNumbers ? ComponentImageSequenceDiagram.LINE_NUMBER_BIAS : 0;
    const svgMessages = messages.map((message, index) => {
      if(2 === message.nodes) {
        const x1 = config.widthBias + message.fromIndex * config.nodeWidth + lineNumberBias;
        const x2 = config.widthBias + message.toIndex * config.nodeWidth + lineNumberBias;
        if('msg' === message.height) {
          y += config.nodeMessageHeight;
        }
        else if('event' === message.height) {
          y += config.nodeEventHeight;
        }
        else if('comment' === message.height) {
          y += config.nodeCommentHeight;
        }
        return (
          <g key={index}>
            {this.renderMessageLine(message, x1, x2, y, config)}
            {this.renderMessageData(message, x1, x2, y, config)}
            {this.renderMessageEndPoints(message, x1, x2, y)}
          </g>
        );
      }
      else if(1 === message.nodes) {
        const x1 = config.widthBias + message.fromIndex * config.nodeWidth + lineNumberBias;
        if('msg' === message.height) {
          y += config.nodeMessageHeight;
        }
        else if('event' === message.height) {
          y += config.nodeEventHeight;
        }
        return (
          <g key={index}>
            {this.renderMessageEventData(message, x1, y, config)}
            {this.renderMessageEndPoint(message, x1, y)}
          </g>
        );
      }
    });
    return (
      <g>
        {svgMessages}
      </g>
    );
  }
  
  renderLineNumbers(messages, nodeHeight, config) {
    if(config.lineNumbers) {
      const lineNumberBias = config.lineNumbers ? ComponentImageSequenceDiagram.LINE_NUMBER_BIAS : 0;
      const x = config.widthBias;
      const y1 = config.heightBias;
      const y2 = config.heightBias + nodeHeight;
      let yText = y1 + config.nodeEventHeight;
      let yHalfExtra = 0;
      const lineNumbers = messages.map((message, index) => {
        if('msg' === message.height) {
          yText += config.nodeMessageHeight;
          yHalfExtra = config.nodeMessageHeight / 2 - 6;
        }
        else if('event' === message.height) {
          yText += config.nodeEventHeight;
          yHalfExtra = config.nodeEventHeight / 2;
        }
        else if('comment' === message.height) {
          yText += config.nodeCommentHeight;
          yHalfExtra = config.nodeCommentHeight / 2 - 6;
        }
        return (
          <text key={index} x={x - 4} y={yText + yHalfExtra} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'end'}}>{index + 1}</text>
        );
      });
      return (
        <g>
          <line x1={x} x2={x} y1={y1} y2={y2} style={{stroke:'Black'}}/>
          {lineNumbers}
        </g>
      );
    }
  }

  calculateNodeHeight(messages, config) {
    let y = config.heightBias;
    messages.forEach((message) => {
      if('msg' === message.height) {
        y += config.nodeMessageHeight;
      }
      else if('event' === message.height) {
        y += config.nodeEventHeight;
      }
      else if('comment' === message.height) {
        y += config.nodeCommentHeight;
      }
    });
    return y;
  }
  
  render() {
    const title = this.props.sequenceDiagram.title;
    const nodes = this.props.sequenceDiagram.nodes;
    const messages = this.props.sequenceDiagram.messages;
    const config = this.props.sequenceDiagram.dataSequenceDiagramConfig;
    const titleHeight = ('' !== title && undefined !== title) ? ComponentImageSequenceDiagram.TITLE_HEIGHT : 0;
    const nodeHeight = this.calculateNodeHeight(messages, config);
    const svgHeight = titleHeight + nodeHeight + config.nodeMessageHeight + config.heightBias;
    const lineNumberBias = config.lineNumbers ? ComponentImageSequenceDiagram.LINE_NUMBER_BIAS : 0;
    const svgWidth = 2 * config.widthBias + config.nodeWidth * (nodes.length - 1) + lineNumberBias;
    const viewBox = [0, 0, svgWidth, svgHeight].join(' ');
    const style = {};
    if(!config.border) {
      style.border = '0px';
    }
    if('default' !== config.backgroundColor) {
      style.backgroundColor = config.backgroundColor;
    }
    return (
      <svg className="markup_seq" width={svgWidth} height={svgHeight} viewBox={viewBox} style={style}>
        {this.renderTitle(title, svgWidth / 2)}
        {this.renderNodes(nodes, nodeHeight, titleHeight, config)}
        {this.renderMessages(messages, titleHeight, config)}
        {this.renderLineNumbers(messages, nodeHeight, config)}
      </svg>
    );
  }
  
  /*_networkPopover(virtualNetwork) {
    let popup = [['name:', virtualNetwork.name], ['family:', virtualNetwork.family], ['subnet:', virtualNetwork.subnet]];
    if(virtualNetwork.valid) {
      if(virtualNetwork.reduced) {
        popup.push(['reduced', '']);
      }
    }
    else {
      popup.push(['not valid', '']);
    }
    return popup;
  }*/
}

ComponentImageSequenceDiagram.TITLE_HEIGHT = 30;
ComponentImageSequenceDiagram.TITLE_FONT_SIZE = 20;
ComponentImageSequenceDiagram.TITLE_TOP = 26;
ComponentImageSequenceDiagram.NODE_WIDTH = 3;
ComponentImageSequenceDiagram.NODE_WIDTH_HALF = ComponentImageSequenceDiagram.NODE_WIDTH / 2;
ComponentImageSequenceDiagram.DISCONNECT_WIDTH = 2;
ComponentImageSequenceDiagram.TEXT_EVENT_BIAS_X = 10;
ComponentImageSequenceDiagram.TEXT_MESSAGE_BIAS_X = 12;
ComponentImageSequenceDiagram.TEXT_BIAS_EVENT_Y = -2;
ComponentImageSequenceDiagram.TEXT_BIAS_MSG_Y = -4;
ComponentImageSequenceDiagram.TEXT_BIAS_COMMENT_Y = 7.5;
ComponentImageSequenceDiagram.COMMENT_BIAS_X = 20;
ComponentImageSequenceDiagram.COMMENT_BIAS_Y = 6;
ComponentImageSequenceDiagram.TRIANGLE_BASE = 12;
ComponentImageSequenceDiagram.TRIANGLE_BASE_2_3 = ComponentImageSequenceDiagram.TRIANGLE_BASE * 2 / 3;
ComponentImageSequenceDiagram.TRIANGLE_TOP = (ComponentImageSequenceDiagram.NODE_WIDTH - 1) / 2;
ComponentImageSequenceDiagram.TRIANGLE_HEIGHT = 4;
ComponentImageSequenceDiagram.ARROW_CONNECTION_BASE = 12;
ComponentImageSequenceDiagram.ARROW_CONNECTION_TOP = (ComponentImageSequenceDiagram.NODE_WIDTH - 1) / 2;
ComponentImageSequenceDiagram.ARROW_CONNECTION_HEIGHT = 3;
ComponentImageSequenceDiagram.LINE_BIAS = ComponentImageSequenceDiagram.TRIANGLE_BASE - ComponentImageSequenceDiagram.TRIANGLE_TOP;
ComponentImageSequenceDiagram.CROSS_LENGTH = 5;
ComponentImageSequenceDiagram.LINE_NUMBER_BIAS = 90;
