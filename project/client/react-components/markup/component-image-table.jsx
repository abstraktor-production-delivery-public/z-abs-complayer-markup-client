
'use strict';

import ComponetMarkupRemarkable from './component-markup-remarkable';
import MarkupDocumentationPage from '../../markup/markup-documentation/markup-documentation-page';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ComponentImageTable extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderTableRow(row, index) {
    const values = row.columns.map((column, cIndex) => {
      const document = MarkupDocumentationPage.parse(column);
      let formattedColumn = '';
      document.forEach((doc) => {
        if('markup_ref' === doc.type) {
          const link = this.props.linkReferences.get(doc.value.refName);
          if(undefined !== link) {
            formattedColumn += `[${'' !== doc.value.text ? doc.value.text : link.text}](${link.path}:::${link.type})`;
          }
        }
        else {
          formattedColumn += doc.value;
        }
      }); 
      return (
        <td key={cIndex}>
          <ComponetMarkupRemarkable inner preview={this.props.preview} value={formattedColumn} />
        </td>
      );
    });
    return (
      <tr key={index}>
        {values}
      </tr>  
    );
  }
  
  renderTableRows(rows) {
    const values = [];
    for(let i = 2; i < rows.length; ++i) {
      values.push(this.renderTableRow(rows[i], i));
    }
    return (
      <tbody>
        {values}
      </tbody>
    );
  }
  
  renderTableHeading(rows) {
    if(2 <= rows.length) {
      const headings = rows[1].columns.map((heading, index) => {
        return (<th key={index}>{heading}</th>);
      });
      return (
        <thead>
          <tr>
            {headings}
          </tr>  
        </thead>  
      );
    }
  }
  
  renderTable() {
    return this.props.values.objects.map((rows, index) => {
      const row0 = rows[0];
      const name = 1 <= row0.columns.length ? row0.columns[0] : '';
      const classHeading = this.props.values.config.classHeading ? ` ${this.props.values.config.classHeading}` : '';
      return (
        <div key={index} className={this.props.classTable}>
          <div className={`panel-heading ${this.props.classHeading}${classHeading}`}>{name}</div>
          <table className={`${this.props.classRow} table table-bordered table-striped table-condensed table-hover`}>
            {this.renderTableHeading(rows)}
            {this.renderTableRows(rows)}
          </table>
        </div>
      );
    });
  }
  
  render() {
    return this.renderTable();
  }
}

module.exports = ComponentImageTable;
