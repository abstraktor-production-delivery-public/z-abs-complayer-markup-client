
'use strict';

import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ComponentTableDataTable extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.showAlways, nextProps.showAlways)
      || !this.shallowCompare(this.props.classTable, nextProps.classTable)
      || !this.shallowCompare(this.props.classHeading, nextProps.classHeading)
      || !this.shallowCompare(this.props.classRow, nextProps.classRow)
      || !this.shallowCompare(this.props.dataTable, nextProps.dataTable)
      || !this.shallowCompare(this.props.filter, nextProps.filter)
      || !this.deepCompare(this.props.values, nextProps.values)
      || !this.shallowCompare(this.props.stage, nextProps.stage)
      || !this.shallowCompare(this.props.filterData, nextProps.filterData)
      || !this.shallowCompare(this.props.preview, nextProps.preview)
      || !this.shallowCompare(this.props.hideColumsIfNotUsed, nextProps.hideColumsIfNotUsed);
  }
  
  renderLink(analyzedValue) {
    if(!this.props.preview) {
      return (
        <Link className="markup_table_link" global href={analyzedValue.link}>
          {analyzedValue.value}
        </Link>
      );
    }
    else {
      return (
        <a className="doc_nav_preview">
          {analyzedValue.value}
        </a>
      );
    }
  }
  
  renderTableRowValue(markupTableHeading, value, index) {
    const analyzedValue = this.props.dataTable.getValue(markupTableHeading, value);
    if(!analyzedValue.link) {
      return (
        <td key={index} style={analyzedValue.style}>
          {analyzedValue.value}
        </td>
      );
    }
    else {
      return (
        <td key={index} style={analyzedValue.style}>
          {this.renderLink(analyzedValue)}
        </td>
      );
    }
  }
  
  renderTableRow(markupTableHeadings, value, index, hideSet) {
    if(this.props.filter && (!this.props.stage || !this.props.filter(value, this.props.stage, this.props.filterData))) {
      return;
    }
    const values = markupTableHeadings.map((heading, index) => {
      if(hideSet.has(heading)) {
        return null;
      }
      else {
        return this.renderTableRowValue(heading, value, index);
      }
    });
    const className = value._commentOut_ ? 'markup_table_comment_out' : undefined;
    return (
      <tr key={index} className={className}>
        <td>{index + 1}</td>
        {values}
      </tr>  
    );
  }
    
  renderTableRows(markupTableHeadings, hideSet) {
    if(undefined !== this.props.values) {
      const values = this.props.values.map((value, index) => {
        return this.renderTableRow(markupTableHeadings, this.deepCopy(value), index, hideSet);
      });
      return (
        <tbody>
          {values}
        </tbody>
      );
    }
  }

  renderTableHeadings(markupTableHeadings, hideSet) {
    const headings = markupTableHeadings.map((heading, index) => {
      if(hideSet.has(heading)) {
        return null;
      }
      else {
        return (<th key={index}>{heading}</th>);
      }
    });
    return (
      <tr>
        <th>#</th>
        {headings}
      </tr>  
    );
  }
  
  render() {
    if(this.props.values && (0 < this.props.values.length || this.props.hasOwnProperty('showAlways'))) {
      const markupTableName = this.props.dataTable.getName();
      const markupTableHeadings = this.props.dataTable.getHeadings();
      const hideSet = new Set();
      if(this.props.hideColumsIfNotUsed?.length) {
        this.props.hideColumsIfNotUsed.forEach((columnName) => {
          let colomnEmpty = true;
          this.props.values.forEach((row) => {
            colomnEmpty &&= !Reflect.get(row, columnName);
          });
          if(colomnEmpty) {
            hideSet.add(columnName);
          }
        });
      }
      return (
        <div className={this.props.classTable}>
          <div className={`panel-heading ${this.props.classHeading}`}>{markupTableName}</div>
          <table className={`${this.props.classRow} table table-bordered table-striped table-condensed table-hover`}>
            <thead>
              {this.renderTableHeadings(markupTableHeadings, hideSet)}
            </thead>
            {this.renderTableRows(markupTableHeadings, hideSet)}
          </table>
        </div>
      );
    }
    else {
      return null;
    }
  }
}
