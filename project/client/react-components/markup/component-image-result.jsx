
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ComponentImageResult extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  render() {
    return <div>this.props.values</div>;
  }
}

module.exports = ComponentImageResult;
