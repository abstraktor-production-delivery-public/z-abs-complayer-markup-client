
'use strict';

import ComponentImageChartLine from './component-chart/component-image-chart-line';
import ComponentImageImage from './component-image-image';
import ComponentImageLab from './component-image-lab';
import ComponentImageSequenceDiagram from './component-image-sequence-diagram';
import ComponentImageFlowchart from './component-image-flowchart';
import ComponentImageNodeDiagram from './component-image-node-diagram';
import ComponentImageStateMachine from './component-image-state-machine';
import ComponentImageTable from './component-image-table';
import ComponetMarkupRemarkable from './component-markup-remarkable';
import ComponentDocumentationAnchor from './component-documentation-anchor';
import ComponentCode from './component-code';
import ComponentTableDataTable from './component-table-data-table';
import LineData from '../helper/line-data';
import LineObject from '../helper/line-object';
import ScrollData from '../helper/scroll-data';
import DataTestCaseSettings from '../../data/data-test-case/data-test-case-settings';
import DataTestDataTestCase from '../../data/data-test-case/data-test-data-test-case';
import DataTestDataIteration from '../../data/data-test-case/data-test-data-iteration';
import DataVerificationTestCase from '../../data/data-test-case/data-verification-test-case';
import DataTestSuiteAbstraction from '../../data/data-test-suite/data-test-suite-abstraction';
import DataActor from '../../data/data-test-case/data-actor';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ComponentMarkup extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.lineData = null;
  }
  
  didMount() {
    this._handleLines();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  didUpdate(prevProps, prevState) {
    this._handleLines();
  }
  
  calculateTop(scrollData) { // TODO: This must be optimized !!!
    let previousobject = null;
    const lineLength = this.lineData.lines.length;
    for(let i = 0; i < lineLength; ++i) {
      const line = this.lineData.lines[i];
      for(let j = 0; j < line.objects.length; ++j) {
        const object = line.objects[j];
        if(object.top > scrollData.top) {
          i = lineLength;
          break;
        }
        else {
          previousobject = object;
        }
      }
    }
    const diff = scrollData.top - previousobject.top;
    const delta = diff / previousobject.delta;
    scrollData.setLine(previousobject.line, delta);
  }
  
  calculateObject(scrollData, whichObject) {
    const dataLine = this.lineData.lines[scrollData.line];
    const objectsLength = dataLine.objects.length;
    const val = scrollData.delta * objectsLength;
    const posIndex = Math.floor(val);
    const posDelta = val - posIndex;
    const object = dataLine.objects[posIndex];
    if('next_block' === whichObject) {
      const line = object.nextRealObject ? object.nextRealObject.line : scrollData.line;
      scrollData.setLine(line, 0.0);
    }
    else if('previous_block' === whichObject) {
      const line = object.previousRealObject ? object.previousRealObject.line : scrollData.line;
      scrollData.setLine(line, 0.0);
    }
  }
  
  scroll(scrollData) {
    if(0 === this.lineData.lines.length) {
      return;
    }
    const dataLine = this.lineData.lines[scrollData.line];
    const objectsLength = dataLine.objects.length;
    const val = scrollData.delta * objectsLength;
    const posIndex = Math.floor(val);
    const posDelta = val - posIndex;
    const object = dataLine.objects[posIndex];
    const calculatedTop = object.top + posDelta * object.delta;
    this.props.onEditorScroll && this.props.onEditorScroll(calculatedTop);
  }
  
  _newDataLineObject(currenLineData, value) {
    const previousStart = currenLineData.start;
    currenLineData.start = currenLineData.stop;
    currenLineData.stop = currenLineData.start + value.lines - 1;
    currenLineData.lines += value.lines;
    if(previousStart === currenLineData.start) {
      ++currenLineData.objectIndex;
    }
    else {
      currenLineData.objectIndex = 0;
    }
    for(let currentLine = currenLineData.start; currentLine <= currenLineData.stop; ++currentLine) {
      if(this.lineData.lines.length <= currentLine) {
        this.lineData.lines.push(new LineData(currentLine));
      }
      this.lineData.lines[this.lineData.lines.length - 1].addObject(new LineObject(++this.lineData.objectId, value.type, currenLineData.start, currenLineData.stop, null, 0, currentLine, 0.0, -1.0, 0.0, null, null));
    }
  }
  
  _findExistingLines() {
    const elements = document.getElementsByClassName('data-line');
    const existingLines = [];
    let currentLine = 0;
    for(let i = 0; i < elements.length; ++i) {
      const line = Number.parseInt(elements[i].getAttribute('data-line-start'));
      const objectIndex = Number.parseInt(elements[i].getAttribute('data-line-object-index'));
      if(line !== currentLine) {
        currentLine = line;
      }
      existingLines.push({
        line: line,
        objectIndex: objectIndex,
        top: elements[i].offsetTop - elements[0].offsetTop,
        element: elements[i]
      });
    }
    return existingLines;
  }
  
  _addExistingLines(existingLines) {
    let previousDataLineObject = {};
    for(let i = 0; i < existingLines.length; ++i) {
      const existingLine = existingLines[i];
      const dataLine = this.lineData.lines[existingLine.line];
      const dataLineObject = dataLine.objects[existingLine.objectIndex];
      dataLineObject.element = existingLine.element;
      dataLineObject.top = existingLine.top;
      dataLineObject.previousRealObject = previousDataLineObject;
      previousDataLineObject.nextRealObject = dataLineObject;
      previousDataLineObject = dataLineObject;
    }
    if(0 !== existingLines.length) {
      const existingLine = existingLines[0];
      const dataLine = this.lineData.lines[existingLine.line];
      const dataLineObject = dataLine.objects[existingLine.objectIndex];
      dataLineObject.previousRealObject = null;
    }
  }

  _calculateLinePosition() {
    if(0 !== this.lineData.lines.length) {
      let currentObject = this.lineData.lines[0].objects[0];
      this.lineData.lines.forEach((dataLine) => {
        dataLine.objects.forEach((object, index) => {
          if(null !== object.element && currentObject.element !== object.element) {
            currentObject = object;
          }
          object.linePosition = dataLine.line + index / dataLine.objects.length;
        });
      });
    }
  }
  
  _calculateValues() {
    if(0 !== this.lineData.lines.length) {
      let currentObject = this.lineData.lines[0].objects[0];
      let nextRealObject = currentObject.nextRealObject;
      let delta = (nextRealObject.top - currentObject.top) / (nextRealObject.linePosition - currentObject.linePosition);
      let previousObject = {linePosition: 0, linePosition: 0}; // FAKE OBJECT FOR THE FIRST STEP IN THE ITERATION
      this.lineData.lines.forEach((dataLine) => {
        dataLine.objects.forEach((object, index) => {
          if(null !== object.element && currentObject.element !== object.element) {
            currentObject = object;
            nextRealObject = currentObject.nextRealObject;
            if(null !== nextRealObject) {
              delta = (nextRealObject.top - currentObject.top) / (nextRealObject.linePosition - currentObject.linePosition);
            }
            else {
              delta = 0;
            }
          }
          object.delta = delta;
          object.previousRealObject = currentObject.previousRealObject;
          object.nextRealObject = nextRealObject;
          previousObject.delta = previousObject.delta * (object.linePosition - previousObject.linePosition);
          object.top = currentObject.top + delta * (object.linePosition - currentObject.linePosition);
          previousObject = object;
        });
      });
      const dataLine = this.lineData.lines[this.lineData.lines.length - 1];
      dataLine.objects[dataLine.objects.length - 1].nextRealObject = null;
    }
  }
  
  _handleLines() {
    if(this.props.inner) {
      return;
    }
    const existingLines = this._findExistingLines();
    this._addExistingLines(existingLines);
    this._calculateLinePosition();
    this._calculateValues();
  }
  
  render() {
    let jsxObjects = [];
    const jsxDivs = [{divStyle: {}, jsxObjects: jsxObjects}];
    
    const mergedDocuments = [];
    const document = this.props.document;
    const documentLength = document.length;
    let previousDoc = null;
    for(let i = 0; i < documentLength; ++i) {
      const doc = document[i];
      if('markup_ref' === doc.type) {
        const link = this.props.linkReferences.get(doc.value.refName);
        if(undefined !== link) {
          const linkValue = `[${'' !== doc.value.text ? doc.value.text : link.text}](${link.path}:::${link.type})`;
          if(null === previousDoc) {
            previousDoc = {
              type: 'markup',
              value: linkValue,
              lines: 0
            };
            mergedDocuments.push(previousDoc);
          }
          else {
            previousDoc.value += linkValue;
          }
        }
      }
      else if('markup_documentation_status' === doc.type) {
        const ieValue = `<div style="${ComponentMarkup.documentationStatusCsses[doc.value.status]}padding:4px 10px;font-size:18px;">Doc Status: ${ComponentMarkup.documentationStatusTexts[doc.value.status]}.${doc.value.comment ? ' - ' : ''}${doc.value.comment}</div>`;
        if(null === previousDoc) {
          previousDoc = {
            type: 'markup',
            value: ieValue,
            lines: 0
          };
          mergedDocuments.push(previousDoc);
        }
        else {
          previousDoc.value += ieValue;
        }
      }
      else if('markup_api_status' === doc.type) {
        const ieValue = `<div style="${ComponentMarkup.apiStatusCsses[doc.value.status]}padding:4px 10px;font-size:18px;">Api Status: ${ComponentMarkup.apiStatusTexts[doc.value.status]}.${doc.value.comment ? ' - ' : ''}${doc.value.comment}</div>`;
        if(null === previousDoc) {
          previousDoc = {
            type: 'markup',
            value: ieValue,
            lines: 0
          };
          mergedDocuments.push(previousDoc);
        }
        else {
          previousDoc.value += ieValue;
        }
      }
      else if('markup_ie' === doc.type) {
        const ieValue = `<code style="color:Green;background-color:Honeydew;border:1px solid Green">${doc.value}</code>`;
        if(null === previousDoc) {
          previousDoc = {
            type: 'markup',
            value: ieValue,
            lines: 0
          };
          mergedDocuments.push(previousDoc);
        }
        else {
          previousDoc.value += ieValue;
        }
      }
      else if('markup_result' === doc.type) {
        const resultValue = `<span class="${ActorResultConst.getClassFromResult(doc.value)} log_result">${doc.value}</span>`;
        if(null === previousDoc) {
          previousDoc = {
            type: 'markup',
            value: resultValue,
            lines: 0
          };
          mergedDocuments.push(previousDoc);
        }
        else {
          previousDoc.value += resultValue;
        }
      }
      else {
        if(null === previousDoc) {
          if('markup' === doc.type) {
            previousDoc = {
              type: 'markup',
              value: doc.value,
              lines: doc.lines
            };
             mergedDocuments.push(previousDoc);
          }
          else {
            mergedDocuments.push(doc);
          }
        }
        else if('markup' === doc.type && 'markup' === previousDoc.type) {
          previousDoc.value += doc.value;
          previousDoc.lines += doc.lines - 1;
        }
        else {
          mergedDocuments.push(doc);
          previousDoc = null;
        }
      }
    }
    this.lineData = this.props.inner ? null : {
      lines: [],
      objectId: 0,
      current: {
        start: 0,
        stop: 1,
        objectIndex: 0,
        lines: 0
      }
    };
    if(!this.props.inner) {
      this.lineData.lines.push(new LineData(0, new LineObject(0, 'top', 0, 1, null, 0, 0, 0.0, -1.0, 0.0, null, null)));
    }
    const currenLineData = this.lineData ? this.lineData.current : null;
    mergedDocuments.forEach((value, index) => {
      let dataLineStart;
      let dataLineStop;
      let dataLineObjectIndex;
      if(!this.props.inner) {
        this._newDataLineObject(currenLineData, value);
        dataLineStart = currenLineData.start;
        dataLineStop = currenLineData.stop;
        dataLineObjectIndex = currenLineData.objectIndex;
      }
      if('markup' === value.type) {
        jsxObjects.push (
          <div key={index} className={`markup_markup${this.props.addClass ? ' ' + this.props.addClass : ''}`}>
            <ComponetMarkupRemarkable preview={this.props.preview} value={value.value} dataLineStart={dataLineStart} />
          </div>
        );
      }
      else if('markup_div' === value.type) {
        jsxObjects = [];
        jsxDivs.push({divStyle: {}, jsxObjects: jsxObjects});
      }
      else if('markup_anchor' === value.type) {
        jsxObjects.push (
          <ComponentDocumentationAnchor key={index} anchor={value.value.id} visible={value.value.visible} dataLineStart={dataLineStart} dataLineStop={dataLineStop} dataLineObjectIndex={dataLineObjectIndex} />
        );
      }
      else if('raw' === value.type) {
        const className = `markup_raw${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex} dangerouslySetInnerHTML={{__html: value.value}} />
        );
      }
      else if('lab' === value.type) {
        const className = `markup_lab${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            {<ComponentImageLab virtualNetworks={value.value}/>}
          </div>
        );
      }
      else if('object' === value.type) {
        value.value.forEach((htmlValue, innerIndex) => {
          if('button' === htmlValue.type) {
            const classNameSpan = `glyphicon ${htmlValue.glyphicon}`;
            const className = `markup_button${!this.props.inner ? ' data-line' : ''}`;
            jsxObjects.push (
              <div key={`${index}_${innerIndex}`} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
                <p className="markup_button btn-group btn-group-sm">
                  <span className="btn btn-default markup_button" style={{float:'none',marginRight:8,borderRadius:3}}>
                    <span className={classNameSpan} aria-hidden="true"></span>
                  </span>
                  <ComponetMarkupRemarkable inner preview={this.props.preview} value={htmlValue.text} />
                </p>
              </div>
            );
          }
          else if('toolbar' === htmlValue.type) {
            const className = `markup_button${!this.props.inner ? ' data-line' : ''}`;
            jsxObjects.push (
              <div key={`${index}_${innerIndex}`} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
                <div className="middle_toolbar" style={{position:'relative'}}>
                  <div className="btn-toolbar" role="toolbar" aria-label="...">
                    {this._renderToolbarButtons(htmlValue)}
                  </div>
                </div>
              </div>
            );
          }
          else if('anchor' === htmlValue.type) {
            const className = `markup_anchor${!this.props.inner ? ' data-line' : ''}`;
            jsxObjects.push (
              <a key={`${index}_${innerIndex}`} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex} name={htmlValue.link}></a>
            );
          }
          else if('br' === htmlValue.type) {
            const className = `markup_br${!this.props.inner ? ' data-line' : ''}`;
            jsxObjects.push (
              <br key={`${index}_${innerIndex}`} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex} />
            );
          }
          else if('log' === htmlValue.type) {
            const className = `markup_html_log${!this.props.inner ? ' data-line' : ''}`;
            const classNameRow = `${ComponentMarkup._getClassFromName(htmlValue.logType)} log_row_nowrap`;
            jsxObjects.push (
              <div key={`${index}_${innerIndex}`} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
                <table className="test_case_log_head_table test_case_log_body_table">
                  <thead>
                    <tr>
                      <th className="log_column_nbr">#</th>
                      <th className="log_column_type">type</th>
                      <th className="log_column_date">date</th>
                      <th>actor</th>
                      <th>log</th>
                      <th>file name</th>
                    </tr>  
                  </thead>
                  <tbody>
                    <tr className={classNameRow}>
                      <td className="log_column_nbr">1</td>
                      <td>{htmlValue.logType}</td>
                      <td className="log_column_date">{htmlValue.date}</td>
                      <td>{htmlValue.actor}</td>
                      <td>{htmlValue.log}</td>
                      <td>{htmlValue.fileName}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            );
          }
          else if('log_start' === htmlValue.type) {
            const className = `markup_html_log${!this.props.inner ? ' data-line' : ''}`;
            jsxObjects.push (
              <div key={`${index}_${innerIndex}`} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
                <table className="test_case_log_head_table test_case_log_body_table">
                  <thead>
                    <tr>
                      <th>type</th>
                      <th>date</th>
                      <th>actor</th>
                      <th>log</th>
                      <th>file name</th>
                    </tr>  
                  </thead>
                  <tbody>
                    <tr className="log">
                      <td colSpan="5">
                        <strong>TEST CASE START: </strong>
                        {htmlValue.date}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            );
          }
          else if('log_end' === htmlValue.type) {
            const className = `markup_html_log${!this.props.inner ? ' data-line' : ''}`;
            const classNameSpan = `test_${htmlValue.result.toLowerCase()} log_result`;
            jsxObjects.push (
              <div key={`${index}_${innerIndex}`} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
                <table className="test_case_log_head_table test_case_log_body_table">
                  <thead>
                    <tr>
                      <th>type</th>
                      <th>date</th>
                      <th>actor</th>
                      <th>log</th>
                      <th>file name</th>
                    </tr>  
                  </thead>
                  <tbody>
                    <tr className="log">
                      <td colSpan="5">
                        <strong>TEST CASE END: </strong>
                        <span className={classNameSpan}>
                          {htmlValue.result}
                        </span>
                        {htmlValue.duration}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            );
          }
        });
      }
      else if('markup_image' === value.type) {
        const className = `markup_img${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentImageImage width="100%" height="100%" data={value.value} />
          </div>
        );
      }
      else if('escape' === value.type) {
        const className = `markup_escape${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <p className="markup_escape">Markup</p>
            <pre className="markup_escape">
              <code className="markup_escape">
                {value.value}
              </code>
            </pre>
          </div>
        );
      }
      else if('chart-line' === value.type) {
        const className = `markup_chart_line${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentImageChartLine width="100%" height="100%" chartLine={value.value} />
          </div>
        );
      }
      else if('seq' === value.type) {
        const className = `markup_seq${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentImageSequenceDiagram width="100%" height="100%" sequenceDiagram={value.value} />
          </div>
        );
      }
      else if('state' === value.type) {
        const className = `markup_state${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentImageStateMachine width="100%" height="100%" stateMachine={value.value} />
          </div>
        );
      }
      else if('flow' === value.type) {
        const className = `markup_flow${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentImageFlowchart width="100%" height="100%" flowchart={value.value} />
          </div>
        );
      }
      else if('node' === value.type) {
        const className = `markup_node${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentImageNodeDiagram width="100%" height="100%" nodeDiagram={value.value} />
          </div>
        );
      }
      else if('table' === value.type) {
        const className = `markup_table${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentImageTable preview={this.props.preview} classTable="markup_table_fit_content" classHeading="markup_table_heading" classRow="markup_test_case_table" values={value.value} linkReferences={this.props.linkReferences} />
          </div>
        );
      }
      else if('javascript' === value.type) {
        const className = `markup_javascript${!this.props.inner ? ' data-line' : ''}`;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <ComponentCode id={`component_markup_${index}`} code={value.value} name="javascript" type="js" />
          </div>
        );
      }
      else if('tc' === value.type) {
        let key = 0;
        const dataTestCaseSettings = new DataTestCaseSettings();
        const dataActor = new DataActor();
        const dataTestDataTestCase = new DataTestDataTestCase();
        const dataTestDataIteration = new DataTestDataIteration();
        const dataVerificationTestCase = new DataVerificationTestCase();
        if(value.value.success) {
          const className = !this.props.inner ? 'data-line' : undefined;
          jsxObjects.push (
            <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
              <p className="markup_tc">Test Case</p>
              <div className="markup_tc">
                <ComponentTableDataTable preview={this.props.preview} classTable="markup_test_case_table_fit_content" classHeading="markup_test_case_table_heading" classRow="markup_test_case_table" key={++key} dataTable={dataTestCaseSettings} values={value.value.tc.settings}/>
                <ComponentTableDataTable preview={this.props.preview} classTable="markup_test_case_table_fit_content" classHeading="markup_test_case_table_heading" classRow="markup_test_case_table" key={++key} dataTable={dataActor} values={value.value.tc.actors}/>
                <ComponentTableDataTable preview={this.props.preview} classTable="markup_test_case_table_fit_content" classHeading="markup_test_case_table_heading" classRow="markup_test_case_table" key={++key} dataTable={dataTestDataTestCase} values={value.value.tc.testDataTestCases}/>
                <ComponentTableDataTable preview={this.props.preview} classTable="markup_test_case_table_fit_content" classHeading="markup_test_case_table_heading" classRow="markup_test_case_table" key={++key} dataTable={dataTestDataIteration} values={value.value.tc.testDataIteration}/>
                <ComponentTableDataTable preview={this.props.preview} classTable="markup_test_case_table_fit_content" classHeading="markup_test_case_table_heading" classRow="markup_test_case_table" key={++key} dataTable={dataVerificationTestCase} values={value.value.tc.verificationTestCases}/>
              </div>
            </div>
          );
        }
      }
      else if('ts' === value.type) {
        let key = 0;
        const dataTestSuiteAbstraction = new DataTestSuiteAbstraction();
        const className = !this.props.inner ? 'data-line' : undefined;
        jsxObjects.push (
          <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
            <p className="markup_ts">Test Suite</p>
            <div className="markup_ts">
              <ComponentTableDataTable preview={this.props.preview} classTable="markup_test_suite_table_fit_content" classHeading="markup_test_suite_table_heading" classRow="markup_test_suite_table" key={++key} dataTable={dataTestSuiteAbstraction} values={value.value.ts.abstractions}/>
            </div>
          </div>
        );
      }
      else {
        if(this.props.onUnkownMarkup) {
          jsxObjects.push(this.props.onUnkownMarkup(value, index, dataLineStart, dataLineStop, dataLineObjectIndex));
        }
        else {
          const className = !this.props.inner ? 'data-line' : undefined;
          jsxObjects.push (
            <div key={index} className={className} data-line-start={dataLineStart} data-line-stop={dataLineStop} data-line-object-index={dataLineObjectIndex}>
              <p> UNKNOWN MARKUP TYPE {value.type, value.value} </p>
            </div>
          );
        }
      }
    });
    const doc = jsxDivs.map((jsxDiv, index) => {
      const jsxObjects = jsxDiv.jsxObjects.map((jsxObject) => {
        return jsxObject;
      });
      return (
        <div key={`markup_div_${index}`} className="markup_div" style={jsxDiv.divStyle}>
          {jsxObjects}
        </div>
      );
    });
    if(!this.props.inner) {
      this._newDataLineObject(currenLineData, {type: 'bottom', lines: 1});
      return (
        <>
          <div key="markup_top" className="data-line" data-line-start="0" data-line-stop="0" data-line-object-index="0"></div>
          {doc}
          <div key="markup_bottom" className="data-line" data-line-start={currenLineData.start} data-line-stop={currenLineData.stop} data-line-object-index="0"></div>
        </>
      );
    }
    else {
      return (
        <>
          {doc}
        </>
      );
    }
  }
  
  _addToolbarButtonGroup() {
    
  }
  
  _renderToolbarButtons(htmlValue) {
    const buttonGroups = [];
  /*<div className="btn-group btn-group-sm" role="group" aria-label="...">
                      
                    </div>*/
  }
  
  _renderHardcodedLab() {
    return [
      {
        "name": "N1",
        "sut": "Actor",
        "family": "IPv4",
        "subnet": "192.168.0.0",
        "description": "Actor IPv4 network 1",
        "valid": true,
        "reduced": false,
        "suts": [
          {
            "name": "actorSut1",
            "sut": "Actor",
            "network": "N1",
            "direction": "server/client",
            "description": "",
            "address": "192.168.0.100",
            "netmask": "255.255.255.0",
            "static": true,
            "external": true,
            "valid": true,
            "reduced": false
          },
          {
            "name": "actorSut2",
            "sut": "Actor",
            "network": "N1",
            "direction": "server",
            "description": "",
            "address": "192.168.0.110",
            "netmask": "255.255.255.0",
            "static": true,
            "external": true,
            "valid": true,
            "reduced": false
          }
        ],
        "servers": [
          {
            "name": "actor1",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": false
          },
          {
            "name": "actor2",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actor3",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actor4",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actor5",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ],
        "clients": [
          {
            "name": "actor1",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actor2",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actor3",
            "sut": "Actor",
            "network": "N1",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ]
      },
      {
        "name": "N2",
        "sut": "Actor",
        "family": "IPv4",
        "subnet": "192.168.0.0",
        "description": "Actor IPv4 network 2",
        "valid": true,
        "reduced": true,
        "suts": [
          {
            "name": "actorSutN21",
            "sut": "Actor",
            "network": "N2",
            "direction": "server/client",
            "description": "",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actorSutN22",
            "sut": "Actor",
            "network": "N2",
            "direction": "server",
            "description": "",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actorSutN23",
            "sut": "Actor",
            "network": "N2",
            "direction": "client",
            "description": "",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ],
        "servers": [
          {
            "name": "actor21",
            "sut": "Actor",
            "network": "N2",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actor22",
            "sut": "Actor",
            "network": "N2",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ],
        "clients": [
          {
            "name": "actor21",
            "sut": "Actor",
            "network": "N2",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          },
          {
            "name": "actor22",
            "sut": "Actor",
            "network": "N2",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ]
      },
      {
        "name": "N3",
        "sut": "Actor",
        "family": "IPv4",
        "subnet": "192.168.0.0",
        "description": "Actor IPv4 network 3",
        "valid": true,
        "reduced": true,
        "suts": [
          {
            "name": "actorSutN31",
            "sut": "Actor",
            "network": "N3",
            "direction": "server/client",
            "description": "",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ],
        "servers": [
          {
            "name": "actor31",
            "sut": "Actor",
            "network": "N3",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ],
        "clients": [
          {
            "name": "actor31",
            "sut": "Actor",
            "network": "N3",
            "address": "192.168.0.99",
            "netmask": "255.255.255.0",
            "static": false,
            "external": false,
            "valid": true,
            "reduced": true
          }
        ]
      },
      {
        "name": "Nv6",
        "sut": "Actor",
        "family": "IPv6",
        "subnet": "",
        "description": "Actor IPv6 network 1",
        "valid": false,
        "reduced": false,
        "suts": [],
        "servers": [],
        "clients": []
      }
    ];
  }
  
  // DOUBLE CODE
  static _getClassFromName(resultName) {
    const foundIndex = ComponentMarkup.logTypePureNames.indexOf(resultName);
    if(-1 !== foundIndex) {
      return ComponentMarkup.logTypeCsses[foundIndex];
    }
    else {
      return '';
    }
  }
}

// DOUBLE CODE
ComponentMarkup.logTypePureNames = [
  'Engine',
  'Debug',
  'Error',
  'Warning',
  'IP',
  'GUI',
  'Success',
  'Failure',
  'TestData',
  'BrowserLog',
  'BrowserError'
];

// DOUBLE CODE
ComponentMarkup.logTypeCsses = [
  'log_engine',
  'log_debug',
  'log_error',
  'log_warning',
  'log_ip',
  'log_gui',
  'log_verify_success',
  'log_verify_failure',
  'log_test_data',
  'log_browser_log',
  'log_browser_err'
];

ComponentMarkup.documentationStatusTexts = [
  'Not documented yet',
  'Partially documented',
  'Majority documented',
  'Fully documented'
];

ComponentMarkup.documentationStatusCsses = [
  'color:White;background-color:Red;',
  'color:White;background-color:Orange;',
  'color:Grey;background-color:Yellow;',
  'color:White;background-color:Green;'
];

ComponentMarkup.apiStatusTexts = [
  'Deprecated',
  'Experimental',
  'Stable',
  'Legacy'
];

ComponentMarkup.apiStatusCsses = [
  'color:White;background-color:Red;',
  'color:White;background-color:Orange;',
  'color:White;background-color:Green;',
  'color:White;background-color:Blue;'
];


module.exports = ComponentMarkup;
