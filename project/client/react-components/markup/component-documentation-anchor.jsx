
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ComponentDocumentationAnchor extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderIcon() {
    if(!this.props.visible) {
      return null;
    }
    return (
      <span className="glyphicon_link glyphicon glyphicon-link" aria-hidden="true"></span>
    );
  }
  
  render() {
    const className = this.props.dataLineStart ? 'document_anchor data-line' : 'document_anchor';
    return (
      <div className={className} data-line-start={this.props.dataLineStart} data-line-stop={this.props.dataLineStop} data-line-object-index={this.props.dataLineObjectIndex}>
        <a id={this.props.anchor} href={`#${this.props.anchor}`}>
          {this.renderIcon()}
        </a>
      </div>
    );
  }
}

module.exports = ComponentDocumentationAnchor;
