
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ComponentImageStateMachine extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.BIAS = new Map([
      ['state',    {x:0,  y:-0, textHX:0, textHY:-5, textVX:0,  textVY:0}]
    ]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderTitle(title, x) {
    if(0 !== title) {
      return (
        <text x={x} y={ComponentImageStateMachine.TITLE_TOP}
          textAnchor="middle" style={{fontSize:ComponentImageStateMachine.TITLE_FONT_SIZE,fontWeight:'bold'}}>
          {title}
        </text>
      );
    }
  }
  
  renderState(state, titleHeight) {
    const bias = this.BIAS.get('state');
    const left = ComponentImageStateMachine.BIAS_X + state.xIndex * (ComponentImageStateMachine.STATE_WIDTH + ComponentImageStateMachine.STATE_DIST_WIDTH);
    const top = titleHeight + ComponentImageStateMachine.BIAS_Y + state.yIndex * (ComponentImageStateMachine.STATE_HEIGHT + ComponentImageStateMachine.STATE_DIST_HEIGHT);
    return (
      <g key={`state_${state.xIndex}_${state.yIndex}`}>
        <rect className="markup_state" x={left - bias.x} y={top - bias.y} height={ComponentImageStateMachine.STATE_HEIGHT} width={ComponentImageStateMachine.STATE_WIDTH} rx="15" ry="15" />
        <text x={left + ComponentImageStateMachine.STATE_WIDTH_HALF} y={top + (ComponentImageStateMachine.STATE_FONT_SIZE + ComponentImageStateMachine.STATE_HEIGHT) / 2} textAnchor="middle" style={{fontSize:ComponentImageStateMachine.STATE_FONT_SIZE,fontWeight:'bold'}}>
          {state.name}
        </text>
      </g>
    );
  }
    
  renderStates(stateRows, titleHeight) {
    const svgStates = stateRows.map((states, yIndex) => {
      return states.map((state) => {
        if(state.name && !state.comment) {
          return this.renderState(state, titleHeight);
        }
      });
    });
    return (
      <g>
        {svgStates}
      </g>
    );
  }
  
  static staticRenderCurve(xStart, yStart, xStop, yStop, positionData, transition, key) {
    const q = ComponentImageStateMachine._q2(xStart, xStop, yStart, yStop, positionData.topDirX, positionData.topDirY);
    const d = ['M', xStart, yStart, 'Q', q.xt, q.yt, xStop, yStop].join(' ');
    return (
      <g key={key}>
		  {/*<path className={`markup_state_${transition.type}`} d={d} />*/}
			<line x1={xStart} y1={yStart} x2={xStop} y2={yStop} stroke="black" strokeWidth="1" fill="orange" />
			{/*<line x1={q.xb} y1={q.yb} x2={q.xt} y2={q.yt} stroke="black" strokeWidth="1" fill="orange" />
			<line x1={xStart} y1={yStart} x2={q.xt} y2={q.yt} stroke="black" strokeWidth="1" fill="orange" />
  <line x1={xStop} y1={yStop} x2={q.xt} y2={q.yt} stroke="black" strokeWidth="1" fill="orange" />*/}
        <circle cx={xStart} cy={yStart} r="2" stroke="black" strokeWidth="1" fill="red" />
        <circle cx={q.xb} cy={q.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
			{/*<circle cx={q.xm} cy={q.ym} r="2" stroke="black" strokeWidth="1" fill="green" />
        <circle cx={q.xt} cy={q.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />*/}
        <circle cx={xStop} cy={yStop} r="2" stroke="black" strokeWidth="1" fill="red" />
      </g>
    );
  }
  
  static renderTransitionXEqYEq(fromState, toState, currentOut, currentIn, outPoint, inPoint, transition, titleHeight) {

  }
  
  static renderTransitionDiffSteps1Y(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight) {
    const xm1 = outPoint.x + ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ym1 = outPoint.y + positionData.yDiff * ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    const xm2 = xm1 + ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ym2 = ym1 + positionData.yDiff * ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    const q1 = ComponentImageStateMachine._q3(outPoint.x, xm1, outPoint.y, ym1, 1);
    const q2 = ComponentImageStateMachine._q3(xm2, xm1, ym2, ym1, -1);
    const d1 = ['M', outPoint.x, outPoint.y, 'Q', q1.xt, q1.yt, xm1, ym1].join(' ');
    const d2 = ['M', xm2, ym2, 'Q', q2.xt, q2.yt, xm1, ym1].join(' ');
    
    const xmIn1 = inPoint.x - ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ymIn1 = inPoint.y - positionData.yDiff * ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    const xmIn2 = xmIn1 - ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ymIn2 = ymIn1 - positionData.yDiff * ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    let qIn1;
    let qIn2;
    let dIn1;
    let dIn2;
    if(1 === positionData.yDiff) {
      qIn1 = ComponentImageStateMachine._q3(inPoint.x, xmIn1, inPoint.y, ymIn1, -1);
      qIn2 = ComponentImageStateMachine._q3(xmIn2, xmIn1, ymIn2, ymIn1, 1);
      dIn1 = ['M', xmIn1, ymIn1, 'Q', qIn1.xt, qIn1.yt, inPoint.x, inPoint.y].join(' ');
      dIn2 = ['M', xmIn2, ymIn2, 'Q', qIn2.xt, qIn2.yt, xmIn1, ymIn1].join(' ');
    }
    else {
      qIn1 = ComponentImageStateMachine._q3(inPoint.x, xmIn1, inPoint.y, ymIn1, -1);
      qIn2 = ComponentImageStateMachine._q3(xmIn2, xmIn1, ymIn2, ymIn1, 1);
      dIn1 = ['M', xmIn1, ymIn1, 'Q', qIn1.xt, qIn1.yt, inPoint.x, inPoint.y].join(' ');
      dIn2 = ['M', xmIn2, ymIn2, 'Q', qIn2.xt, qIn2.yt, xmIn1, ymIn1].join(' ');
    }
    const qM1 = ComponentImageStateMachine._q3(xm2, xmIn2, ym2, ymIn2, 1);
    const dM1 = ['M', xm2, ym2, 'Q', qM1.xt, qM1.yt, xmIn2, ymIn2].join(' ');
    
	/*
    
    const angle = ComponentImageStateMachine._angleQuadraticBezier(q2.xt, q2.yt, inPoint.x, inPoint.y, positionData);
    const translateArrow = `translate(${inPoint.x}, ${inPoint.y}) rotate(${angle * 180 / Math.PI})`;
   */
    return (
      <g key={`key_${fromState.xIndex}_${fromState.yIndex}_${toState.xIndex}_${toState.yIndex}_${currentOut}_${currentIn}`}>
        <path className={`d1 markup_state_${transition.type}`} d={d1} />
        <path className={`d2 markup_state_${transition.type}`} d={d2} />
        <path className={`dIn1 markup_state_${transition.type}`} d={dIn1} />
        <path className={`dIn2 markup_state_${transition.type}`} d={dIn2} />
        <path className={`dM1 markup_state_${transition.type}`} d={dM1} />
			{/*<polygon points={ComponentImageStateMachine.ARROW} transform={translateArrow} />*/}
        {/*<line x1={outPoint.x} y1={outPoint.y} x2={xm1} y2={ym1} stroke="black" strokeWidth="1" fill="orange" />
		{/*<rect x={outPoint.x} y={outPoint.y} width={xm1 - outPoint.x} height={ym1 - outPoint.y} stroke="black" strokeWidth="1" fill="none" />*/}
		
        <circle cx={outPoint.x} cy={outPoint.y} r="2" stroke="black" strokeWidth="1" fill="red" />
        {/*<circle cx={q1.xb} cy={q1.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
        <circle cx={q1.xt} cy={q1.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />
        <circle cx={xm1} cy={ym1} r="2" stroke="black" strokeWidth="1" fill="red" />*/}

        {/*<rect x={xm1} y={ym1} width={xm2 - xm1} height={ym2 - ym1} stroke="black" strokeWidth="1" fill="none" />*/}
		{/*<circle cx={q2.xb} cy={q2.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
        <circle cx={q2.xt} cy={q2.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />*/}
        
        {/*<line x1={xm1} y1={ym1} x2={q2.xt} y2={q2.yt} stroke="black" strokeWidth="1" fill="orange" />
        <line x1={inPoint.x} y1={inPoint.y} x2={q2.xt} y2={q2.yt} stroke="black" strokeWidth="1" fill="orange" />*/}
        
               
        <text x={xm1} y={ym1 - 6} textAnchor="middle" style={{fontSize:'10px',fontWeight:'bold'}}>
          {transition.data}
        </text>
      </g>
    );
  }
  
  static renderTransitionDiffStepsMY(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight) {
    const xm1 = outPoint.x + ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD;
    const ym1 = outPoint.y + ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    const xm2 = xm1 + ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ym2 = ym1 + ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    const q1 = ComponentImageStateMachine._q3(outPoint.x, xm1, outPoint.y, ym1, 1);
    const q2 = ComponentImageStateMachine._q3(xm2, xm1, ym2, ym1, -1);
    const d1 = ['M', outPoint.x, outPoint.y, 'Q', q1.xt, q1.yt, xm1, ym1].join(' ');
    const d2 = ['M', xm2, ym2, 'Q', q2.xt, q2.yt, xm1, ym1].join(' ');
    
    const xmIn1 = inPoint.x - ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ymIn1 = inPoint.y - ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    const qIn1 = ComponentImageStateMachine._q3(inPoint.x, xmIn1, inPoint.y, ymIn1, -1);
    const dIn1 = ['M', xmIn1, ymIn1, 'Q', qIn1.xt, qIn1.yt, inPoint.x, inPoint.y].join(' ');
    
    const xmM1 = xmIn1 - 2 * ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ymM1 = ym1 + 2 * ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD;
    const xmM3 = xmIn1 - ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD ;
    const ymM3 = ym1 + ComponentImageStateMachine.STATE_DIST_HEIGHT;
    
    const qM1 = ComponentImageStateMachine._q3(xm2, xmM1, ym2, ymM1, 1);
    const dM1 = ['M', xm2, ym2, 'Q', qM1.xt, qM1.yt, xmM1, ymM1].join(' ');
    
//    const qM2 = ComponentImageStateMachine._q3(xmM3, xmM1, ymM1, ymM3, -1);
//    const dM2 = ['M', xmM1, ymM1, 'Q', qM2.xt, qM2.yt, xmM3, ymM3].join(' ');
    
  //  const qM3 = ComponentImageStateMachine._q2(xmM3, xmIn1, ymM3, ymIn1, positionData.topDirX, positionData.topDirY);
  //  const dM3 = ['M', xmM3, ymM3, 'Q', qM3.xt, qM3.yt, xmIn1, ymIn1].join(' ');
    const qM3 = ComponentImageStateMachine._q2(xmM1, xmIn1, ymM1, ymIn1, positionData.topDirX, positionData.topDirY);
    const dM3 = ['M', xmM1, ymM1, 'Q', qM3.xt, qM3.yt, xmIn1, ymIn1].join(' ');
	
    return (
      <g key={`key_${fromState.xIndex}_${fromState.yIndex}_${toState.xIndex}_${toState.yIndex}_${currentOut}_${currentIn}`}>
        <path className={`d1 markup_state_${transition.type}`} d={d1} />
        <path className={`d2 markup_state_${transition.type}`} d={d2} />
        <path className={`dIn1 markup_state_${transition.type}`} d={dIn1} />
			{/*<path className={`dM2 markup_state_${transition.type}`} d={dM2} />*/}
        <path className={`dM1 markup_state_${transition.type}`} d={dM1} />
        <path className={`dM3 markup_state_${transition.type}`} d={dM3} />
		{/*<path className={`markup_state_${transition.type}`} d={dM2} />*/}
			{/*<polygon points={ComponentImageStateMachine.ARROW} transform={translateArrow} />*/}
        {/*<line x1={outPoint.x} y1={outPoint.y} x2={xm1} y2={ym1} stroke="black" strokeWidth="1" fill="orange" />
		{/*<rect x={outPoint.x} y={outPoint.y} width={xm1 - outPoint.x} height={ym1 - outPoint.y} stroke="black" strokeWidth="1" fill="none" />*/}
		
        <circle cx={outPoint.x} cy={outPoint.y} r="2" stroke="black" strokeWidth="1" fill="red" />
        {/*<circle cx={q1.xb} cy={q1.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
        <circle cx={q1.xt} cy={q1.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />
        <circle cx={xm1} cy={ym1} r="2" stroke="black" strokeWidth="1" fill="red" />*/}

        {/*<rect x={xm1} y={ym1} width={xm2 - xm1} height={ym2 - ym1} stroke="black" strokeWidth="1" fill="none" />*/}
		{/*<circle cx={q2.xb} cy={q2.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
        <circle cx={q2.xt} cy={q2.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />*/}
        
        {/*<line x1={xm1} y1={ym1} x2={q2.xt} y2={q2.yt} stroke="black" strokeWidth="1" fill="orange" />
        <line x1={inPoint.x} y1={inPoint.y} x2={q2.xt} y2={q2.yt} stroke="black" strokeWidth="1" fill="orange" />*/}
        
               
        <text x={xm1} y={ym1 - 6} textAnchor="middle" style={{fontSize:'10px',fontWeight:'bold'}}>
          {transition.data}
        </text>
      </g>
    );
  }
  
  static renderTransitionDiffStep(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight) {
    const xm = inPoint.x + (outPoint.x - inPoint.x) / 2 ;
    const ym = outPoint.y + (inPoint.y - outPoint.y) / 2;
    
    const q1 = ComponentImageStateMachine._q2(outPoint.x, xm, outPoint.y, ym, positionData.topDirX, positionData.topDirY);
    const q2 = ComponentImageStateMachine._q2(xm, inPoint.x, ym, inPoint.y, positionData.topDirX2, positionData.topDirY2);
    
	const d1 = ['M', outPoint.x, outPoint.y, 'Q', q1.xt, q1.yt, xm, ym].join(' ');
    const d2 = ['M', xm, ym, 'Q', q2.xt, q2.yt, inPoint.x, inPoint.y].join(' ');
    
    const angle = ComponentImageStateMachine._angleQuadraticBezier(q2.xt, q2.yt, inPoint.x, inPoint.y, positionData);
    const translateArrow = `translate(${inPoint.x}, ${inPoint.y}) rotate(${angle * 180 / Math.PI})`;
   
    return (
      <g key={`key_${fromState.xIndex}_${fromState.yIndex}_${toState.xIndex}_${toState.yIndex}_${currentOut}_${currentIn}`}>
        <path className={`markup_state_${transition.type}`} d={d1} />
        <path className={`markup_state_${transition.type}`} d={d2} />
        <polygon points={ComponentImageStateMachine.ARROW} transform={translateArrow} />
        <circle cx={outPoint.x} cy={outPoint.y} r="2" stroke="black" strokeWidth="1" fill="red" />
        
        {/*<line x1={xm} y1={ym} x2={inPoint.x} y2={inPoint.y} stroke="black" strokeWidth="1" fill="orange" />
        <line x1={xm} y1={ym} x2={q2.xt} y2={q2.yt} stroke="black" strokeWidth="1" fill="orange" />
        <line x1={inPoint.x} y1={inPoint.y} x2={q2.xt} y2={q2.yt} stroke="black" strokeWidth="1" fill="orange" />*/}
        
        {/*<circle cx={xm} cy={ym} r="2" stroke="black" strokeWidth="1" fill="red" />*/}
			{/*<circle cx={q1.xb} cy={,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,q1.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
        <circle cx={q1.xm} cy={q1.ym} r="2" stroke="black" strokeWidth="1" fill="green" />
			<circle cx={q1.xt} cy={q1.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />*/}
        
        <circle cx={inPoint.x} cy={inPoint.y} r="2" stroke="black" strokeWidth="1" fill="red" />
        {/*<circle cx={q2.xb} cy={q2.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
        <circle cx={q2.xm} cy={q2.ym} r="2" stroke="black" strokeWidth="1" fill="green" />
        <circle cx={q2.xt} cy={q2.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />*/}
        
        <text x={xm} y={ym - 6} textAnchor="middle" style={{fontSize:'10px',fontWeight:'bold'}}>
          {transition.data}
        </text>
      </g>
    );
  }
  
  static renderTransitionDiff(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight) {
    if(1 === Math.abs(positionData.xDiff)) {
      return ComponentImageStateMachine.renderTransitionDiffStep(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight);
    }
    else if(1 === Math.abs(positionData.yDiff)) {
      return ComponentImageStateMachine.renderTransitionDiffSteps1Y(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight);
    }
    else {
      return ComponentImageStateMachine.renderTransitionDiffStepsMY(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight);
    }
  }
  
  static renderTransitionEqLine(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight) {
    const q = ComponentImageStateMachine._q1(outPoint.x, inPoint.x, outPoint.y, inPoint.y, positionData.topDirX, positionData.topDirY);
    const d = ['M', outPoint.x, outPoint.y, 'Q', q.xt, q.yt , inPoint.x, inPoint.y].join(' ');
    const angle = ComponentImageStateMachine._angleQuadraticBezier(q.xt, q.yt, inPoint.x, inPoint.y, positionData);
    const translateArrow = `translate(${inPoint.x}, ${inPoint.y}) rotate(${(Math.PI + angle) * 180 / Math.PI})`;
    return (
      <g key={`key_${fromState.xIndex}_${fromState.yIndex}_${toState.xIndex}_${toState.yIndex}_${currentOut}_${currentIn}`}>
        <path className={`markup_state_${transition.type}`} d={d} />
        <polygon points={ComponentImageStateMachine.ARROW} transform={translateArrow} />
        <text x={q.xt} y={q.yt + 5} textAnchor="middle" style={{fontSize:'10px',fontWeight:'bold'}}>
          {transition.data}
        </text>
        <circle cx={outPoint.x} cy={outPoint.y} r="2" stroke="black" strokeWidth="1" fill="red" />
        <circle cx={inPoint.x} cy={inPoint.y} r="2" stroke="black" strokeWidth="1" fill="red" />
       {/*<circle cx={q.xb} cy={q.yb} r="2" stroke="black" strokeWidth="1" fill="yellow" />
        <circle cx={q.xm} cy={q.ym} r="2" stroke="black" strokeWidth="1" fill="green" />
        <circle cx={q.xt} cy={q.yt} r="2" stroke="black" strokeWidth="1" fill="blue" />*/}
      </g>
    );
  }
  
  static renderTransitionXGtYGt(fromState, toState, currentOut, currentIn, outPoint, inPoint, transition, titleHeight) {

  }
  
  calculatePoint(position, state, titleHeight) {
    const left = ComponentImageStateMachine.BIAS_X + state.xIndex * (ComponentImageStateMachine.STATE_DIST_WIDTH + ComponentImageStateMachine.STATE_WIDTH);
    const top = titleHeight + ComponentImageStateMachine.BIAS_Y + state.yIndex * (ComponentImageStateMachine.STATE_HEIGHT + ComponentImageStateMachine.STATE_DIST_HEIGHT);
    const bias = this.BIAS.get('state');
    if(ComponentImageStateMachine.RIGHT === position) {
      return {
        x: left + ComponentImageStateMachine.POSITION_X[position] + bias.x,
        y: top + ComponentImageStateMachine.POSITION_Y[position]
      }
    }
    else if(ComponentImageStateMachine.LEFT === position) {
      return {
        x: left + ComponentImageStateMachine.POSITION_X[position] - bias.x,
        y: top + ComponentImageStateMachine.POSITION_Y[position]
      }
    }
    else if(ComponentImageStateMachine.TOP === position) {
      return {
        x: left + ComponentImageStateMachine.POSITION_X[position],
        y: top + ComponentImageStateMachine.POSITION_Y[position] - bias.y
      }
    }
    else if(ComponentImageStateMachine.BOTTOM === position) {
      return {
        x: left + ComponentImageStateMachine.POSITION_X[position],
        y: top + ComponentImageStateMachine.POSITION_Y[position] + bias.y
      }
    }
    else {
      return {
        x: left + ComponentImageStateMachine.POSITION_X[position],
        y: top + ComponentImageStateMachine.POSITION_Y[position]
      }
    }
  }
  
  calculatePosition(fromState, toState, rows, columns) {
    const xDiff = toState.xIndex - fromState.xIndex;
    const yDiff = toState.yIndex - fromState.yIndex;
    const diff = [fromState.xIndex - toState.xIndex, fromState.yIndex - toState.yIndex];
    const direction = [xDiff > 0 ? ComponentImageStateMachine.DIRECTION_X_EAST : (xDiff < 0 ? ComponentImageStateMachine.DIRECTION_X_WEST : ComponentImageStateMachine.DIRECTION_X_NONE), yDiff > 0 ? ComponentImageStateMachine.DIRECTION_Y_SOUTH : (yDiff < 0 ? ComponentImageStateMachine.DIRECTION_Y_NORTH : ComponentImageStateMachine.DIRECTION_Y_NONE)];
    const xOrientation = fromState.xIndex < columns / 2 ? ComponentImageStateMachine.ORIENTATION_X_LEFT  : ComponentImageStateMachine.ORIENTATION_X_RIGHT;
    const yOrientation = fromState.yIndex < rows / 2 ? ComponentImageStateMachine.ORIENTATION_Y_TOP : ComponentImageStateMachine.ORIENTATION_Y_BOTTOM;
    const positionData = ComponentImageStateMachine.POSITION_LOG[xOrientation][yOrientation][direction[0]][direction[1]];
    return {
      angle: positionData.angle,
      topDirX: positionData.topDirX,
      topDirY: positionData.topDirY,
      topDirX2: positionData.topDirX2,
      topDirY2: positionData.topDirY2,
      from:  positionData.from,
      to:  positionData.to,
      diff: diff,
      xOrientation: xOrientation,
      yOrientation: yOrientation,
      direction: direction,
      xDiff: xDiff,
      yDiff: yDiff,
      func: positionData.func,
      log: positionData.log
    };
  }
  
  renderTransition(fromState, toState, transition, rows, columns, titleHeight) {
    const positionData = this.calculatePosition(fromState, toState, rows, columns);
    const currentOut = fromState.currentOut++;
    const currentIn = toState.currentIn++;
    const outPoint = this.calculatePoint(positionData.from, fromState, titleHeight);
    const inPoint = this.calculatePoint(positionData.to, toState, titleHeight);
    if(positionData && !transition.comment) {
    
    }
    if(positionData.func && !transition.comment) {
      return positionData.func(fromState, toState, currentOut, currentIn, outPoint, inPoint, positionData, transition, titleHeight);
    }
    else {
     return null;
    }
  }
  
  renderTransitions(transitions, stateMap, rows, columns, titleHeight) {
    return transitions.map((transition, index) => {
      if('' !== transition.fromStateName && '' !== transition.toStateName) {
        const fromState = stateMap.get(transition.fromStateName);
        const toState = stateMap.get(transition.toStateName);
        return this.renderTransition(fromState, toState, transition, rows, columns, titleHeight);
      }
    });
  }
  
  render() {
    const title = this.props.stateMachine.title;
    const stateRows = this.props.stateMachine.stateRows;
    const transitions = this.props.stateMachine.transitions;
    const rows = stateRows.length;
    let columns = 0;
    const stateMap = new Map();
    stateRows.forEach((states, yIndex) => {
      states.forEach((state, xIndex) => {
        stateMap.set(state.name, {
          xIndex: xIndex,
          yIndex: yIndex,
          in: state.in,
          out: state.out,
          currentIn: 0,
          currentOut: 0,
          type: state.type
        });
      });
      if(states.length > columns) {
        columns = states.length;
      }
    });
    const titleHeight = ('' !== title && undefined !== title) ? ComponentImageStateMachine.TITLE_HEIGHT : 0;
    const stateHeight = ComponentImageStateMachine.BIAS_Y + stateRows.length * ComponentImageStateMachine.STATE_HEIGHT + (stateRows.length - 1) * ComponentImageStateMachine.STATE_DIST_HEIGHT;
    const svgHeight = titleHeight + stateHeight + ComponentImageStateMachine.BIAS_X;
    const svgWidth = 2 * ComponentImageStateMachine.BIAS_X + columns * ComponentImageStateMachine.STATE_WIDTH + (columns - 1) * ComponentImageStateMachine.STATE_DIST_WIDTH;
    const viewBox = [0, 0, svgWidth, svgHeight].join(' ');
    return (
      <svg className="markup_state" width={svgWidth} height={svgHeight} viewBox={viewBox} >
        {/*  <rect className="markup_state" x={0} y={titleHeight} width={svgWidth} height={svgHeight - titleHeight} /> */}
        {this.renderTitle(title, svgWidth / 2)}
        {this.renderStates(stateRows, titleHeight)}
        {this.renderTransitions(transitions, stateMap, rows, columns, titleHeight)}
      </svg>
    );
  }

  static _q1(x1, x2, y1, y2, topDirX, topDirY) {
    const hypo = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    const angle = Math.acos((x2 - x1) / hypo);
    const xb = (Math.max(x1, x2) - Math.min(x1, x2)) / 2 + Math.min(x1, x2);
    const yb = (Math.max(y1, y2) - Math.min(y1, y2)) / 2 + Math.min(y1, y2);
    const sinX = Math.sin(Math.PI - angle);
    const cosY = Math.cos(Math.PI - angle);
    const xt = xb + topDirX * ComponentImageStateMachine.STATE_DIST_WIDTH_TOP * sinX;
    const yt = yb + topDirY * ComponentImageStateMachine.STATE_DIST_HEIGHT_TOP * cosY;
    return {
      xb: xb,
      yb: yb,
      xm: xt - (xt - xb) / 2,
      ym: yt - (yt - yb) / 2,
      xt: xt,
      yt: yt
    };
  }
  
  static _q2(x1, x2, y1, y2, topDirX, topDirY) {
    const hypo = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    const angle = Math.acos((x2 - x1) / hypo);
    const xb = (Math.max(x1, x2) - Math.min(x1, x2)) / 2 + Math.min(x1, x2);
    const yb = (Math.max(y1, y2) - Math.min(y1, y2)) / 2 + Math.min(y1, y2);
    const sinX = Math.sin(Math.PI - angle);
    const cosY = Math.cos(Math.PI - angle);
    const xt = 1 === topDirX ? Math.max(x1, x2) : Math.min(x1, x2);
    const yt = -1 === topDirY ? Math.max(y1, y2) : Math.min(y1, y2);
    return {
      xb: xb,
      yb: yb,
      xm: xt - (xt - xb) / 2,
      ym: yt - (yt - yb) / 2,
      xt: xt,
      yt: yt
    };
  }
  
  //
  //    /|
  // c / | a
  //  /  |
  //  ____
  //
  //    b
  static _q3(x1, x2, y1, y2, dir) {
    const a1 = y2 - y1;
    const b1 = x2 - x1;
    const A = Math.atan(a1 / b1);
    const B = ComponentImageStateMachine.MATH_PI_HALF - A;
    
    const cosA = Math.cos(A);
    const sinB = Math.sin(B);
    const cosB = Math.cos(B);
    const b2 = b1 * cosA;
    const a2 = ComponentImageStateMachine.STATE_DIST_TOP_LENGTH / 3;
    const xb = b2 * sinB;
    const yb = b2 * cosB;
    
    const b3 = a2 * Math.cos(B);
    const a3 = a2 * Math.sin(A);
    const xt = xb + dir * b3;
    const yt = yb - dir * a3;

    return {
      xb: x1 + xb,
      yb: y1 + yb,
      xm: xt - (xt - xb) / 2,
      ym: yt - (yt - yb) / 2,
      xt: x1 + xt,
      yt: y1 + yt
    };
  }
  
  static _angleQuadraticBezier(xControl, yControl, xStop, yStop, positionData) {
    const a = yStop - yControl;
    const b = xStop - xControl;
    const A = Math.atan(a/b);
    return A + positionData.angle;
  }
}

ComponentImageStateMachine.MATH_PI_TO_GRADES = 180 / Math.PI;
ComponentImageStateMachine.MATH_PI_HALF = Math.PI / 2;

ComponentImageStateMachine.TITLE_HEIGHT = 60;
ComponentImageStateMachine.TITLE_FONT_SIZE = 20;
ComponentImageStateMachine.TITLE_TOP = ComponentImageStateMachine.TITLE_FONT_SIZE + (ComponentImageStateMachine.TITLE_HEIGHT - ComponentImageStateMachine.TITLE_FONT_SIZE) / 2;
ComponentImageStateMachine.BIAS_X = 50;
ComponentImageStateMachine.BIAS_Y = 30;
ComponentImageStateMachine.STATE_AREA_WIDTH = 180;
ComponentImageStateMachine.STATE_AREA_HEIGHT = 100;
ComponentImageStateMachine.STATE_WIDTH = 90;
ComponentImageStateMachine.STATE_HEIGHT = 40;
ComponentImageStateMachine.STATE_WIDTH_HALF = ComponentImageStateMachine.STATE_WIDTH / 2;
ComponentImageStateMachine.STATE_HEIGHT_HALF = ComponentImageStateMachine.STATE_HEIGHT / 2;
ComponentImageStateMachine.STATE_DIST_WIDTH = ComponentImageStateMachine.STATE_AREA_WIDTH - ComponentImageStateMachine.STATE_WIDTH;
ComponentImageStateMachine.STATE_DIST_HEIGHT = ComponentImageStateMachine.STATE_AREA_HEIGHT - ComponentImageStateMachine.STATE_HEIGHT;
ComponentImageStateMachine.STATE_DIST_TOP_LENGTH = Math.sqrt(Math.pow(ComponentImageStateMachine.STATE_WIDTH_HALF, 2), Math.pow(ComponentImageStateMachine.STATE_DIST_HEIGHT_HALF, 2));
ComponentImageStateMachine.STATE_DIST_WIDTH_HALF = ComponentImageStateMachine.STATE_DIST_WIDTH / 2;
ComponentImageStateMachine.STATE_DIST_HEIGHT_HALF = ComponentImageStateMachine.STATE_DIST_HEIGHT / 2;
ComponentImageStateMachine.STATE_DIST_WIDTH_THIRD = ComponentImageStateMachine.STATE_DIST_WIDTH / 3;
ComponentImageStateMachine.STATE_DIST_HEIGHT_THIRD = ComponentImageStateMachine.STATE_DIST_HEIGHT / 3;
ComponentImageStateMachine.STATE_DIST_MIN = Math.min(ComponentImageStateMachine.STATE_DIST_WIDTH_HALF, ComponentImageStateMachine.STATE_DIST_HEIGHT_HALF); // TODO: REMOVE
ComponentImageStateMachine.STATE_DIST_WIDTH_TOP = ComponentImageStateMachine.STATE_DIST_WIDTH * 2 / 5;
ComponentImageStateMachine.STATE_DIST_HEIGHT_TOP = ComponentImageStateMachine.STATE_DIST_HEIGHT * 2 / 5;
ComponentImageStateMachine.STATE_FONT_SIZE = 12;
ComponentImageStateMachine.ARROW = [-10, 3, 0, 0, -10, -3].join(' ');
ComponentImageStateMachine.HALF_PI = Math.PI / 2;

ComponentImageStateMachine.TOP = 0;
ComponentImageStateMachine.RIGHT = 1;
ComponentImageStateMachine.BOTTOM = 2;
ComponentImageStateMachine.LEFT = 3;
ComponentImageStateMachine.POSITION_X = [ComponentImageStateMachine.STATE_WIDTH_HALF, ComponentImageStateMachine.STATE_WIDTH, ComponentImageStateMachine.STATE_WIDTH_HALF, 0];
ComponentImageStateMachine.POSITION_Y = [0, ComponentImageStateMachine.STATE_HEIGHT_HALF, ComponentImageStateMachine.STATE_HEIGHT, ComponentImageStateMachine.STATE_HEIGHT_HALF];


ComponentImageStateMachine.ORIENTATION_X = 0;
ComponentImageStateMachine.ORIENTATION_Y = 1;
ComponentImageStateMachine.ORIENTATION_X_LEFT = 0;
ComponentImageStateMachine.ORIENTATION_X_RIGHT = 1;
ComponentImageStateMachine.ORIENTATION_Y_TOP = 0;
ComponentImageStateMachine.ORIENTATION_Y_BOTTOM = 1; 
ComponentImageStateMachine.ORIENTATION_LOG = [['LEFT_TOP', 'LEFT_BOTTOM'], ['RIGHT_BOTTOM', 'RIGHT_TOP']];

ComponentImageStateMachine.DIRECTION_X_NONE = 0;
ComponentImageStateMachine.DIRECTION_X_WEST = 1;
ComponentImageStateMachine.DIRECTION_X_EAST = 2;
ComponentImageStateMachine.DIRECTION_Y_NONE = 0;
ComponentImageStateMachine.DIRECTION_Y_NORTH = 1;
ComponentImageStateMachine.DIRECTION_Y_SOUTH = 2; 
ComponentImageStateMachine.DIRECTION_LOG = [['NONE_NONE', 'NONE_NORTH', 'NONE_SOUTH'], ['WEST_NONE', 'WEST_NORTH', 'WEST_SOUTH'], ['EAST_NONE', 'EAST_NORTH', 'EAST_SOUTH']];

// ORIENTATION_X
// ORIENTATION_Y
// DIRECTION_X
// DIRECTION_Y
ComponentImageStateMachine.POSITION_LOG = [
  [
    [
      [
        {log:'LEFT_TOP_NONE_NONE'},
        {log:'LEFT_TOP_NONE_NORTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.RIGHT,angle:0,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'LEFT_TOP_NONE_SOUTH',from:ComponentImageStateMachine.LEFT,to:ComponentImageStateMachine.LEFT,angle:Math.PI,topDirX:-1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
      ],
      [
        {log:'LEFT_TOP_WEST_NONE',from:ComponentImageStateMachine.BOTTOM,to:ComponentImageStateMachine.BOTTOM,angle:0,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'LEFT_TOP_WEST_NORTH'},
        {log:'LEFT_TOP_WEST_SOUTH'}
      ],
      [
        {log:'LEFT_TOP_EAST_NONE',from:ComponentImageStateMachine.TOP,to:ComponentImageStateMachine.TOP,angle:Math.PI,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'LEFT_TOP_EAST_NORTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.LEFT,angle:0,topDirX:1,topDirY:-1,topDirX2:-1,topDirY2:1,func:ComponentImageStateMachine.renderTransitionDiff},
        {log:'LEFT_TOP_EAST_SOUTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.LEFT,angle:0,topDirX:1,topDirY:1,topDirX2:-1,topDirY2:-1,func:ComponentImageStateMachine.renderTransitionDiff}
      ]
    ],
    [
      [
        {log:'LEFT_BOTTOM_NONE_NONE'},
        {log:'LEFT_BOTTOM_NONE_NORTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.RIGHT,angle:0,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'LEFT_BOTTOM_NONE_SOUTH',from:ComponentImageStateMachine.LEFT,to:ComponentImageStateMachine.LEFT,angle:Math.PI,topDirX:-1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine}
      ],
      [
        {log:'LEFT_BOTTOM_WEST_NONE',from:ComponentImageStateMachine.TOP,to:ComponentImageStateMachine.TOP,angle:0,topDirX:1,topDirY:-1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'LEFT_BOTTOM_WEST_NORTH'},
        {log:'LEFT_BOTTOM_WEST_SOUTH'}
      ],
      [
        {log:'LEFT_BOTTOM_EAST_NONE',from:ComponentImageStateMachine.BOTTOM,to:ComponentImageStateMachine.BOTTOM,angle:Math.PI,topDirX:1,topDirY:-1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'LEFT_BOTTOM_EAST_NORTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.LEFT,angle:0,topDirX:1,topDirY:-1,topDirX2:-1,topDirY2:1,func:ComponentImageStateMachine.renderTransitionDiff},
        {log:'LEFT_BOTTOM_EAST_SOUTH'}
      ]
    ]
  ],
  [
    [
      [
        {log:'RIGHT_TOP_NONE_NONE'},
        {log:'RIGHT_TOP_NONE_NORTH',from:ComponentImageStateMachine.LEFT,to:ComponentImageStateMachine.LEFT,angle:Math.PI,topDirX:-1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'RIGHT_TOP_NONE_SOUTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.RIGHT,angle:0,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
      ],
      [
        {log:'RIGHT_TOP_WEST_NONE',from:ComponentImageStateMachine.BOTTOM,to:ComponentImageStateMachine.BOTTOM,angle:0,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'RIGHT_TOP_WEST_NORTH'},
        {log:'RIGHT_TOP_WEST_SOUTH'}
      ],
      [
        {log:'RIGHT_TOP_EAST_NONE',from:ComponentImageStateMachine.TOP,to:ComponentImageStateMachine.TOP,angle:Math.PI,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'RIGHT_TOP_EAST_NORTH'},
        {log:'RIGHT_TOP_EAST_SOUTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.LEFT,angle:0,topDirX:1,topDirY:1,topDirX2:-1,topDirY2:-1,func:ComponentImageStateMachine.renderTransitionDiff}
      ]
    ],
    [
      [
        {log:'RIGHT_BOTTOM_NONE_NONE'},
        {log:'RIGHT_BOTTOM_NONE_NORTH',from:ComponentImageStateMachine.LEFT,to:ComponentImageStateMachine.LEFT,angle:Math.PI,topDirX:-1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'RIGHT_BOTTOM_NONE_SOUTH',from:ComponentImageStateMachine.RIGHT,to:ComponentImageStateMachine.RIGHT,angle:0,topDirX:1,topDirY:1,func:ComponentImageStateMachine.renderTransitionEqLine}
      ],
      [
        {log:'RIGHT_BOTTOM_WEST_NONE',from:ComponentImageStateMachine.TOP,to:ComponentImageStateMachine.TOP,angle:0,topDirX:1,topDirY:-1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'RIGHT_BOTTOM_WEST_NORTH'},
        {log:'RIGHT_BOTTOM_WEST_SOUTH'}
      ],
      [
        {log:'RIGHT_BOTTOM_EAST_NONE',from:ComponentImageStateMachine.BOTTOM,to:ComponentImageStateMachine.BOTTOM,angle:Math.PI,topDirX:1,topDirY:-1,func:ComponentImageStateMachine.renderTransitionEqLine},
        {log:'RIGHT_BOTTOM_EAST_NORTH'},
        {log:'RIGHT_BOTTOM_EAST_SOUTH'}
      ]
    ],
  ]
];
