
'use strict';

import ComponentMarkup from './component-markup';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


class ComponentLocalNote extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderNote() {
    if(!this.props.note || !this.props.note.created) {
      return null;
    }
    return (
      <div className="local_note_outer">
        <div className="local_note_inner" data-line-start={this.props.dataLineStart} data-line-stop={this.props.dataLineStart} data-line-object-index="0">
          <ComponentMarkup inner={this.props.inner} document={this.props.note.document} addClass="markup_local_note"/>
        </div>
      </div>
    );
  }
  
  renderAddNote() {
    if(this.props.note && this.props.note.created) {
      return null;
    }
    return (
      <li className="local_document_choice">
        <Link onClick={(e) => {
            this.props.onAddNote && this.props.onAddNote(); 
          }}>Add Note
        </Link>
      </li>
    );
  }
  
  renderEditNote() {
    if(!this.props.note || !this.props.note.created) {
      return null;
    }
    return (
      <li className="local_document_choice">
        <Link onClick={(e) => {
            this.props.onEditNote && this.props.onEditNote(); 
          }}>Edit Note
        </Link>
      </li>
    );
  }
  
  renderDeleteNote() {
    if(!this.props.note || !this.props.note.created) {
      return null;
    }
    return (
      <li className="local_document_choice">
        <Link onClick={(e) => {
            this.props.onDeleteNote && this.props.onDeleteNote(); 
          }}>Delete Note
        </Link>
      </li>
    );
  }
  
  renderLocalItem() {
    return (
      <div className="dropdown local_document_choice">
        <button className="btn btn-xs dropdown-toggle local_document_choice" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          <span className="caret"></span>
        </button>
        <ul className="dropdown-menu dropdown-menu-right local_document_choice" aria-labelledby="dropdownMenu1">
          {this.renderAddNote()}
          {this.renderEditNote()}
          {this.renderDeleteNote()}
        </ul>
      </div>
    );
  }

  render() {
    return (
      <>
        {this.renderLocalItem()}
        {this.renderNote()}
      </>
    );
  }
}

module.exports = ComponentLocalNote;
