
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ComponentImageNodeDiagram extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props.nodeDiagram, nextProps.nodeDiagram);
  }
  
  renderTitle(title, x) {
    if(0 !== title) {
      return (
        <text x={x} y={ComponentImageNodeDiagram.TITLE_TOP} textAnchor="middle" style={{fontSize:ComponentImageNodeDiagram.TITLE_FONT_SIZE,fontWeight:'bold'}}>
          {title}
        </text>  
      );
    }
  }
  
  renderGroup(group, titleHeight, config, index) {
    if(-1 === group.leftStartIndex || -1 === group.leftStopIndex || -1 === group.topStartIndex || -1 === group.topStopIndex) {
      return null;
    }
    const groupBias = 25;
    const halfNodeWidthBetween = config.nodeWidthBetween / 2;
    const halfNodeHeightBetween = config.nodeHeightBetween / 2;
    const left = config.widthBias + group.leftStartIndex * (config.nodeWidth + config.nodeWidthBetween) - halfNodeWidthBetween;
    const top = titleHeight + config.heightBias + group.topStartIndex * (config.nodeHeight + config.nodeHeightBetween) - halfNodeHeightBetween;
    const width = (group.leftStopIndex - group.leftStartIndex) * (config.nodeWidth + config.nodeWidthBetween);
    const height = (group.topStopIndex - group.topStartIndex) * (config.nodeHeight + config.nodeHeightBetween);
    return (
      <g key={`group_${index}`}>
        <rect className="node_diagram_group" style={{fill:group.color}} x={left} y={top} rx="4" ry="4" width={width} height={height} />
        <text x={left + 5} y={top + 15} style={{fontSize:'12px',fontWeight:'bold',textAnchor:'left'}}>
          {group.name}
        </text>
      </g>
    );
  }
  
  renderGroups(groups, titleHeight, config) {
    return groups.map((group, index) => {
      return this.renderGroup(group, titleHeight, config, index);
    });
  }
  
  renderStar(node, titleHeight, config) {
    const left = config.widthBias + node.xIndex * (config.nodeWidth + config.nodeWidthBetween);
    const top = titleHeight + config.heightBias + node.yIndex * (config.nodeHeight + config.nodeHeightBetween);
   
    const lengthW = config.nodeWidth / 2;
    const lengthH = config.nodeHeight / 2;
    const corners = node.corners ? node.corners : 5;
    const points = new Array(corners);
    const angle = 360 / corners;
    for(let i = 0; i < corners; ++i) {
      const px = left + lengthW + lengthW * Math.sin((180 + angle * i) * Math.PI / 180);
      const py = top + lengthH + lengthH * Math.cos((180 + angle * i) * Math.PI / 180);
      points[i] = `${px},${py}`;
    }
    return (
      <g key={`node_${node.xIndex}_${node.yIndex}`}>
        <polygon className="node_diagram_star" points={`${points.join(' ')}`} />
        <text x={left + config.nodeWidth / 2} y={top + config.nodeHeight / 2 + 5} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'middle'}}>
          {node.name}
        </text>
      </g>
    );
  }
  
  renderPolygon(node, titleHeight, config) {
    const left = config.widthBias + node.xIndex * (config.nodeWidth + config.nodeWidthBetween);
    const top = titleHeight + config.heightBias + node.yIndex * (config.nodeHeight + config.nodeHeightBetween);
   
    const lengthW = config.nodeWidth / 2;
    const lengthH = config.nodeHeight / 2;
    const corners = node.corners ? node.corners : 5;
    const points = new Array(corners);
    const angle = 360 / corners;
    for(let i = 0; i < corners; ++i) {
      const px = left + lengthW + lengthW * Math.sin((180 + angle * i) * Math.PI / 180);
      const py = top + lengthH + lengthH * Math.cos((180 + angle * i) * Math.PI / 180);
      points[i] = `${px},${py}`;
    }
    return (
      <g key={`node_${node.xIndex}_${node.yIndex}`}>
        <polygon className="node_diagram_star" points={`${points.join(' ')}`} />
        <text x={left + config.nodeWidth / 2} y={top + config.nodeHeight / 2 + 5} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'middle'}}>
          {node.name}
        </text>
      </g>
    );
  }
  
  renderSquare(node, titleHeight, config) {
    if('' === node.name) {
      return null;
    }
    const left = config.widthBias + node.xIndex * (config.nodeWidth + config.nodeWidthBetween);
    const top = titleHeight + config.heightBias + node.yIndex * (config.nodeHeight + config.nodeHeightBetween);
    return (
      <g key={`node_${node.xIndex}_${node.yIndex}`}>
        <rect className="node_diagram_square" style={{fill:node.color}} x={left} y={top} rx="4" ry="4" width={config.nodeWidth} height={config.nodeHeight} />
        <text x={left + config.nodeWidth / 2} y={top + config.nodeHeight / 2 + 5} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'middle'}}>
          {node.name}
        </text>
      </g>
    );
  }
  
  renderNode(node, titleHeight, config) {
    switch(node.type) {
      case 'polygon':
        return this.renderPolygon(node, titleHeight, config);
      case 'star':
        return this.renderStar(node, titleHeight, config);
      case 'square':
      default:
        return this.renderSquare(node, titleHeight, config);
    }
  }
  
  renderNodes(nodeRows, titleHeight, config) {
    const svgNodes = nodeRows.map((nodes, yIndex) => {
      return nodes.map((node, xIndex) => {
        return this.renderNode(node, titleHeight, config);
      });
    });
    return (
      <g>
        {svgNodes}
      </g>
    );
  }
  
  calculateCurrentPoint(length, current, currents) {
    const line = length * 0.8;
    const bais = length * 0.1;
    const linePart = line / (currents + 1);
    return bais + (current + 1) * linePart;
  }
  
  calculatePoint(position, current, currents, xIndex, yIndex, titleHeight, config, calculatedConfig) {
    const left = config.widthBias + xIndex * (config.nodeWidth + config.nodeWidthBetween);
    const top = titleHeight + config.heightBias + yIndex * (config.nodeHeight + config.nodeHeightBetween);
    if(ComponentImageNodeDiagram.RIGHT === position || ComponentImageNodeDiagram.LEFT === position) {
      return {
        x: left + calculatedConfig.positionX[position],
        y: top + this.calculateCurrentPoint(config.nodeHeight, current, currents)
      }
    }
    else {
      return {
        x: left + this.calculateCurrentPoint(config.nodeWidth, current, currents),
        y: top + calculatedConfig.positionY[position]
      }
    }
  }
  
  getPosition(decidedPosition, calculatedPosition) {
    if('top' === decidedPosition) {
      return ComponentImageNodeDiagram.TOP;
    }
    else if('right' === decidedPosition) {
      return ComponentImageNodeDiagram.RIGHT;
    }
    else if('bottom' === decidedPosition) {
      return ComponentImageNodeDiagram.BOTTOM;
    }
    else if('left' === decidedPosition) {
      return ComponentImageNodeDiagram.LEFT;
    }
    else {
      return calculatedPosition;
    }
  }
  
  calculatePosition(connection, fromNode, toNode) {
    const position = {
      from: -1,
      to: -1
    };
    const xDiff = fromNode.xIndex - toNode.xIndex;
    const yDiff = fromNode.yIndex - toNode.yIndex;
    if(xDiff < 0) {
      position.from = this.getPosition(connection.fromPosition, ComponentImageNodeDiagram.RIGHT);
      position.to = this.getPosition(connection.toPosition, ComponentImageNodeDiagram.LEFT);
    }
    else if(xDiff > 0) {
      position.from = this.getPosition(connection.fromPosition, ComponentImageNodeDiagram.LEFT);
      position.to = this.getPosition(connection.toPosition, ComponentImageNodeDiagram.RIGHT);
    }
    else {
      if(yDiff < 0) {
        position.from = this.getPosition(connection.fromPosition, ComponentImageNodeDiagram.BOTTOM);
        position.to = this.getPosition(connection.toPosition, ComponentImageNodeDiagram.TOP);
      }
      else if(yDiff > 0) {
        position.from = this.getPosition(connection.fromPosition, ComponentImageNodeDiagram.TOP);
        position.to = this.getPosition(connection.toPosition, ComponentImageNodeDiagram.BOTTOM);
      }
    }
    return position;
  }
  
  calculateTextPoint(outPoint, inPoint) {
    return {
      x: (outPoint.x + inPoint.x) / 2,
      y: (outPoint.y + inPoint.y) / 2 - 2
    };
  }
  
  renderConnectionStart(connection, outPoint) {
    return (
      <circle cx={outPoint.x} cy={outPoint.y} r="3"/>
    );
  }
 
  renderConnectionEnd(connection, inPoint, position, slope) {
    if(!slope) {
      if(ComponentImageNodeDiagram.TOP === position) {
        return (
          <polygon points={`${inPoint.x}, ${inPoint.y} ${inPoint.x - 3}, ${inPoint.y - 6} ${inPoint.x + 3}, ${inPoint.y - 6}`}/>
        );
      }
      else if(ComponentImageNodeDiagram.RIGHT === position) {
        return (
          <polygon points={`${inPoint.x}, ${inPoint.y} ${inPoint.x + 6}, ${inPoint.y - 3} ${inPoint.x + 6}, ${inPoint.y + 3}`}/>
        );
      }
      else if(ComponentImageNodeDiagram.BOTTOM === position) {
        return (
          <polygon points={`${inPoint.x}, ${inPoint.y} ${inPoint.x - 3}, ${inPoint.y + 6} ${inPoint.x + 3}, ${inPoint.y + 6}`}/>
        );
      }
      else if(ComponentImageNodeDiagram.LEFT === position) {
        return (
          <polygon points={`${inPoint.x}, ${inPoint.y} ${inPoint.x - 6}, ${inPoint.y - 3} ${inPoint.x - 6}, ${inPoint.y + 3}`}/>
        );
      }
    }
    else {
      return (
        <circle cx={inPoint.x} cy={inPoint.y} r="3"/>
      );
    }
  }

  calculateLineConnectingPoints(outPoint, inPoint) {
    const points = [];
    if(undefined !== outPoint && undefined !== inPoint) {
      if(outPoint.x === inPoint.x) {
        if(outPoint.y !== inPoint.y) {
          points.push(outPoint, inPoint);
        }
      }
      else if(outPoint.y === inPoint.y) {
        if(outPoint.x !== inPoint.x) {
          points.push(outPoint, inPoint);
        }
      }
      else {
        points.push(outPoint, {x: outPoint.x, y: inPoint.y}, inPoint);
      }
    }
    return points;
  }
  
  calculateLinePoints(point, indexXFrom, indexXTo, indexYFrom, indexYTo, positionFrom, positionTo, config, calculatedConfig, direction) {
    const points = [];
    const hDiff = calculatedConfig.halfNodeHeightBetween / 5;
    const wDiff = calculatedConfig.halfNodeWidthBetween / 5;
    const MoreIndex = [1, 0];
    const LessIndex = [0, 1];
    const MoreH = [hDiff, -hDiff];
    const LessH = [-hDiff, hDiff];
    const MoreW = [-wDiff, wDiff];
    const LessW = [wDiff, -wDiff];
    const h = hDiff;
    const w = wDiff;
    if(ComponentImageNodeDiagram.TOP === positionFrom) {
      if(indexXFrom === indexXTo) {
        if(indexYFrom - 1 === indexYTo && ComponentImageNodeDiagram.BOTTOM === positionTo) {
          points.push(point, {x:point.x, y:point.y - calculatedConfig.halfNodeHeightBetween});
        }
        else if(indexYFrom < indexYTo) {
          points.push(point, {x:point.x, y:point.y - calculatedConfig.halfNodeHeightBetween + h}, {x:config.widthBias + (indexXFrom + LessIndex[direction]) * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + LessW[direction], y:point.y - calculatedConfig.halfNodeHeightBetween + MoreH[0]});
        }
        else {
          points.push(point, {x:point.x, y:point.y - calculatedConfig.halfNodeHeightBetween + h}, {x:config.widthBias + (indexXFrom + MoreIndex[direction]) * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + MoreW[direction], y:point.y - calculatedConfig.halfNodeHeightBetween + MoreH[0]});          
        }
      }
      else if(indexXFrom < indexXTo) {
        points.push(point, {x:point.x, y:point.y - calculatedConfig.halfNodeHeightBetween + h}, {x:config.widthBias + (indexXFrom + 1) * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + w, y:point.y - calculatedConfig.halfNodeHeightBetween + h});
      }
      else {
        points.push(point, {x:point.x, y:point.y - calculatedConfig.halfNodeHeightBetween + h}, {x:config.widthBias + indexXFrom * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + w, y:point.y - calculatedConfig.halfNodeHeightBetween + h});
      }
    }
    else if(ComponentImageNodeDiagram.RIGHT === positionFrom) {
      if(indexYFrom === indexYTo) {
        if(indexXFrom + 1 === indexXTo && ComponentImageNodeDiagram.LEFT === positionTo) {
          points.push(point, {x:point.x + calculatedConfig.halfNodeWidthBetween, y:point.y});
        }
        else if(indexXFrom < indexXTo) {
          points.push(point, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:point.y}, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:config.heightBias + (indexYFrom + LessIndex[direction]) * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + MoreH[direction]});          
        }
        else {
          points.push(point, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:point.y}, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:config.heightBias + (indexYFrom + MoreIndex[direction]) * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + LessH[direction]});          
        }
      }
      else if(indexYFrom < indexYTo) {
        points.push(point, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:point.y}, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:config.heightBias + (indexYFrom + 1)  * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + MoreH[direction]});
      }
      else {
        points.push(point, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:point.y}, {x:point.x + calculatedConfig.halfNodeWidthBetween - w, y:config.heightBias + indexYFrom * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + LessH[direction]});
      }
    }
    else if(ComponentImageNodeDiagram.BOTTOM === positionFrom) {
      if(indexXFrom === indexXTo) {
        if(indexYFrom + 1 === indexYTo && ComponentImageNodeDiagram.TOP === positionTo) {
          points.push(point, {x:point.x, y:point.y + calculatedConfig.halfNodeHeightBetween});
        }
        else if(indexYFrom < indexYTo) {
          points.push(point, {x:point.x, y:point.y + calculatedConfig.halfNodeHeightBetween - h}, {x:config.widthBias + (indexXFrom + LessIndex[direction]) * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + LessW[direction], y:point.y + calculatedConfig.halfNodeHeightBetween - MoreH[0]});
        }
        else {
          points.push(point, {x:point.x, y:point.y + calculatedConfig.halfNodeHeightBetween - h}, {x:config.widthBias + (indexXFrom + MoreIndex[direction]) * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + MoreW[direction], y:point.y + calculatedConfig.halfNodeHeightBetween - MoreH[0]});          
        }
      }
      else if(indexXFrom < indexXTo) {
        points.push(point, {x:point.x, y:point.y + calculatedConfig.halfNodeHeightBetween - h}, {x:config.widthBias + (indexXFrom + 1) * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + w, y:point.y + calculatedConfig.halfNodeHeightBetween - h});
      }
      else {
        points.push(point, {x:point.x, y:point.y + calculatedConfig.halfNodeHeightBetween - h}, {x:config.widthBias + indexXFrom * (config.nodeWidth + config.nodeWidthBetween) - calculatedConfig.halfNodeWidthBetween + w, y:point.y + calculatedConfig.halfNodeHeightBetween - h});
      }
    }
    else if(ComponentImageNodeDiagram.LEFT === positionFrom) {
      if(indexYFrom === indexYTo) {
        if(indexXFrom - 1 === indexXTo && ComponentImageNodeDiagram.RIGHT === positionTo) {
          points.push(point, {x:point.x - calculatedConfig.halfNodeWidthBetween, y:point.y});
        }
        else if(indexXFrom < indexXTo) {
          points.push(point, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:point.y}, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:config.heightBias + (indexYFrom + LessIndex[direction]) * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + MoreH[direction]});          
        }
        else {
          points.push(point, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:point.y}, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:config.heightBias + (indexYFrom + MoreIndex[direction]) * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + LessH[direction]});          
        }
      }
      else if(indexYFrom < indexYTo) {
        points.push(point, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:point.y}, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:config.heightBias + (indexYFrom + 1)  * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + MoreH[direction]});
      }
      else {
        points.push(point, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:point.y}, {x:point.x - calculatedConfig.halfNodeWidthBetween + w, y:config.heightBias + indexYFrom * (config.nodeHeight + config.nodeHeightBetween) - calculatedConfig.halfNodeHeightBetween + LessH[direction]});
      }
    }
    return points;
  }
 
  renderNoneSlopeLine(points, type) {
    const lines = [];
    for(let i = 1; i < points.length; ++i) {
      lines.push(<line key={i} className={type} x1={points[i - 1].x} y1={points[i - 1].y} x2={points[i].x} y2= {points[i].y} style={{strokeWidth:2}} />);
    }
    return lines;
  }
  
  renderNoneSlopeLines(outPoint, inPoint, outXIndex, inXIndex, outYIndex, inYIndex, position, config, calculatedConfig, type) {
    const startPoints = this.calculateLinePoints(outPoint, outXIndex, inXIndex, outYIndex, inYIndex, position.from, position.to, config, calculatedConfig, 1);
    const endPoints = this.calculateLinePoints(inPoint, inXIndex, outXIndex, inYIndex, outYIndex, position.to, position.from, config, calculatedConfig, 0);
    const connectingPoints = this.calculateLineConnectingPoints(startPoints[startPoints.length - 1], endPoints[endPoints.length - 1]);
    return (
      <g>
        {this.renderNoneSlopeLine(startPoints, type)}
        {this.renderNoneSlopeLine(endPoints, type)}
        {this.renderNoneSlopeLine(connectingPoints, type)}
      </g>
    );
  }
  
  renderConnection(connection, fromNode, toNode, titleHeight, config, calculatedConfig) {
    const position = this.calculatePosition(connection, fromNode, toNode);
    const currentOut = fromNode.currentOut[position.from]++;
    const currentIn = toNode.currentIn[position.to]++;
    const outPoint = this.calculatePoint(position.from, currentOut, fromNode.out[position.from], fromNode.xIndex, fromNode.yIndex, titleHeight, config, calculatedConfig);
    const inPoint = this.calculatePoint(position.to, currentIn, toNode.in[position.to], toNode.xIndex, toNode.yIndex, titleHeight, config, calculatedConfig);
    const textPoint = this.calculateTextPoint(outPoint, inPoint);
    let type = `seq_dia_protocol_${0 !== connection.type.length ? connection.type : 'unknown'}`;
    if('part' === connection.connectionType || 'part-bi-directional' === connection.connectionType) {
     type += ' seq_message_part';
    }
    let line = <line className={type} x1={outPoint.x} y1={outPoint.y} x2={inPoint.x} y2= {inPoint.y} style={{strokeWidth:2}} />;
    const slope = undefined === connection.slope || true === connection.slope;
    if(!slope) {
      line = this.renderNoneSlopeLines(outPoint, inPoint, fromNode.xIndex, toNode.xIndex, fromNode.yIndex, toNode.yIndex, position, config, calculatedConfig, type);
    }
    return (
      <g key={`key_${fromNode.xIndex}_${fromNode.yIndex}_${toNode.xIndex}_${toNode.yIndex}_${currentIn}_${currentOut}`}>
        {line}
        <text x={textPoint.x} y={textPoint.y} style={{fontSize:'12px',fontWeight:'bold',textAnchor:'middle'}}>
          {connection.data}
        </text>
        {this.renderConnectionStart(connection, outPoint)}
        {this.renderConnectionEnd(connection, inPoint, position.to, slope)}
      </g>
    );
  }
  
  renderConnections(connections, nodeMap, titleHeight, config) {
    const halfNodeWidth = config.nodeWidth / 2;
    const halfNodeHeight = config.nodeHeight / 2;
    const halfNodeWidthBetween = config.nodeWidthBetween / 2;
    const halfNodeHeightBetween = config.nodeHeightBetween / 2;
    const calculatedConfig = {
      positionX: [halfNodeWidth, config.nodeWidth, halfNodeWidth, 0],
      positionY: [0, halfNodeHeight, config.nodeHeight, halfNodeHeight],
      halfNodeWidthBetween: halfNodeWidthBetween,
      halfNodeHeightBetween: halfNodeHeightBetween
    };
    connections.forEach((connection) => {
      const fromNode = nodeMap.get(connection.fromNodeName);
      const toNode = nodeMap.get(connection.toNodeName);
      const position = this.calculatePosition(connection, fromNode, toNode);
      ++fromNode.out[position.from];
      ++toNode.in[position.to];
    });
    return connections.map((connection) => {
      const fromNode = nodeMap.get(connection.fromNodeName);
      const toNode = nodeMap.get(connection.toNodeName);
      return this.renderConnection(connection, fromNode, toNode, titleHeight, config, calculatedConfig);
    });
  }
  
  renderXml(xml) {
    return (
      <g dangerouslySetInnerHTML={{__html: xml}} />
    );
  }
  
  render() {
    const title = this.props.nodeDiagram.title;
    const nodeRows = this.props.nodeDiagram.nodeRows;
    const connections = this.props.nodeDiagram.connections;
    const config = this.props.nodeDiagram.dataNodeDiagramConfig;
    const groups = this.props.nodeDiagram.groups;
    const xml = this.props.nodeDiagram.xml;
    let columns = 0;
    const nodeMap = new Map();
    nodeRows.forEach((nodes, yIndex) => {
      nodes.forEach((node, xIndex) => {
        nodeMap.set(node.name, {
          xIndex: xIndex,
          yIndex: yIndex,
          in: [0, 0, 0, 0],
          out: [0, 0, 0, 0],
          currentIn: [0, 0, 0, 0],
          currentOut: [0, 0, 0, 0]
        });
      });
      if(nodes.length > columns) {
        columns = nodes.length;
      }
    });
    const titleHeight = ('' !== title && undefined !== title) ? ComponentImageNodeDiagram.TITLE_HEIGHT : 0;
    const nodeHeight = config.heightBias + nodeRows.length * config.nodeHeight + (nodeRows.length - 1) * config.nodeHeightBetween;
    const svgHeight = titleHeight + nodeHeight + config.heightBias;
    const svgWidth = 2 * config.widthBias + columns * config.nodeWidth + (columns - 1) * config.nodeWidthBetween;
    const viewBox = [0, 0, svgWidth, svgHeight].join(' ');
    const style = {};
    if(!config.border) {
      style.border = '0px';
    }
    if('default' !== config.backgroundColor) {
      style.backgroundColor = config.backgroundColor;
    }
    return (
      <svg className="markup_node" width={svgWidth} height={svgHeight} viewBox={viewBox} style={style}>
        {this.renderTitle(title, svgWidth / 2)}
        {this.renderGroups(groups, titleHeight, config)}
        {this.renderNodes(nodeRows, titleHeight, config)}
        {this.renderConnections(connections, nodeMap, titleHeight, config)}
        {this.renderXml(xml)}
      </svg>
    );
  }
}

ComponentImageNodeDiagram.TITLE_HEIGHT = 60;
ComponentImageNodeDiagram.TITLE_FONT_SIZE = 20;
ComponentImageNodeDiagram.TITLE_TOP = ComponentImageNodeDiagram.TITLE_FONT_SIZE + (ComponentImageNodeDiagram.TITLE_HEIGHT - ComponentImageNodeDiagram.TITLE_FONT_SIZE) / 2;


ComponentImageNodeDiagram.TOP = 0;
ComponentImageNodeDiagram.RIGHT = 1;
ComponentImageNodeDiagram.BOTTOM = 2;
ComponentImageNodeDiagram.LEFT = 3;

ComponentImageNodeDiagram.TEXT_BIAS_Y = 10;

