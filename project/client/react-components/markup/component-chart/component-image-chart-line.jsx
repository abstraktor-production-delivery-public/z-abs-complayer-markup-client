
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ComponentDataChartLine extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderTitle(title, x) {
    if(0 !== title) {
      return (
        <text x={x} y={ComponentDataChartLine.TITLE_TEXT} textAnchor="middle" style={{fontSize:ComponentDataChartLine.TITLE_FONT_SIZE,fontWeight:'bold'}}>
          {title}
        </text>  
      );
    }
  }

  renderGrid(config, calculated) {
    const grid = this.props.chartLine.grid;
    if(null !== grid) {
      if(undefined !== calculated) {
        const xAxis = this._getAxis('x');
        const yAxis = this._getAxis('y');
        const xLines = [];
        const yLines = [];
        if(undefined !== xAxis && undefined !== yAxis) {
          if(0 !== grid.dx) {
            const halfGrid = grid.dx / 2;
            const pt = this._calculatePoint(0, 0, config, calculated);
            xLines.push(
              <text key={`grid_y_text_0`} x={pt.x} y={pt.y + 12} textAnchor="middle" style={{fontSize:ComponentDataChartLine.GRID_FONT_SIZE,fontWeight:'bold'}}>
                {0}
              </text>
            );
            for(let x = grid.dx; x <= xAxis.topValue; x += grid.dx) {
              const ph1 = this._calculatePoint(x - halfGrid, 0, config, calculated);
              const ph2 = this._calculatePoint(x - halfGrid, yAxis.topValue, config, calculated);
              xLines.push(<line key={`grid_x_h_${x}`} x1={ph1.x} y1={ph1.y} x2={ph2.x} y2={ph2.y} style={{stroke:'#ebebeb',strokeWidth:'1px'}} />);
              const p1 = this._calculatePoint(x, 0, config, calculated);
              const p2 = this._calculatePoint(x, yAxis.topValue, config, calculated);
              xLines.push(<line key={`grid_x_${x}`} x1={p1.x} y1={p1.y} x2={p2.x} y2={p2.y} style={{stroke:'#cccccc',strokeWidth:'1px'}} />);
              const pt = this._calculatePoint(x, 0, config, calculated);
              xLines.push(
                <text key={`grid_x_text_${x}`} x={pt.x} y={pt.y + 12} textAnchor="middle" style={{fontSize:ComponentDataChartLine.GRID_FONT_SIZE,fontWeight:'bold'}}>
                  {x}
                </text>
              );
            }
          }
          if(0 !== grid.dy) {
            const halfGrid = grid.dy / 2;
            const pt = this._calculatePoint(0, 0, config, calculated);
            yLines.push(
              <text key={`grid_y_text_0`} x={pt.x - 10} y={pt.y + ComponentDataChartLine.GRID_FONT_SIZE_HALF - 1} textAnchor="middle" style={{fontSize:ComponentDataChartLine.GRID_FONT_SIZE,fontWeight:'bold'}}>
                {0}
              </text>
            );
            for(let y = grid.dy; y <= yAxis.topValue; y += grid.dy) {
              const ph1 = this._calculatePoint(0, y - halfGrid, config, calculated);
              const ph2 = this._calculatePoint(xAxis.topValue, y - halfGrid, config, calculated);
              yLines.push(<line key={`grid_y_h_${y}`} x1={ph1.x} y1={ph1.y} x2={ph2.x} y2={ph2.y} style={{stroke:'#ebebeb',strokeWidth:'1px'}} />);
              const p1 = this._calculatePoint(0, y, config, calculated);
              const p2 = this._calculatePoint(xAxis.topValue, y, config, calculated);
              yLines.push(<line key={`grid_y_${y}`} x1={p1.x} y1={p1.y} x2={p2.x} y2={p2.y} style={{stroke:'#cccccc',strokeWidth:'1px'}} />);
              const pt = this._calculatePoint(0, y, config, calculated);
              yLines.push(
                <text key={`grid_y_text_${y}`} x={pt.x - 10} y={pt.y + ComponentDataChartLine.GRID_FONT_SIZE_HALF - 1} textAnchor="middle" style={{fontSize:ComponentDataChartLine.GRID_FONT_SIZE,fontWeight:'bold'}}>
                  {y}
                </text>
              );
            }
          }
          return (
            <g>
              <g key={`grid_x`}>
                {xLines}
              </g>
              <g key={`grid_y`}>
                {yLines}
              </g>
            </g>
          );
        }
      }
    }
  }
  
  renderAxes(config, calculated) {
    if(undefined !== calculated) {
      const x = this._getAxis('x');
      const y = this._getAxis('y');
      if(undefined !== x && undefined !== y) {
        const fromAxis = this._calculatePoint(0, 0, config, calculated);
        const toXAxis = this._calculatePoint(x.topValue, 0, config, calculated);
        const toYAxis = this._calculatePoint(0, y.topValue, config, calculated);
        return (
          <g>
            <line x1={fromAxis.x} y1={fromAxis.y} x2={toXAxis.x} y2={toXAxis.y} style={{stroke:'#333333',strokeWidth:'1px'}} />
            <line x1={fromAxis.x} y1={fromAxis.y} x2={toYAxis.x} y2={toYAxis.y} style={{stroke:'#333333',strokeWidth:'1px'}} />
          </g>
        );
      }
    }
  }
  
  renderData(config, calculated) {
    if(undefined !== calculated) {
      const datas = this.props.chartLine.data;
      const allLines = [];
      let lineIndex = 0;
      datas.forEach((data) => {
        const lineDatas = data.datas;
        if(2 <= lineDatas.length) {
          const lines = [];
          let from = this._calculatePoint(lineDatas[0][0], lineDatas[0][1], config, calculated);
          for(let i = 1; i < lineDatas.length; ++i) {
            const to = this._calculatePoint(lineDatas[i][0], lineDatas[i][1], config, calculated);
            if(!isNaN(from.x) && !isNaN(from.y) && !isNaN(to.x) && !isNaN(to.y)) {
              lines.push(<line key={`point_${data.name}_${i}`} x1={from.x} y1={from.y} x2={to.x} y2={to.y} style={{stroke:ComponentDataChartLine.DEFAULT_COLORS[lineIndex] ,strokeWidth:'2px'}} />);
              from = to;
            }
          }
          allLines.push(
            <g key={`line_${data.name}`}>
              {lines}
            </g>
          );                   
        }
        ++lineIndex;
      });
      return (
        <g>
          {allLines}
        </g>
      );
    }
  }
  
  _getAxis(axis) {
    const axes = this.props.chartLine.axes;
    for(let i = 0; i < axes.length; ++i) {
      if(axis === axes[i].direction) {
        return axes[i];
      }
    }
  }
  
  _calculatePoint(x, y, config, calculated) {
    return {
      x: x * calculated.xScale + config.widthBias,
      y: ComponentDataChartLine.TITLE_HEIGHT + config.heightBias + config.height - y * calculated.yScale
    };
  }
  
  _calculate(config) {
    const x = this._getAxis('x');
    const y = this._getAxis('y');
    if(undefined !== x && undefined !== y) {
      return {
        xScale: config.width / x.topValue,
        yScale: config.height / y.topValue
      };
    }
  }
  
  render() {
    const title = this.props.chartLine.title;
    const config = this.props.chartLine.dataChartLineConfig;
    const calculated = this._calculate(config);
    const titleHeight = ('' !== title && undefined !== title) ? ComponentDataChartLine.TITLE_HEIGHT : 0;
    const svgWidth = 2 * config.widthBias + config.width;
    const chartHeight = 2 * config.heightBias + config.height;
    const svgHeight = titleHeight + chartHeight;
    const viewBox = [0, 0, svgWidth, svgHeight].join(' ');
    return (
      <svg className="markup_node" width={svgWidth} height={svgHeight} viewBox={viewBox}>
        {this.renderTitle(title, svgWidth / 2)}
        {this.renderGrid(config, calculated)}
        {this.renderAxes(config, calculated)}
        {this.renderData(config, calculated)}
      </svg>
    );
  }
}

ComponentDataChartLine.TITLE_HEIGHT = 40;
ComponentDataChartLine.TITLE_FONT_SIZE = 20;
ComponentDataChartLine.TITLE_TEXT = (ComponentDataChartLine.TITLE_HEIGHT + ComponentDataChartLine.TITLE_FONT_SIZE) / 2;

ComponentDataChartLine.DEFAULT_COLORS = ['Blue', 'Red', 'Yellow', 'Green'];

ComponentDataChartLine.GRID_FONT_SIZE = 10;
ComponentDataChartLine.GRID_FONT_SIZE_HALF = ComponentDataChartLine.GRID_FONT_SIZE / 2;

