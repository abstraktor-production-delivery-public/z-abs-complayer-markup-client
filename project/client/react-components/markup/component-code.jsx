
'use strict';

import CodeMirrorEditor from 'z-abs-complayer-codemirror-client/client/code-mirror-editor';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class ComponentCode extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  render(options) {
    const id = this.props.id ? `${this.props.id}_component_code_view` : 'component_code_view';
    return (
      <div>
        <p className="markup_code">{this.props.name}</p>
        <CodeMirrorEditor id={id} code={this.props.code} options={this.getOptions(this.props.type)} height="100%" />
      </div>
    );
  }
  
  getOptions(type) {
    return {
      lineNumbers: true,
      highlightActiveLineGutter: true,
      highlightActiveLine: false,
      closeBrackets: true,
      bracketMatching: true,
      drawSelection: true,
      foldGutter: true,
      
      tabKeyAction: 'indent', 
      tabSize: 2,
      indentType: 'spaces',
      indentUnit: 2,
      
      readOnly: true,
      autofocus: true,
      dragDrop: false,
      
      type
    };
  }
}
