
'use strict';


class LineObject {
  constructor(id, type, start, stop, element, top, line, linePosition, delta, percent, previousRealObject, nextRealObject) {
    this.id = id;
    this.type = type;
    this.start = start;
    this.stop = stop;
    this.element = element;
    this.top = top;
    this.line = line;
    this.linePosition = linePosition;
    this.delta = delta;
    this.percent = percent;
    this.previousRealObject = previousRealObject;
    this.nextRealObject = nextRealObject;
  }
}

module.exports = LineObject;
