
'use strict';


class ScrollData {
  constructor(line = 0, delta = 0.0, top = 0.0, length = Number.MAX_SAFE_INTEGER) {
    this.line = line;
    this.delta = delta;
    this.top = top;
    this.length = length;
  }
  
  setLength(length) {
    this.length = length + 2;
    this.setLine(this.line, this.delta);
  }
  
  setLineFirst() {
    this.line = 0
    this.delta = 0.0;
  }
  
  setLineLast() {
    this.line = this.length ;
    this.delta = 0.0;
  }
  
  setLine(line = 0, delta = 0.0) {
    if(line < 0) {
      this.line = 0;
      this.delta = 0;
    }
    else if(line >= 0 && line < this.length) {
      this.line = line;
      this.delta = delta;  
    }
    else if(line >= this.length) {
      this.line = this.length;
      this.delta = 0;
    }
  }
  
  setScroll(top) {
    this.top = top;
  }
  
  increment(delta = 0.0) {
    if(this.line < this.length) {
      ++this.line;
    }
    this.delta = delta;
  }
  
  stepLines(lines) {
    if(this.line + lines > this.length) {
      this.line = this.length;
    }
    else if(this.line + lines < 0) {
      this.line = 0;
    }
    else {
      this.line += lines;
    }
  }
  
  decrement(delta = 0.0) {
    if(1 <= this.line) {
      --this.line;
    }
    this.delta = delta;
  }
}

module.exports = ScrollData;
