
'use strict';

import { ActionStyleGet } from './action-style';
import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionStyleGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionStyleGet());
  }
}