
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionStyleGet extends Action {
  constructor() {
    super();
  }
}

export class ActionStyleUpdate extends Action {
  constructor(selectorText, onChange) {
    super(selectorText, onChange);
  }
}
