
'use strict';

const DataTestDataSystemGlobal = require('../../data/data-test-data/data-test-data-system-global');
const DataTestDataSystemLocal = require('../../data/data-test-data/data-test-data-system-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupTestDataSystem {
  stringify(dataTestDataGlobals, dataTestDataLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataTestDataSystemLocal(), dataTestDataLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataTestDataSystemGlobal(), dataTestDataGlobals, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataTestDataSystemGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataTestDataSystemLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupTestDataSystem();
