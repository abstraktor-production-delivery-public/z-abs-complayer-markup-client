
'use strict';

const DataTestDataEnvironmentGlobal = require('../../data/data-test-data/data-test-data-environment-global');
const DataTestDataEnvironmentLocal = require('../../data/data-test-data/data-test-data-environment-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupTestDataEnvironment {
  stringify(dataTestDataGlobals, dataTestDataLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataTestDataEnvironmentLocal(), dataTestDataLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataTestDataEnvironmentGlobal(), dataTestDataGlobals, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataTestDataEnvironmentGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataTestDataEnvironmentLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupTestDataEnvironment();
