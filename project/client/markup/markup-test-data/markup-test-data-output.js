
'use strict';

const DataTestDataOutputGlobal = require('../../data/data-test-data/data-test-data-output-global');
const DataTestDataOutputLocal = require('../../data/data-test-data/data-test-data-output-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupTestDataOutput {
  stringify(dataTestDataGlobals, dataTestDataLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataTestDataOutputLocal(), dataTestDataLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataTestDataOutputGlobal(), dataTestDataGlobals, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataTestDataOutputGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataTestDataOutputLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupTestDataOutput();
