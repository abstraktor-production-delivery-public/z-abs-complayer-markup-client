
'use strict';


class MarkupTablesParser {
  parse(markup, cbRows) {
    const rows = this._getRows(markup);
    if(undefined !== cbRows) {
      cbRows = cbRows(rows);
    }
    const formattedRows = this._formatRows(rows);
    const objectTables = this._getObjectTables(formattedRows);
    return {
      success: objectTables.success,
      rows: formattedRows,
      objects: objectTables.objects,
      config: undefined
    };
  }
  
  _getRows(markup) {
    let index = 0;
    let nextIndex = 0;
    const rows = [];
    const length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex));
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length));
        index = length;
      }
    }
    return rows;
  }
  
  _formatRows(rows) {
    const formattedRows = [];
    rows.forEach((row, rowIndex) => {
      let trimmedRow = row.trim();
      const commentOut = trimmedRow.startsWith('#');
      if(commentOut) {
        trimmedRow = trimmedRow.substring(1).trim();
      }
      const columns = trimmedRow.split('|');
      if(1 === columns.length) {
        formattedRows.push({
          object: false,
          empty: true,
          commentOut: commentOut,
          row: rowIndex,
          columns: [],
          success: 0 === columns[0].length
        });  
      }
      else if(columns.length >= 2) {
        const success = 0 === columns[0].length && 0 === columns[columns.length - 1].length;
        columns.splice(columns.length - 1, 1);
        columns.splice(0, 1);
        let empty = true;
        columns.forEach((column, columnIndex, array) => {
          const value = column.trim();
          array[columnIndex] = value;
          empty &&= 0 === value.length;
        });
        formattedRows.push({
          object: true,
          empty: empty,
          commentOut: commentOut,
          row: rowIndex,
          columns: columns,
          success: success
        });
      }
    });
    return formattedRows;
  }
  
  _getObjectTables(rows) {
    const objectTables = {
      success: true,
      objects: []
    };
    let objectRows = undefined;
    let rowlength = 0;
    rows.forEach((row) => {
      if(row.object) {
        if(undefined === objectRows) {
          objectRows = [];
          objectRows.push(row);
          objectTables.objects.push(objectRows);
          if(1 !== row.columns.length) {
            row.success = false;
            objectTables.success = false;
          }
        }
        else {
          if(!row.empty) {
            objectRows.push(row);
          }
          if(0 === rowlength) {
            rowlength = row.columns.length;
          }
          else if(row.columns.length != rowlength) {
            row.success = false;
            objectTables.success = false;
          }
        }
      }
      else {
        objectRows = undefined;
        rowlength = 0;
      }
    });
    return objectTables;
  }
}

module.exports = new MarkupTablesParser();
