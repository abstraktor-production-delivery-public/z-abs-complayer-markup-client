
'use strict';

const DataNodeDiagram = require('../data/data-node-diagram/data-node-diagram');
const DataNodeDiagramNode = require('../data/data-node-diagram/data-node-diagram-node');
const DataNodeDiagramConnection = require('../data/data-node-diagram/data-node-diagram-connection');
const DataNodeDiagramGroup = require('../data/data-node-diagram/data-node-diagram-group');
const DataNodeDiagramConfig = require('../data/data-node-diagram/data-node-diagram-config');


class MarkupNodeDiagram {
  markupName() {
    return 'node';
  }
  
  stringify(dataNodeDiagram) {
    let stringified = '';
    if('' !== dataNodeDiagram.title) {
      stringified += `${MarkupNodeDiagram.TAG_TITLE} ${dataNodeDiagram.title}\n`;
    }
    const dataNodeDiagramConfig = dataNodeDiagram.dataNodeDiagramConfig;
    const keys = Reflect.ownKeys(dataNodeDiagramConfig);
    stringified += 'Config(';
    if(1 <= keys.length) {
      stringified += `${keys[0]}: ${Reflect.get(dataNodeDiagramConfig, keys[0])}`;
      for(let i = 1; i < keys.length; ++i) {
        stringified += `, ${keys[i]}: ${Reflect.get(dataNodeDiagramConfig, keys[i])}`;
      }
    }
    stringified += ')\n\n';
    if(1 <= dataNodeDiagram.nodeRows.length) {
      for(let i = 0; i < dataNodeDiagram.nodeRows.length; ++i) {
        stringified += 'Nodes[';
        stringified += this._stringifyNodeParameter(dataNodeDiagram.nodeRows[i][0]);
        for(let j = 1; j < dataNodeDiagram.nodeRows[i].length; ++j) {
          stringified += `, ${this._stringifyNodeParameter(dataNodeDiagram.nodeRows[i][j])}`;
        }
        stringified += ']\n';
      }
      stringified += '\n';
    }
    else {
      stringified += 'Nodes[]\n\n';
    }
    if(0 !==  dataNodeDiagram.groups.length) {
      stringified += 'Groups[';
      if(1 <= dataNodeDiagram.groups.length) {
        stringified += this._stringifyGroupParameter(dataNodeDiagram.groups[0]);
        for(let i = 1; i < dataNodeDiagram.groups.length; ++i) {
          stringified += `, ${this._stringifyGroupParameter(dataNodeDiagram.groups[i])}`;
        }
      }
      stringified += ']\n';
    }
    for(let i = 0; i < dataNodeDiagram.connections.length; ++i) {
      const connection = dataNodeDiagram.connections[i];
      if(-1 !== connection.fromXIndex && -1 !== connection.fromYIndex && -1 !== connection.toXIndex && -1 !== connection.toYIndex)
      {
        const fromNode = this._getNodeFromName(dataNodeDiagram, connection.fromNodeName);
        const toNode = this._getNodeFromName(dataNodeDiagram, connection.toNodeName);
        if(null !== fromNode && null !== toNode)
        {
          const type = connection.type.trim();
          const fromPosition = connection.fromPosition.trim();
          const toPosition = connection.toPosition.trim();
          const slope = undefined !== connection.slope ? (connection.slope ? 'true' : 'false') : '';
          let connectionParameters = [];
          if(0 !== slope.length) {
            connectionParameters.unshift(type, fromPosition, toPosition, slope);
          }
          else if(0 !== toPosition.length) {
            connectionParameters.unshift(type, fromPosition, toPosition);
          }
          else if(0 !== fromPosition.length) {
            connectionParameters.unshift(type, fromPosition);
          }
          else if(0 !== type.length) {
            connectionParameters.unshift(type);
          }
          stringified += `\n${fromNode.name} ${this._stringifyConnectionType(connection)} ${toNode.name}[${connectionParameters.join(', ')}]: ${connection.data}`;
          continue;
        }
      }
      stringified += `\n${connection.data}`
    }
    if(null !== dataNodeDiagram.xml) {
      stringified += '\n' + dataNodeDiagram.xml;
    }
    return stringified;
  }
  
  _stringifyGroupParameter(group) {
    let stringify = group.name;
    const color = group.color.trim();
    if(-1 !== group.leftStartIndex && -1 !== group.leftStopIndex && -1 !== group.topStartIndex && -1 !== group.topStopIndex) {
      stringify += `(${group.leftStartIndex}; ${group.leftStopIndex}; ${group.topStartIndex}; ${group.topStopIndex}`;
      if(0 !== color.length) {
        stringify += `; ${color}`;
      }
      stringify += `)`;
    }
    return stringify;
  }
  
  _stringifyNodeParameter(node) {
    let stringify = node.name;
    const type = node.type.trim();
    const corners = node.corners;
    const color = node.color.trim();
    let nodeParameters = [];
    if(0 !== color.length) {
      nodeParameters.unshift(type, corners, color);
    }
    else if(0 !== corners.length) {
      nodeParameters.unshift(type, corners);
    }
    else if(0 !== type.length) {
      nodeParameters.unshift(type);
    }
    if(0 !== nodeParameters.length) {
      stringify += `(${nodeParameters.join('; ')})`;
    }
    return stringify;
  }
    
  parse(markup) {
    const parsed = this._getRows(markup);
    const rows = parsed.rows;
    const dataNodeDiagram = new DataNodeDiagram();
    rows.forEach((row) => {
      this._parseRow(dataNodeDiagram, row);
    });
    dataNodeDiagram.addXml(parsed.xml);
    return dataNodeDiagram;
  }
  
  _parseRow(dataNodeDiagram, row) {
    const trimRow = row.trim();
    if(trimRow.startsWith(MarkupNodeDiagram.TAG_TITLE)) {
      dataNodeDiagram.setTitle(trimRow.substr(MarkupNodeDiagram.TAG_TITLE_LENGTH).trim());
    }
    else if(trimRow.startsWith('Nodes')) {
      let index = 0;
      while(true) {
        const start = trimRow.indexOf('[', index);
        if(-1 === start) {
          break;
        }
        const stop = trimRow.indexOf(']', start + 1);
        const nodeNames = trimRow.substring(start + 1, -1 !== stop ? stop : undefined).split(',');
        if(null !== nodeNames) {
          dataNodeDiagram.addNodeRow();
          nodeNames.forEach((nodeName) => {
            let trimNode = '';
            let trimNodeName = '';
            let trimNodeType = '';
            let trimNodeCorners = '';
            let trimNodeColor = '';
            if(nodeName.startsWith(',') && nodeName.endsWith(',')) {
              trimNode = nodeName.substring(1, nodeName.length - 1).trim();
            }
            else {
              trimNode = nodeName.trim();
            }
            const startTrimNode = trimNode.indexOf('(');
            const stopTrimNode = trimNode.indexOf(')', -1 !== startTrimNode ? startTrimNode + 1 : undefined);
            if(-1 !== startTrimNode && -1 !== stopTrimNode) {
              const nodeParameters = trimNode.substring(startTrimNode + 1, -1 !== stopTrimNode ? stopTrimNode : undefined).split(';');
              if(null !== nodeParameters) {
                trimNodeName = trimNode.substring(0, startTrimNode);
                if(1 <= nodeParameters.length) {
                  trimNodeType = nodeParameters[0].trim();
                }
                if(2 <= nodeParameters.length) {
                  const Corners = Number.parseInt(nodeParameters[1].trim());
                  if(!Number.isNaN(Corners)) {
                    trimNodeCorners = Corners;
                  }
                }
                if(3 <= nodeParameters.length) {
                  trimNodeColor = nodeParameters[2].trim();
                }
              }
            }
            else {
              trimNodeName = trimNode;
            }
            dataNodeDiagram.addNode(new DataNodeDiagramNode(trimNodeName, trimNodeType, trimNodeCorners, trimNodeColor));
          });
        }
        if(-1 === stop) {
          break;
        }
        else {
          index = stop;
        }
      }
    }
    else if(trimRow.startsWith('Groups')) {
      let start = trimRow.indexOf('[');
      let stop = trimRow.indexOf(']', start);
      if(-1 !== start && -1 !== stop) {
        const groups = trimRow.substring(start + 1, -1 !== stop ? stop : undefined).split(',');
        if(null !== groups) {
          groups.forEach((group) => {
            let groupTrim = group.trim();
            let startGroupIndex = groupTrim.indexOf('(');
            let stopGroupIndex = groupTrim.indexOf(')', startGroupIndex);
            let groupName = '';
            let groupLeftStartIndex = -1;
            let groupLeftStopIndex = -1;
            let groupTopStartIndex = -1;
            let groupTopStopIndex = -1;
            let color = '';
            if(-1 !== startGroupIndex && -1 !== stopGroupIndex) {
              groupName = groupTrim.substring(0, startGroupIndex);
              const groupParameters = groupTrim.substring(startGroupIndex + 1, -1 !== stopGroupIndex ? stopGroupIndex : undefined).split(';');
              if(5 <= groupParameters.length) {
                color = groupParameters[4].trim();
              }
              if(4 <= groupParameters.length) {
                const groupTopStopIndex_ = Number.parseInt(groupParameters[3].trim());
                if(!Number.isNaN(groupTopStopIndex_)) {
                  groupTopStopIndex = groupTopStopIndex_;
                }
                const groupTopStartIndex_ = Number.parseInt(groupParameters[2].trim());
                if(!Number.isNaN(groupTopStartIndex_)) {
                  groupTopStartIndex = groupTopStartIndex_;
                }
                const groupLeftStopIndex_ = Number.parseInt(groupParameters[1].trim());
                if(!Number.isNaN(groupLeftStopIndex_)) {
                  groupLeftStopIndex = groupLeftStopIndex_;
                }
                const groupLeftStartIndex_ = Number.parseInt(groupParameters[0].trim());
                if(!Number.isNaN(groupLeftStartIndex_)) {
                  groupLeftStartIndex = groupLeftStartIndex_;
                }
              }
            }
            dataNodeDiagram.addGroup(new DataNodeDiagramGroup(groupName, groupLeftStartIndex, groupLeftStopIndex, groupTopStartIndex, groupTopStopIndex, color));
          });
        }
      }
    }
    else if(trimRow.startsWith('Config')) {
      const start = trimRow.indexOf('(', 0);
      if(-1 === start) {
        return;
      }
      const stop = trimRow.indexOf(')', start + 1);
      const configParameters = trimRow.substring(start + 1, -1 !== stop ? stop : undefined).split(',');
      if(null !== configParameters) {
        const config = {};
        configParameters.forEach((configParameter) => {
          const parameters = configParameter.split(':');
          if(2 === parameters.length) {
            const parameterName = parameters[0].trim();
            const parameterValue = parameters[1].trim();
            const numberValue = Number.parseInt(parameterValue);
            if(!Number.isNaN(numberValue)) {
              dataNodeDiagram.addConfig(parameterName, numberValue);
            }
            else if('true' === parameterValue) {
              dataNodeDiagram.addConfig(parameterName, true);
            }
            else if('false' === parameterValue) {
              dataNodeDiagram.addConfig(parameterName, false);
            }
            else {
              dataNodeDiagram.addConfig(parameterName, parameterValue);
            }
          }
        });
      }
    }
    else {
      let type = '';
      let fromPosition = '';
      let toPosition = '';
      let data = '';
      let slope;
      let typeStartIndex = trimRow.indexOf('[');
      let typeStopIndex = trimRow.indexOf(']');
      if(-1 !== typeStartIndex && -1 !== typeStopIndex) {
        const types = trimRow.slice(typeStartIndex + 1, typeStopIndex).split(',');
        if(null !== types) {
          if(1 <= types.length) {
            type = types[0].trim();
          }
          if(2 <= types.length) {
            fromPosition = types[1].trim();
          }
          if(3 <= types.length) {
            toPosition = types[2].trim();
          }
          if(4 <= types.length) {
            const slope_ = types[3].trim(); 
            slope = slope_ === 'true' ? true : (slope_ === 'false') ? false : undefined;
          }
        }
      }
      const dataIndex = trimRow.indexOf(':');
      if(-1 !== dataIndex) {
        data = trimRow.substring(dataIndex + 1, trimRow.length).trim();
      }
      const connectionType = this._parseConnectionType(trimRow);
      if(null !== connectionType) {
        const from = trimRow.substring(0, connectionType.index).trim();
        const to = trimRow.substring(connectionType.index + connectionType.size, typeStartIndex).trim();
        const fromNode = this._getNodeFromName(dataNodeDiagram, from);
        const toNode = this._getNodeFromName(dataNodeDiagram, to);
        if(null !== fromNode && null !== toNode) {
          dataNodeDiagram.addConnection(new DataNodeDiagramConnection(data, type, connectionType.type, fromNode, toNode, fromPosition, toPosition, slope));
        }
      }
    }
  }
  
  _getRows(markup) {
    let parsedXml = null;
    const xmls = markup.match(/<[^>]+>|\+/g);
    if(null !== xmls) {
      if(1 === xmls.length) {
        parsedXml = xmls[0];
      }
      else {
        const start = markup.indexOf(xmls[0]);
        const lastXml = xmls[xmls.length - 1];
        const stop = markup.lastIndexOf(lastXml);
        parsedXml = markup.substring(start, stop + lastXml.length);
      }
    }
    let index = 0;
    let nextIndex = 0;
    let rows = [];
    let length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex).trim());
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length).trim());
        index = length;
      }
    }
    return {
      rows: rows,
      xml: parsedXml
    };
  }
  
  _stringifyConnectionType(connection) {
    for(let i = 0; i < MarkupNodeDiagram.CONNECTION.length; ++i) {
      if(MarkupNodeDiagram.CONNECTION[i].type === connection.connectionType) {
        return MarkupNodeDiagram.CONNECTION[i].sign;
      }
    }
  }
  
  _parseConnectionType(row) {
    for(let i = 0; i < MarkupNodeDiagram.CONNECTION.length; ++i) {
      const index = row.indexOf(MarkupNodeDiagram.CONNECTION[i].sign);
      if(-1 !== index) {
        return {
          sign: MarkupNodeDiagram.CONNECTION[i].sign,
          type: MarkupNodeDiagram.CONNECTION[i].type,
          size: MarkupNodeDiagram.CONNECTION[i].sign.length,
          index: index
        };
      }
    }
    return null;
  }
  
  _getNodeFromName(dataNodeDiagram, nodeName) {
    for(let i = 0; i < dataNodeDiagram.nodeRows.length; ++i) {
      for(let j = 0; j < dataNodeDiagram.nodeRows[i].length; ++j) {
        if(dataNodeDiagram.nodeRows[i][j].name === nodeName) {
          return dataNodeDiagram.nodeRows[i][j];
        }
      }
    }
    return null;
  }
}

MarkupNodeDiagram.TAG_TITLE = 'Title:';
MarkupNodeDiagram.TAG_TITLE_LENGTH = MarkupNodeDiagram.TAG_TITLE.length;
MarkupNodeDiagram.START = -1;
MarkupNodeDiagram.OPENED = -2;
MarkupNodeDiagram.CLOSED = -3;
MarkupNodeDiagram.COMMA = -4;
MarkupNodeDiagram.FOUND_TYPE = 0;
MarkupNodeDiagram.FOUND_INDEX = 1;
MarkupNodeDiagram.END = 10000;

MarkupNodeDiagram.CONNECTION = [
  {
    sign: '=>',
    type: 'normal'
  },
  {
    sign: '<=>',
    type: 'normal-bi-directional'
  },
  {
    sign: '<=',
    type: 'normal-reverse'
  },
  {
    sign: '->',
    type: 'part'
  },
  {
    sign: '<->',
    type: 'part-bi-directional'
  },
  {
    sign: '<-',
    type: 'part-reverse'
  },
  {
    sign: '-o',
    type: 'connect'
  },
  {
    sign: '-x',
    type: 'disconnect'
  }
];


module.exports = new MarkupNodeDiagram();
