
'use strict';

import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';


class MarkupDocumentationAnchor {
  constructor() {
    this.key = 'ANCHOR';
    this.type = 'markup_anchor';
  }
  
  stringify(dataAnchor) {
    return `${this.key}={"id":"${dataAnchor.id}","visible":${dataAnchor.visible}}`;
  }
  
  parse(markup) {
    let dataAnchor = null;
    try {
      dataAnchor = JSON.parse(markup);
    } 
    catch(err) {}
    if(dataAnchor && dataAnchor.id) {
      if(undefined === dataAnchor.visible || 'boolean' !== typeof dataAnchor.visible) {
        dataAnchor.visible = false;
      }
      return dataAnchor;
    }
    return {
      id: GuidGenerator.create(),
      visible: true
    };
  }
}


module.exports = new MarkupDocumentationAnchor() ;
