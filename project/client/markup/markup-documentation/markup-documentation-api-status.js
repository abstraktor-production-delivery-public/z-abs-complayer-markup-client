
'use strict';


class MarkupDocumentationApiStatus {
  constructor() {
    this.key = 'API-STATUS';
    this.type = 'markup_api_status';
  }
  
  stringify(dataDocStatus) {
    let status = '';
    if(MarkupDocumentationApiStatus.DEPRECATED === dataDocStatus.status) {
      status = 'DEPRECATED';
    }
    else if(MarkupDocumentationApiStatus.EXPERIMENTAL === dataDocStatus.status) {
      status = 'EXPERIMENTAL';
    }
    else if(MarkupDocumentationApiStatus.STABLE === dataDocStatus.status) {
      status = 'STABLE';
    }
    else if(MarkupDocumentationApiStatus.LEGACY === dataDocStatus.status) {
      status = 'LEGACY';
    }
    else {
      status = 'DEPRECATED';
    }
    return `${this.key}=${status}, ${dataDocStatus.comment}`;
  }
  
  parse(markup) {
    const parameters = markup.split(',');
    parameters.forEach((parameter, index) => {
      parameters[index] = parameter.trim();
    });
    let status = 0;
    if(1 <= parameters.length) {
      const parameter = parameters[0];
      if('DEPRECATED' === parameter) {
        status = MarkupDocumentationApiStatus.DEPRECATED;
      }
      else if('EXPERIMENTAL' === parameter) {
        status = MarkupDocumentationApiStatus.EXPERIMENTAL;
      }
      else if('STABLE' === parameter) {
        status = MarkupDocumentationApiStatus.STABLE;
      }
      else if('LEGACY' === parameter) {
        status = MarkupDocumentationApiStatus.LEGACY;
      }
      else {
        status = MarkupDocumentationApiStatus.DEPRECATED;
      }
    }
    return {
      status: status,
      comment: 2 <= parameters.length ? parameters[1] : ''
    };
  }
}

MarkupDocumentationApiStatus.DEPRECATED = 0;
MarkupDocumentationApiStatus.EXPERIMENTAL = 1;
MarkupDocumentationApiStatus.STABLE = 2;
MarkupDocumentationApiStatus.LEGACY = 3;

module.exports = new MarkupDocumentationApiStatus() ;
