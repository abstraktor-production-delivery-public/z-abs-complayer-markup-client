
'use strict';

import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';


class MarkupDocumentationLocalNote {
  constructor() {
    this.key = 'NOTE';
    this.type = 'markup_local_note';
  }
  
  stringify(dataLocalNote) {
    return `${this.key}={"guid":"${dataLocalNote.guid}"}`;
  }
  
  parse(markup) {
    let dataLocalNote = null;
    try {
      dataLocalNote = JSON.parse(markup);
    } 
    catch(err) {}
    if(dataLocalNote && dataLocalNote.guid) {
      return dataLocalNote;
    }
    return {
      guid: GuidGenerator.create()
    };
  }
}


module.exports = new MarkupDocumentationLocalNote() ;
