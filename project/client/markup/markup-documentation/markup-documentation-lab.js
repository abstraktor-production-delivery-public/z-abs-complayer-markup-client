
'use strict';

const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupDocumentationLab {
  stringify(lab) {
    return lab;
  }
  
  parse(markup) {
    return markup;
  }
}


module.exports = new MarkupDocumentationLab();
