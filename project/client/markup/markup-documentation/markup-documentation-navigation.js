
'use strict';

const DataDocumentationNavigation = require('../../data/data-documentation/data-documentation-navigation');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupDocumentationNavigation {
  stringify(navigations) {
    this._handleStacks(navigations);
    const navigation = navigations.map((navigation) => {
      let nameLength = 0;
      let linkLength = 0;
      navigation.refs.forEach((ref) => {
        nameLength = Math.max(nameLength, ref.name.length);
        linkLength = Math.max(linkLength, ref.link.length);
      });
      const headingLength = Math.max(nameLength + linkLength + 1, navigation.heading.length);
      const head = `|${navigation.heading}${' '.repeat(headingLength - navigation.heading.length)}|`;
      const rows = navigation.refs.map((ref) => {
        return `|${ref.name}${' '.repeat(nameLength - ref.name.length)}|${ref.link}${' '.repeat(linkLength - ref.link.length)}|`;
      });
      return `${head}\n${rows.join('\n')}`
    });
    return `${navigation.join('\n\n')}`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    const navigations = [];
    const uniqueLinks = new Map();
    let result = true;
    objectTables.objects.forEach((objectTable) => {
      if(1 <= objectTable.length) {
        let heading;
        if(1 === objectTable[0].columns.length) {
          heading = objectTable[0].columns[0];
        }
        else {
          return;
        }
        const navigation = {
          heading: heading,
          refs: []
        };
        navigations.push(navigation);
        if(2 <= objectTable.length) {
          for(let i = 1; i < objectTable.length; ++i) {
            if(2 === objectTable[i].columns.length) {
              const link = objectTable[i].columns[1];
              if(uniqueLinks.has(link)) {
                result = false;
                objectTables.rows.forEach((row) => {
                  if(2 === row.columns.length) {
                    if(link === row.columns[1]) {
                      row.success = false;
                    }
                  }
                });
              }
              else {
                uniqueLinks.set(link);
              }
              navigation.refs.push({
                name: objectTable[i].columns[0],
                link: link
              });
            }
          }
        }
      }
    });
    if(result) {
      return {
        success: true,
        navigations: navigations
      };
    }
    else {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
  }
  
  _handleStacks(navigations) {
    navigations.findIndex((navigation) => {
      const refIndex = navigation.refs.findIndex((ref) => {
        return null !== ref.link.match(/^\[\[stack\-[a-z-]+\]\]/);
      });
      if(-1 !== refIndex) {
        navigation.refs = [{
          name: '[[stack]]',
          link: ''
        }];
      }
    });
  }
}

module.exports = new MarkupDocumentationNavigation();
