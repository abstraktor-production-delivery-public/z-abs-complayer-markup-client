
'use strict';


class MarkupDocumentationDocumentationStatus {
  constructor() {
    this.key = 'DOC-STATUS';
    this.type = 'markup_documentation_status';
  }
  
  stringify(dataDocStatus) {
    let status = '';
    if(MarkupDocumentationDocumentationStatus.NOT_YET === dataDocStatus.status) {
      status = 'NOT-YET';
    }
    else if(MarkupDocumentationDocumentationStatus.PARTLY === dataDocStatus.status) {
      status = 'PARTLY';
    }
    else if(MarkupDocumentationDocumentationStatus.MOSTLY === dataDocStatus.status) {
      status = 'MOSTLY';
    }
    else if(MarkupDocumentationDocumentationStatus.FULLY === dataDocStatus.status) {
      status = 'FULLY';
    }
    else {
      status = 'NOT-YET';
    }
    return `${this.key}=${status}, ${dataDocStatus.comment}`;
  }
  
  parse(markup) {
    const parameters = markup.split(',');
    parameters.forEach((parameter, index) => {
      parameters[index] = parameter.trim();
    });
    let status = 0;
    if(1 <= parameters.length) {
      const parameter = parameters[0];
      if('NOT-YET' === parameter) {
        status = MarkupDocumentationDocumentationStatus.NOT_YET;
      }
      else if('PARTLY' === parameter) {
        status = MarkupDocumentationDocumentationStatus.PARTLY;
      }
      else if('MOSTLY' === parameter) {
        status = MarkupDocumentationDocumentationStatus.MOSTLY;
      }
      else if('FULLY' === parameter) {
        status = MarkupDocumentationDocumentationStatus.FULLY;
      }
      else {
        status = MarkupDocumentationDocumentationStatus.NOT_YET;
      }
    }
    return {
      status: status,
      comment: 2 <= parameters.length ? parameters[1] : ''
    };
  }
}

MarkupDocumentationDocumentationStatus.NOT_YET = 0;
MarkupDocumentationDocumentationStatus.PARTLY = 1;
MarkupDocumentationDocumentationStatus.MOSTLY = 2;
MarkupDocumentationDocumentationStatus.FULLY = 3;

module.exports = new MarkupDocumentationDocumentationStatus() ;
