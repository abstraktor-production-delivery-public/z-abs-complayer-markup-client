
'use strict';

const DataDocumentationNavigation = require('../../data/data-documentation/data-documentation-navigation');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');
const MarkupDocumentationNavigation = require('./markup-documentation-navigation');
const MarkupDocumentationPage = require('./markup-documentation-page');
const DataDocumentation = require('../../data/data-documentation/data-documentation');


class MarkupDocumentation {
  stringify(documentation, currentPage) {
    let results = [];
    results.push(MarkupDocumentationNavigation.stringify(documentation.navigations));
    results.push(MarkupDocumentationPage.stringify(documentation.page));
    return `${results.join('\n\n')}\n`;
  }
  
  parse(markup) {
    let documentation = new DataDocumentation();
    let foundParts = this._getparse(markup);
    foundParts.forEach((part) => {
      if(MarkupDocumentation.NAVIGATION_START === part.type) {
       // documentation.addNavigation(MarkupDocumentationNavigation.parse(part.value));
      }
      else if(MarkupDocumentation.PAGE_START === part.type) {
        documentation.setPage(MarkupDocumentationPage.parse(part.value).values);
      }
    });
    return {
      success: true,
      result: documentation
    };
  }
  
  _getparse(markup, index = 0, foundParts = []) {    
    let indexStart = markup.indexOf(MarkupDocumentation.PART_START, index);
    let indexStop = markup.indexOf(MarkupDocumentation.PART_STOP, indexStart + MarkupDocumentation.PART_START_LENGTH);
    if(-1 === indexStart || -1 === indexStop) {
      return foundParts;
    }
    else {
      if(markup.substr(indexStart, MarkupDocumentation.NAVIGATION_START_LENGTH).startsWith(MarkupDocumentation.NAVIGATION_START)) {
        foundParts.push({
          type: MarkupDocumentation.NAVIGATION_START,
          value: markup.substring(indexStart + MarkupDocumentation.NAVIGATION_START_LENGTH, indexStop).trim()
        });
      }
      else if(markup.substr(indexStart, MarkupDocumentation.PAGE_START_LENGTH).startsWith(MarkupDocumentation.PAGE_START)) {
        foundParts.push({
          type: MarkupDocumentation.PAGE_START,
          value: markup.substring(indexStart + MarkupDocumentation.PAGE_START_LENGTH, indexStop).trim()
        });
      }
     }
    return this._getparse(markup, indexStop + MarkupDocumentation.PART_STOP_LENGTH, foundParts);
  }
}

MarkupDocumentation.PART_START = '€[';
MarkupDocumentation.PART_STOP = '}€[';
MarkupDocumentation.PART_START_LENGTH = MarkupDocumentation.PART_START.length;
MarkupDocumentation.PART_STOP_LENGTH = MarkupDocumentation.PART_STOP.length;

MarkupDocumentation.NAVIGATION_START = '€[navigation]{';
MarkupDocumentation.NAVIGATION_STOP = '}€[navigation]';
MarkupDocumentation.NAVIGATION_START_LENGTH = MarkupDocumentation.NAVIGATION_START.length;

MarkupDocumentation.PAGE_START = '€[page]{';
MarkupDocumentation.PAGE_STOP = '}€[page]';
MarkupDocumentation.PAGE_START_LENGTH = MarkupDocumentation.PAGE_START.length;

module.exports = new MarkupDocumentation();
