
'use strict';


class MarkupDocumentationResult {
  constructor() {
    this.key = 'RESULT';
    this.type = 'markup_result';
  }
  
  stringify(dataResult) {
    return `${this.key}=${dataResult}`;
  }
  
  parse(markup) {
    return markup;
  }
}


module.exports = new MarkupDocumentationResult() ;
