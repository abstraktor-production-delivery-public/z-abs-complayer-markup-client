
'use strict';

const MarkupParseHelper = require('../markup-parse-helper');
const DataStateMachineDiagramConfig = require('../../data/data-state-machine/data-state-machine-config');
const DataStateMachine = require('../../data/data-state-machine/data-state-machine');
const DataStateMachineState = require('../../data/data-state-machine/data-state-machine-state');
const DataStateMachineTransition = require('../../data/data-state-machine/data-state-machine-transition');
const MarkupPatternConfig = require('../markup-pattern-config');


class MarkupDocumentationStateMachine {
  markupName() {
    return 'state';
  }
  
  stringify(stateMachine) {
    let stringified = '';
    if('' !== stateMachine.title) {
      stringified += `${MarkupDocumentationStateMachine.TAG_TITLE}${stateMachine.title}\n`;
    }
    stringified += MarkupPatternConfig.stringify(stateMachine.dataStateMachineDiagramConfig);
    if(1 <= stateMachine.stateRows.length) {
      for(let i = 0; i < stateMachine.stateRows.length; ++i) {
        if(stateMachine.stateRows[i][0].comment) {
          stringified += '// ';
        }
        stringified += 'States[';
        const firstState = stateMachine.stateRows[i][0];
        stringified += firstState.name;
         for(let j = 1; j < stateMachine.stateRows[i].length; ++j) {
          const block = stateMachine.stateRows[i][j];
          stringified += `, ${block.name}`;
        }
        stringified += ']\n';
      }
    }
    else {
      stringified += 'States[]';
    }
    for(let i = 0; i < stateMachine.transitions.length; ++i) {
      const transition = stateMachine.transitions[i];
      if(-1 !== transition.fromXIndex && -1 !== transition.fromYIndex && -1 !== transition.toXIndex && -1 !== transition.toYIndex) {
        const fromState = this._getStateFromName(stateMachine, transition.fromStateName);
        const toState = this._getStateFromName(stateMachine, transition.toStateName);
        if(null !== fromState && null !== toState) {
          stringified += '\n';
          if(transition.comment) {
            stringified += '// ';
          }
          stringified += `${fromState.name} ${this._stringifyTransitionType(transition)} ${toState.name}[${transition.type}]: ${transition.data}`;
          continue;
        }
      }
      stringified += `\n${transition.data}`
    }
    return stringified;
  }
  
  parse(markup) {
    let rows = MarkupParseHelper.getRows(markup);
    let dataStateMachine = new DataStateMachine();
    rows.forEach((row) => {
      this._parseRow(dataStateMachine, row);
    });
    return dataStateMachine;
  }
  
  _parseRow(dataStateMachine, row, comment = false) {
    const trimRow = row.trim();
    if(trimRow.startsWith('//')) {
      this._parseRow(dataStateMachine, trimRow.substring(2), true);
    }
    else if(trimRow.startsWith(MarkupDocumentationStateMachine.TAG_TITLE)) {
      dataStateMachine.setTitle(trimRow.substr(MarkupDocumentationStateMachine.TAG_TITLE_LENGTH));
    }
    else if(trimRow.startsWith('States')) {
      let index = 0;
      while(true) {
        const start = trimRow.indexOf('[', index);
        if(-1 === start) {
          break;
        }
        const stop = trimRow.indexOf(']', start + 1);
        const stateNames = trimRow.substring(start + 1, -1 !== stop ? stop : undefined).split(',');
        if(null !== stateNames) {
          dataStateMachine.addStateRow();
          stateNames.forEach((stateName) => {
            let trimState = '';
            if(stateName.startsWith(',') && stateName.endsWith(',')) {
              trimState = stateName.substring(1, stateName.length - 1).trim();
            }
            else {
              trimState = stateName.trim();
            }
            dataStateMachine.addState(new DataStateMachineState(trimState, comment));
          });
        }
        if(-1 === stop) {
          break;
        }
        else {
          index = stop;
        }
      }
    }
    else if(trimRow.startsWith('Config')) {
      MarkupPatternConfig.parse(trimRow, dataStateMachine);
    }
    else {
      let type = 'normal';
      let data = '';
      let typeStartIndex = trimRow.indexOf('[');
      let typeStopIndex = trimRow.indexOf(']');
      if(-1 !== typeStartIndex && -1 !== typeStopIndex) {
        let types = trimRow.slice(typeStartIndex + 1, typeStopIndex).match(/([^\s,]+)/g);
        if(types && 1 === types.length) {
          type = types[0];
        }
      }
      let dataIndex = trimRow.indexOf(':');
      if(-1 !== dataIndex) {
        data = trimRow.substring(dataIndex + 1, trimRow.length).trim();
      }
      let transitionTypeIndex = this._parseTransitionTypeIndex(trimRow);
      if(-1 !== transitionTypeIndex) {
        const transitionType = this._parseTransitionType(trimRow, transitionTypeIndex);
        const from = trimRow.substring(0, transitionTypeIndex).trim();
        const to = trimRow.substring(transitionTypeIndex + transitionType.size, typeStartIndex).trim();
        const fromState = this._getStateFromName(dataStateMachine, from);
        const toState = this._getStateFromName(dataStateMachine, to);
        if(null !== fromState && null !== toState) {
          dataStateMachine.addTransition(new DataStateMachineTransition(data, type, transitionType.type, fromState, toState, comment));
        }
        /*else if(-1 !== fromIndex) {
          dataStateMachine.addTransition(new DataStateMachineTransition(trimRow, type, transitionType.type, fromState, toState));
        }*/
      }
    }
  }
  
  _stringifyTransitionType(transition) {
    if('simplex' === transition.transitionType) {
      return '->';
    }
    else if('duplex' === transition.transitionType) {
      return '<->';
    }
  }
  
  _parseTransitionTypeIndex(row) {
    let index = row.indexOf('<->');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('->');
    if(-1 !== index) {
      return index;
    }
    return -1;
  }
  
  _parseTransitionType(row, index) {
    if('->' === row.substr(index, 2)) {
      return {
        type: 'simplex',
        size: 2
      };
    }
    else if('<->' === row.substr(index, 3)) {
      return {
        type: 'duplex',
        size: 3
      };
    }
    return '';
  }
  
  _getStateFromName(dataStateMachine, stateName) {
    for(let i = 0; i < dataStateMachine.stateRows.length; ++i) {
      for(let j = 0; j < dataStateMachine.stateRows[i].length; ++j) {
        if(dataStateMachine.stateRows[i][j].name === stateName) {
          return dataStateMachine.stateRows[i][j];
        }
      }
    }
    return null;
  }
}

MarkupDocumentationStateMachine.TAG_TITLE = 'Title: ';
MarkupDocumentationStateMachine.TAG_TITLE_LENGTH = MarkupDocumentationStateMachine.TAG_TITLE.length;

module.exports = new MarkupDocumentationStateMachine();
