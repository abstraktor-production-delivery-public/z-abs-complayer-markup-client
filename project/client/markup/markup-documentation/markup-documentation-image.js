
'use strict';

const DataImage = require('../../data/data-image/data-image');


class MarkupDocumentationImage {
  constructor() {
    this.key = 'IMAGE';
    this.type = 'markup_image';
  }
  
  stringify(dataImage) {
    return `${this.key}=${this._stringifyParameters(dataImage)}`;
  }
  
  _stringifyParameters(dataImage) {
    let params = '';
    const parameters = Reflect.ownKeys(dataImage);
    parameters.forEach((parameter) => {
      const value = Reflect.get(dataImage, parameter);
      if(undefined !== value && '' !== value) {
        params += `${parameter}: ${value}, `;
      }
    });
    return params;
  }
  
  parse(markup) {
    const dataImage = new DataImage();
    const parameters = [];
    const parametersRaw = markup.split(',');
    parametersRaw.forEach((parameterRaw) => {
      const split = parameterRaw.split(':');
      if(2 === split.length) {
        const parameterName = split[0].trim();
        const parameterValue = split[1].trim();
        dataImage.addParameter(parameterName, parameterValue);
      }
    });
    return dataImage;
  }
}


module.exports = new MarkupDocumentationImage() ;
