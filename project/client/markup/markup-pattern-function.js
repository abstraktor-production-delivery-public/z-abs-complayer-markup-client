
const DataData = require('../data/data-data');


class MarkupPatternFunction {
  stringify(name, parameterObjects, separationWord = ',', startWord = '(', stopWord = ')') {
    let stringified = '';
    const objects = Array.isArray(parameterObjects) ? parameterObjects : [parameterObjects];
    for(let i = 0; i < objects.length; ++i) {
      stringified += `${name}${startWord}`;
      const object = objects[i];
      const keys = Reflect.ownKeys(object);
      if(1 <= keys.length) {
        stringified += `${Reflect.get(object, keys[0])}`;
        for(let i = 1; i < keys.length; ++i) {
          stringified += `${separationWord} ${Reflect.get(object, keys[i])}`;
        }
      }
      stringified += `${stopWord}\n`;
    }
    stringified += '\n';
    return stringified;
  }
  
  parse(markup, parametersCb, separationWord = ',', startWord = '(', stopWord = ')') {
    let index = 0;
    const startWordLength = startWord.length;
    const stopWordLength = stopWord.length;
    while(true) {
      let start = markup.indexOf(startWord, index);
      if(-1 === start) {
        break;
      }
      start += startWordLength;
      const stop = markup.indexOf(stopWord, start);
      if(-1 === stop) {
        break;
      }
      const parameters = markup.substring(start, stop).split(separationWord);
      const trimParameters = parameters.map((parameter) => {return parameter.trim()});
      parametersCb(...trimParameters);
      index = stop + stopWordLength;
    }
  }
}

module.exports = new MarkupPatternFunction();
