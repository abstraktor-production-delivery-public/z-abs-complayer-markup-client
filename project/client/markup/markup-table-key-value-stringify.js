
'use strict';


class MarkupTableKeyValueStringify {
  stringify(dataObject, dataValues, stringifyEmptyTables) {
    const markupTableName = dataObject.getName();
    const markupTableHeadings = dataObject.getHeadings();
    const markupTableRows = this._getMarkupTableRows(markupTableHeadings, dataValues);
    if(0 === markupTableRows.length && !stringifyEmptyTables) {
      return '';
    }
    const sizes = this._calculateSizes(markupTableName, markupTableHeadings, markupTableRows);
    return this._generateMarkup(markupTableName, markupTableHeadings, markupTableRows, sizes);
  }
  
  _getMarkupTableRows(markupTableHeadings, dataValues) {
    const markupTableRows = [];
    if(undefined !== dataValues) {
      dataValues.forEach((dataValue) => {
        const markupTableRow = {
          row: [],
          commentOut: false
        };
        markupTableRows.push(markupTableRow);
        markupTableHeadings.forEach((key) => {
          markupTableRow.row.push(Reflect.get(dataValue, key));
        });
        markupTableRow.commentOut = Reflect.get(dataValue, '_commentOut_');
      });
    }
    return markupTableRows;
  }
  
  _calculateSizes(markupTableName, markupTableHeadings, markupTableRows) {
    const sizes = Array(markupTableHeadings.length).fill(0);  
    this._calculateSize(sizes, markupTableHeadings, false);
    markupTableRows.forEach((markupTableRow) => {
      this._calculateSize(sizes, markupTableRow.row, markupTableRow.commentOut);
    });
    return sizes;
  }
  
  _calculateSize(sizes, array, commentOut) {
    array.forEach((value, index) => {
      if(undefined !== value) {
        if(sizes[index] < value.length) {
          sizes[index] = value.length + (0 === index && commentOut ? 1 : 0);
        }
      }
    });
  }
  
  _generateMarkup(markupTableName, markupTableHeadings, markupTableRows, sizes) {
    const totalColumnSize = sizes.length - 1 + sizes.reduce((prev, curr) => prev + curr);
    if(markupTableName.length <= totalColumnSize) {
      markupTableName += ' '.repeat(totalColumnSize - markupTableName.length);
      this._generateMarkupRow(markupTableHeadings, false, sizes);
      markupTableRows.forEach((markupTableRows) => {
        this._generateMarkupRow(markupTableRows.row, markupTableRows.commentOut, sizes);
      });
      markupTableRows.push({
        row: new Array(sizes.length),
        commentOut: false
      });
      const lastMarkupTableRows = markupTableRows[markupTableRows.length - 1];
      this._generateMarkupRow(lastMarkupTableRows.row, lastMarkupTableRows.commentOut, sizes);
    }
    let markup = `|${markupTableName}|\n|${markupTableHeadings.join('|')}|`;
    markupTableRows.forEach((markupTableRows) => {
      markup += `\n${markupTableRows.commentOut ? '#' : ''}|${markupTableRows.row.join('|')}|`;
    });
    return markup;
  }
  
  _generateMarkupRow(row, commentOut, sizes) {
    for(let index = 0; index < row.length; ++index) {
      if(undefined !== row[index]) {
        const repeateSize = sizes[index] - row[index].length - (commentOut && 0 === index ? 1 : 0);
        if(repeateSize > 0) {
          row[index] = row[index] +  ' '.repeat(sizes[index] - row[index].length - (commentOut && 0 === index ? 1 : 0));
        }
      }
      else {
         row[index] = ' '.repeat(sizes[index]);
      }
    };
  }
}

module.exports = new MarkupTableKeyValueStringify();
