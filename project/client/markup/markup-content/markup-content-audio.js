
'use strict';

const DataContentAudioGlobal = require('../../data/data-content/data-content-audio-global');
const DataContentAudioLocal = require('../../data/data-content/data-content-audio-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupContentAudio {
  stringify(dataContentGlobals, dataContentLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataContentAudioLocal(), dataContentLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataContentAudioGlobal(), dataContentGlobals, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataContentAudioGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataContentAudioLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupContentAudio();
