
'use strict';

const DataReposContentLocal = require('../../data/data-repos/data-repos-content-local');
const DataReposDataLocal = require('../../data/data-repos/data-repos-data-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupRepos {
  stringify(dataReposContentLocal, dataReposDataLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataReposContentLocal(), dataReposContentLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataReposDataLocal(), dataReposDataLocal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      contentRepos: MarkupTableKeyValue.parse(DataReposContentLocal, objectTables.objects),
      dataRepos: MarkupTableKeyValue.parse(DataReposDataLocal, objectTables.objects)
    };
  }
}

module.exports = MarkupRepos;
