
'use strict';

const DataConstructor = require('../../data/data-constructor');
const DataTestCase = require('../../data/data-test-case/data-test-case');
const DataTestCaseSettings = require('../../data/data-test-case/data-test-case-settings');
const DataActor = require('../../data/data-test-case/data-actor');
const DataTestDataTestCase = require('../../data/data-test-case/data-test-data-test-case');
const DataTestDataIteration = require('../../data/data-test-case/data-test-data-iteration');
const DataVerificationTestCase = require('../../data/data-test-case/data-verification-test-case');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupTestCase {
  markupName() {
    return 'tc';
  }
    
  stringify(dataTestCase, stringifyEmptyTables = true) {
    const dataTestCaseTc = dataTestCase.tc;
    let markup = '';
    markup += this._stringifyMarkupPart(markup.length, MarkupTableKeyValue.stringify(new DataTestCaseSettings(), dataTestCaseTc.settings, stringifyEmptyTables));
    markup += this._stringifyMarkupPart(markup.length, MarkupTableKeyValue.stringify(new DataActor(), dataTestCaseTc.actors, stringifyEmptyTables));
    markup += this._stringifyMarkupPart(markup.length, MarkupTableKeyValue.stringify(new DataTestDataTestCase(), dataTestCaseTc.testDataTestCases, stringifyEmptyTables));
    markup += this._stringifyMarkupPart(markup.length, MarkupTableKeyValue.stringify(new DataTestDataIteration(), dataTestCaseTc.testDataIteration, stringifyEmptyTables));
    markup += this._stringifyMarkupPart(markup.length, MarkupTableKeyValue.stringify(new DataVerificationTestCase(), dataTestCaseTc.verificationTestCases, stringifyEmptyTables));
    return markup;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    const settings = MarkupTableKeyValue.parse(DataTestCaseSettings, objectTables.objects);
    const actors = MarkupTableKeyValue.parse(DataActor, objectTables.objects);
    const testDataTestCases = MarkupTableKeyValue.parse(DataTestDataTestCase, objectTables.objects);
    const testDataIteration = MarkupTableKeyValue.parse(DataTestDataIteration, objectTables.objects);
    const verificationTestCases = MarkupTableKeyValue.parse(DataVerificationTestCase, objectTables.objects);
    const dataTestCase = new DataTestCase(DataConstructor.create(settings), DataConstructor.create(actors), DataConstructor.create(testDataTestCases), DataConstructor.create(testDataIteration), DataConstructor.create(verificationTestCases));
    return {
      success: true,
      tc: dataTestCase
    };
  }
  
  _stringifyMarkupPart(markupLength, markupPart) {
    if(0 !== markupPart.length) {
      if(0 !== markupLength) {
        return `\n\n${markupPart}`;
      }
      else {
        return `${markupPart}`;
      }
    }
    else {
      return '';
    }
  }
}


module.exports = new MarkupTestCase();
