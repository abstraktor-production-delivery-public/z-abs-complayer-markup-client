
'use strict';


class MarkupParseHelper {
  getRows(markup) {
    let index = 0;
    let nextIndex = 0;
    let rows = [];
    let length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex));
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length));
        index = length;
      }
    }
    return rows;
  }
}

module.exports = new MarkupParseHelper();
