
'use strict';

const DataLab = require('../data/data-lab/data-lab');
const DataLabNetwork = require('../data/data-lab/data-lab-network');
const DataLabInterfaceClient = require('../data/data-lab/data-lab-interface-client');
const DataLabInterfaceSut = require('../data/data-lab/data-lab-interface-sut');
const DataLabInterfaceServer = require('../data/data-lab/data-lab-interface-server');
const MarkupPatternFunction = require('./markup-pattern-function');


class MarkupLab {
  markupName() {
    return 'lab';
  }
  
  stringify(networks) {
    const dataLab = new DataLab(networks);
    let stringified = '';
    dataLab.networks.forEach((network) => {
      stringified += 'Network(';
      stringified += network.networkName + ', ';
      stringified += network.family + ', ';
      stringified += network.subnet + ', ';
      stringified += network.description + ', ';
      stringified += network.valid + ', ';
      stringified += network.reduced + ')\n';
      if(0 !== network.clients.length) {
        network.clients.forEach((client) => {
          stringified += MarkupPatternFunction.stringify('Client', client);
        });
      }
      if(0 !== network.suts.length) {
        network.suts.forEach((sut) => {
          stringified += MarkupPatternFunction.stringify('Sut', sut);
        });
      }
      if(0 !== network.servers.length) {
        network.servers.forEach((server) => {
          stringified += MarkupPatternFunction.stringify('Server', server);
        });
      }
    });
    return stringified;
  }
  
  parse(markup) {
    const rows = this._getRows(markup);
    const dataLab = new DataLab();
    rows.forEach((row) => {
      this._parseRow(dataLab, row);
    });
    return dataLab.networks;
  }
  
  _parseRow(dataLab, row) {
    const trimRow = row.trim();
    if(trimRow.startsWith('Network(')) {
      const stop = trimRow.indexOf(')');
      const parametersRaw = trimRow.substring('Network('.length, -1 !== stop ? stop : undefined).split(',');
      const parameters = parametersRaw.map((parameter) => {
        return parameter.trim();
      });
      dataLab.addNetwork(new DataLabNetwork(...parameters));
    }
    else if(trimRow.startsWith('Client')) {
      MarkupPatternFunction.parse(trimRow, (...parameters) => {
        const client = new DataLabInterfaceClient(...parameters);
        const network = dataLab.getNetwork(client.network);
        if(undefined !== network) {
          network.addClient(client);
        }
      });
    }
    else if(trimRow.startsWith('Sut')) {
      MarkupPatternFunction.parse(trimRow, (...parameters) => {
        const sut = new DataLabInterfaceSut(...parameters);
        const network = dataLab.getNetwork(sut.network);
        if(undefined !== network) {
          network.addSut(sut);
        }
      });
    }
    else if(trimRow.startsWith('Server')) {
      MarkupPatternFunction.parse(trimRow, (...parameters) => {
        const server = new DataLabInterfaceServer(...parameters);
        const network = dataLab.getNetwork(server.network);
        if(undefined !== network) {
          network.addServer(server);
        }
      });
    }
  }
  
  _getRows(markup) {
    let index = 0;
    let nextIndex = 0;
    let rows = [];
    let length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex).trim());
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length).trim());
        index = length;
      }
    }
    return rows;
  }   
}


module.exports = new MarkupLab();
