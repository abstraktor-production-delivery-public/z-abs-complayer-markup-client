
'use strict';


class MarkupTableKeyValueParse {
  parse(dataObject, objectTables) {
    const objectTable = objectTables.find((objectTable) => {
      return dataObject.name.substr(4) === objectTable[0].columns[0];
    });
    const objects = [];
    if(undefined !== objectTable && 2 <= objectTable.length) {
      const parameterNames = objectTable[1].columns;
      for(let i = 2; i < objectTable.length; ++i) {
        const dataObjectInstance = new dataObject(...objectTable[i].columns);
        if(objectTable[i].commentOut && dataObjectInstance.commentOut) {
          dataObjectInstance.commentOut();
        }
        objects.push(dataObjectInstance);
      }
    }
    return objects;
  }
}

module.exports = new MarkupTableKeyValueParse();
