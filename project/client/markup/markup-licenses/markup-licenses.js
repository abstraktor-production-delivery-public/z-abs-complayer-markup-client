
'use strict';

const DataLicenses = require('../../data/data-licenses/data-licenses');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupLicenses {
  stringify(dataLicenses) {
    return `\n${MarkupTableKeyValue.stringify(new DataLicenses(), dataLicenses, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      licenses: MarkupTableKeyValue.parse(DataLicenses, objectTables.objects)
    };
  }
}

module.exports = new MarkupLicenses();
