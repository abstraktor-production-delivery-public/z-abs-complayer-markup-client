
'use strict';

const DataSystemUnderTestInstances = require('../../data/data-system-under-test/data-system-under-test-instances');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupSystemUnderTestInstances {
  stringify(dataSystemUnderTestInstances) {
    return `\n${MarkupTableKeyValue.stringify(new DataSystemUnderTestInstances(), dataSystemUnderTestInstances, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      instances: MarkupTableKeyValue.parse(DataSystemUnderTestInstances, objectTables.objects)
    };
  }
}

module.exports = new MarkupSystemUnderTestInstances();
