
'use strict';

const DataSystemUnderTestNodes = require('../../data/data-system-under-test/data-system-under-test-nodes');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupSystemUnderTestNodes {
  stringify(dataSystemUnderTestNodes) {
    return `\n${MarkupTableKeyValue.stringify(new DataSystemUnderTestNodes(), dataSystemUnderTestNodes, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      nodes: MarkupTableKeyValue.parse(DataSystemUnderTestNodes, objectTables.objects)
    };
  }
}


module.exports = new MarkupSystemUnderTestNodes();
