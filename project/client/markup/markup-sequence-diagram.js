
'use strict';

const DataSequenceDiagram = require('../data/data-sequence-diagram/data-sequence-diagram');
const DataSequenceDiagramNode = require('../data/data-sequence-diagram/data-sequence-diagram-node');
const DataSequenceDiagramMessage = require('../data/data-sequence-diagram/data-sequence-diagram-message');
const DataSequenceDiagramConfig = require('../data/data-sequence-diagram/data-sequence-diagram-config');


class MarkupSequenceDiagram {
  markupName() {
    return 'seq';
  }
  
  stringify(sequenceDiagram) {
    let stringified = '';
    if('' !== sequenceDiagram.title) {
      stringified += `${MarkupSequenceDiagram.TAG_TITLE} ${sequenceDiagram.title}\n`;
    }
    const dataSequenceDiagramConfig = sequenceDiagram.dataSequenceDiagramConfig;
    const keys = Reflect.ownKeys(dataSequenceDiagramConfig);
    stringified += 'Config(';
    if(1 <= keys.length) {
      stringified += `${keys[0]}: ${Reflect.get(dataSequenceDiagramConfig, keys[0])}`;
      for(let i = 1; i < keys.length; ++i) {
        stringified += `, ${keys[i]}: ${Reflect.get(dataSequenceDiagramConfig, keys[i])}`;
      }
    }
    stringified += ')\n\n';
    if(1 <= sequenceDiagram.nodes.length) {
      stringified += 'Nodes[';
      stringified += sequenceDiagram.nodes[0].name;
      for(let i = 1; i < sequenceDiagram.nodes.length; ++i) {
        stringified += `, ${sequenceDiagram.nodes[i].name}`;
      }
      stringified += ']';
    }
    else {
      stringified += 'Nodes[]\n';
    }
    for(let i = 0; i < sequenceDiagram.messages.length; ++i) {
      const message = sequenceDiagram.messages[i];
      if(2 === message.nodes) {
        if(-1 !== message.fromIndex && -1 !== message.toIndex) {
          stringified += `\n${sequenceDiagram.nodes[message.fromIndex].name} ${this._stringifyMessageType(message)} ${sequenceDiagram.nodes[message.toIndex].name}[${message.type}]: ${message.data}`;
        }
      }
      else if(1 === message.nodes) {
        if(-1 !== message.fromIndex) {
          stringified += `\n${sequenceDiagram.nodes[message.fromIndex].name} ${this._stringifyMessageType(message)} [${message.type}]: ${message.data}`;
        }
      }
      else {
        stringified += `\n${message.data}`
      }
    }
    return stringified;
  }
  
  parse(markup) {
    const rows = this._getRows(markup);
    const dataSequenceDiagram = new DataSequenceDiagram();
    rows.forEach((row) => {
      this._parseRow(dataSequenceDiagram, row);
    });
    return dataSequenceDiagram;
  }
  
  _parseRow(dataSequenceDiagram, row) {
    const trimRow = row.trim();
    if(trimRow.startsWith(MarkupSequenceDiagram.TAG_TITLE)) {
      dataSequenceDiagram.setTitle(trimRow.substr(MarkupSequenceDiagram.TAG_TITLE_LENGTH));
    }
    else if(trimRow.startsWith('Nodes')) {
      const nodeNames = trimRow.slice(trimRow.indexOf('[') + 1, trimRow.indexOf(']')).match(/([^\s,]+)/g);
      if(null !== nodeNames) {
        nodeNames.forEach((nodeName) => {
          dataSequenceDiagram.addNode(new DataSequenceDiagramNode(nodeName));
        });
      }
    }
    else if(trimRow.startsWith('Config')) {
      const start = trimRow.indexOf('(', 0);
      if(-1 === start) {
        return;
      }
      const stop = trimRow.indexOf(')', start + 1);
      const configParameters = trimRow.substring(start + 1, -1 !== stop ? stop : undefined).split(',');
      if(null !== configParameters) {
        const config = {};
        configParameters.forEach((configParameter) => {
          const parameters = configParameter.split(':');
          if(2 === parameters.length) {
            const parameterName = parameters[0].trim();
            const parameterValue = parameters[1].trim();
            const numberValue = Number.parseInt(parameterValue);
            if(!Number.isNaN(numberValue)) {
              dataSequenceDiagram.addConfig(parameterName, numberValue);
            }
            else if('true' === parameterValue) {
              dataSequenceDiagram.addConfig(parameterName, true);
            }
            else if('false' === parameterValue) {
              dataSequenceDiagram.addConfig(parameterName, false);
            }
            else {
              dataSequenceDiagram.addConfig(parameterName, parameterValue);
            }
          }
        });
      }
    }
    else {
      let type = 'unknown';
      let data = '';
      const typeStartIndex = trimRow.indexOf('[');
      const typeStopIndex = trimRow.indexOf(']');
      if(-1 !== typeStartIndex && -1 !== typeStopIndex) {
        let types = trimRow.slice(typeStartIndex + 1, typeStopIndex).match(/([^\s,]+)/g);
        if(types && 1 === types.length) {
          type = types[0];
        }
      }
      let dataIndex = trimRow.indexOf(':');
      if(-1 !== dataIndex) {
        data = trimRow.substring(dataIndex + 1, trimRow.length).trim();
      }
      const messageTypeIndex = this._parseMessageTypeIndex(trimRow);
      if(-1 !== messageTypeIndex) {
        const messageType = this._parseMessageType(trimRow, messageTypeIndex);
        if(2 === messageType.nodes) {
          const from = trimRow.substring(0, messageTypeIndex).trim();
          const to = trimRow.substring(messageTypeIndex + messageType.size, typeStartIndex).trim();
          const fromIndex = this._getNodeIndex(dataSequenceDiagram, from);
          const toIndex = this._getNodeIndex(dataSequenceDiagram, to);
          if(-1 !== fromIndex && -1 !== toIndex) {
            dataSequenceDiagram.addMessage(new DataSequenceDiagramMessage(data, type, messageType.nodes, messageType.type, messageType.height, fromIndex, toIndex));
          }
        }
        else {
          const from = trimRow.substring(0, messageTypeIndex).trim();
          const fromIndex = this._getNodeIndex(dataSequenceDiagram, from);
          if(-1 !== fromIndex) {
            dataSequenceDiagram.addMessage(new DataSequenceDiagramMessage(data, type, messageType.nodes, messageType.type, messageType.height, fromIndex, -1));
          }
        }
      }
    }
  }
  
  _getRows(markup) {
    let index = 0;
    let nextIndex = 0;
    const rows = [];
    const length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex).trim());
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length).trim());
        index = length;
      }
    }
    return rows;
  }
    
  _stringifyMessageType(message) {
    if('normal' === message.messageType) {
      return '=>';
    }
    else if('normal-bi-directional' === message.messageType) {
      return '<=>';
    }
    else if('part' === message.messageType) {
      return '->';
    }
    else if('part-bi-directional' === message.messageType) {
      return '<->';
    }
    else if('connect' === message.messageType) {
      return '-o';
    }
    else if('disconnect' === message.messageType) {
      return '-x';
    }
    else if('start' === message.messageType) {
      return '>';
    }
    else if('stop' === message.messageType) {
      return '#';
    }
    else if('pause' === message.messageType) {
      return '||';
    }
    else if('comment' === message.messageType) {
      return '//';
    }
    else if('long-comment' === message.messageType) {
      return '/**/';
    }
    else if('verify' === message.messageType) {
      return '==';
    }
    else if('new' === message.messageType) {
      return '=';
    }
    else if('delete' === message.messageType) {
      return '!';
    }
    else if('browser' === message.messageType) {
      return 'browser';
    }
    else if('page' === message.messageType) {
      return 'page';
    }
    else if('click' === message.messageType) {
      return 'click';
    }
    else if('type' === message.messageType) {
      return 'type';
    }
  }
  
  _parseMessageTypeIndex(row) {
    let colonIndex = row.indexOf(':');
    if(-1 !== colonIndex) {
      row = row.substring(0, colonIndex);
    }
    let index = row.indexOf('<=>');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('=>');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('<->');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('->');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('-o');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('-x');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('>');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('#');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('||');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('//');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('/**/');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('==');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('=');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('!');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('browser');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('page');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('click');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('type');
    if(-1 !== index) {
      return index;
    }
    return -1;
  }
  
  _parseMessageType(row, index) {
    if('=>' === row.substr(index, 2)) {
      return {
        type: 'normal',
        size: 2,
        nodes: 2,
        height: 'msg'
      };
    }
    else if('<=>' === row.substr(index, 3)) {
      return {
        type: 'normal-bi-directional',
        size: 3,
        nodes: 2,
        height: 'msg'
      };
    }
    else if('->' === row.substr(index, 2)) {
      return {
        type: 'part',
        size: 2,
        nodes: 2,
        height: 'msg'
      };
    }
    else if('<->' === row.substr(index, 3)) {
      return {
        type: 'part-bi-directional',
        size: 3,
        nodes: 2,
        height: 'msg'
      };
    }
    else if('-o' === row.substr(index, 2)) {
      return {
        type: 'connect',
        size: 2,
        nodes: 2,
        height: 'msg'
      };
    }
    else if('-x' === row.substr(index, 2)) {
      return {
        type: 'disconnect',
        size: 2,
        nodes: 2,
        height: 'msg'
      };
    }
    else if('>' === row.substr(index, 1)) {
      return {
        type: 'start',
        size: 1,
        nodes: 1,
        height: 'event'
      };
    }
    else if('#' === row.substr(index, 1)) {
      return {
        type: 'stop',
        size: 1,
        nodes: 1,
        height: 'event'
      };
    }
    else if('||' === row.substr(index, 2)) {
      return {
        type: 'pause',
        size: 2,
        nodes: 1,
        height: 'event'
      };
    }
    else if('//' === row.substr(index, 2)) {
      return {
        type: 'comment',
        size: 2,
        nodes: 1,
        height: 'event'
      };
    }
    else if('/**/' === row.substr(index, 4)) {
      return {
        type: 'long-comment',
        size: 4,
        nodes: 2,
        height: 'comment'
      };
    }
    else if('==' === row.substr(index, 2)) {
      return {
        type: 'verify',
        size: 2,
        nodes: 1,
        height: 'event'
      };
    }
    else if('=' === row.substr(index, 1)) {
      return {
        type: 'new',
        size: 1,
        nodes: 1,
        height: 'event'
      };
    }
    else if('!' === row.substr(index, 1)) {
      return {
        type: 'delete',
        size: 1,
        nodes: 1,
        height: 'event'
      };
    }
    else if('browser' === row.substr(index, 7)) {
      return {
        type: 'browser',
        size: 2,
        nodes: 1,
        height: 'event'
      };
    }
    else if('page' === row.substr(index, 4)) {
      return {
        type: 'page',
        size: 2,
        nodes: 1,
        height: 'event'
      };
    }
    else if('click' === row.substr(index, 5)) {
      return {
        type: 'click',
        size: 2,
        nodes: 1,
        height: 'event'
      };
    }
    else if('type' === row.substr(index, 4)) {
      return {
        type: 'type',
        size: 2,
        nodes: 1,
        height: 'event'
      };
    }
    return '';
  }
  
  _getNodeIndex(dataSequenceDiagram, nodeName) {
    return dataSequenceDiagram.nodes.findIndex((node) => {
      return node.name === nodeName;
    });
  }
}

MarkupSequenceDiagram.TAG_TITLE = 'Title: ';
MarkupSequenceDiagram.TAG_TITLE_LENGTH = MarkupSequenceDiagram.TAG_TITLE.length;


module.exports = new MarkupSequenceDiagram();
