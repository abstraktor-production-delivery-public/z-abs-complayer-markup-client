
'use strict';

const DataAddressesServerAddressGlobal = require('../../data/addresses/data-addresses-server-address-global');
const DataAddressesServerAddressLocal = require('../../data/addresses/data-addresses-server-address-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesServerAddress {
  stringify(dataAddressesServerAddressGlobal, dataAddressesServerAddressLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesServerAddressLocal(), dataAddressesServerAddressLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesServerAddressGlobal(), dataAddressesServerAddressGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesServerAddressGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesServerAddressLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesServerAddress();
