
'use strict';

const DataAddressesClientAddressGlobal = require('../../data/addresses/data-addresses-client-address-global');
const DataAddressesClientAddressLocal = require('../../data/addresses/data-addresses-client-address-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesClientAddress {
  stringify(dataAddressesClientAddressGlobal, dataAddressesClientAddressLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesClientAddressLocal(), dataAddressesClientAddressLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesClientAddressGlobal(), dataAddressesClientAddressGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesClientAddressGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesClientAddressLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesClientAddress();
