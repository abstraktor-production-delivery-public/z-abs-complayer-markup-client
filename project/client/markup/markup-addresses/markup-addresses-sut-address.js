
'use strict';

const DataAddressesSutAddressGlobal = require('../../data/addresses/data-addresses-sut-address-global');
const DataAddressesSutAddressLocal = require('../../data/addresses/data-addresses-sut-address-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesSutAddress {
  stringify(dataAddressesSutAddressGlobal, dataAddressesSutAddressLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesSutAddressLocal(), dataAddressesSutAddressLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesSutAddressGlobal(), dataAddressesSutAddressGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesSutAddressGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesSutAddressLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesSutAddress();
