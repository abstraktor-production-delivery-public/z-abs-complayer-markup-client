
'use strict';

const DataAddressesSrcGlobal = require('../../data/addresses/data-addresses-src-global');
const DataAddressesSrcLocal = require('../../data/addresses/data-addresses-src-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesSrc {
  stringify(dataAddressesSrcGlobal, dataAddressesSrcLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesSrcLocal(), dataAddressesSrcLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesSrcGlobal(), dataAddressesSrcGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesSrcGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesSrcLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesSrc();
