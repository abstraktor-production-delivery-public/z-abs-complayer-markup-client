
'use strict';

const DataAddressesDnsGlobal = require('../../data/addresses/data-addresses-dns-global');
const DataAddressesDnsLocal = require('../../data/addresses/data-addresses-dns-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesDns {
  stringify(dataAddressesDnsGlobal, dataAddressesDnsLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesDnsLocal(), dataAddressesDnsLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesDnsGlobal(), dataAddressesDnsGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesDnsGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesDnsLocal, objectTables.objects)
    };
  }
}


module.exports = new MarkupAddressesDns();
