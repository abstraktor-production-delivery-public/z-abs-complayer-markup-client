
'use strict';


class DataLabNetworks {
  constructor(networkName, family, subnet, description, valid, reduced) {
    this.networkName = networkName ? networkName : 'N1';
    this.family = 'IPv6' === family ? 'IPv6' : 'IPv4';
    this.subnet = subnet;
    this.description = description;
    this.valid = 'false' === valid ? false : true;
    this.reduced = 'true' === reduced ? true : false;
    this.clients = [];
    this.suts = [];
    this.servers = [];
  }
  
  addClient(client) {
    this.clients.push(client);
  }
  
  addSut(sut) {
    this.suts.push(sut);
  }
  
  addServer(server) {
    this.servers.push(server);
  }
}

module.exports = DataLabNetworks;
