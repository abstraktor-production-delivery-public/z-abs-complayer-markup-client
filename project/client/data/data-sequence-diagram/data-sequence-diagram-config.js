
'use strict';

const DataConfig = require('../data-config');


class DataSequenceDiagramConfig extends DataConfig {
  constructor(parameters) {
    super();
    this.nodeWidth = 150;
    this.nodeMessageHeight = 17;
    this.nodeEventHeight = 10;
    this.nodeCommentHeight = 26;
    this.widthBias = 30;
    this.heightBias = 30;
    this.lineNumbers = false;
    this.border = true;
    this.backgroundColor = 'default';
  }
}

module.exports = DataSequenceDiagramConfig;
