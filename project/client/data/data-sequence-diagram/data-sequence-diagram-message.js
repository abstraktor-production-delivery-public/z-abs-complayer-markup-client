
'use strict';


class DataSequenceDiagramMessage {
  constructor(data, type, nodes, messageType, height, fromIndex, toIndex) {
    this.data = data;
    this.type = type;
    this.nodes = nodes;
    this.messageType = messageType;
    this.height = height;
    this.fromIndex = fromIndex;
    this.toIndex = toIndex;
  }
}

module.exports = DataSequenceDiagramMessage;
