
'use strict';

const DataSequenceDiagramConfig = require('./data-sequence-diagram-config');


class DataSequenceDiagram {
  constructor(title = '', nodes = [], messages = []) {
    this.title = title;
    this.nodes = nodes;
    this.messages = messages;
    this.dataSequenceDiagramConfig = new DataSequenceDiagramConfig();
  }
  
  setTitle(title) {
    this.title = title;    
  }
  
  addNode(node) {
    this.nodes.push(node);
  }

  addMessage(message) {
    this.messages.push(message);
  }
  
  addConfig(name, value) {
    this.dataSequenceDiagramConfig.addConfig(name, value);
  }
}

module.exports = DataSequenceDiagram;
