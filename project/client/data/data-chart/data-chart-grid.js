
'use strict'


class DataChartGrid {
  constructor(dx, dy) {
    const deltaX = Number.parseInt(dx);
    this.dx = !Number.isNaN(deltaX) ? deltaX : 0;
    const deltaY = Number.parseInt(dy);
    this.dy = !Number.isNaN(deltaY) ? deltaY : 0;
  }
}

module.exports = DataChartGrid;
