
'use strict'


class DataChartAxis {
  constructor(direction, topValue) {
    this.direction = direction;
    const numberTopValue = Number.parseInt(topValue);
    this.topValue = !Number.isNaN(numberTopValue) ? numberTopValue : 0;
  }
}

module.exports = DataChartAxis;
