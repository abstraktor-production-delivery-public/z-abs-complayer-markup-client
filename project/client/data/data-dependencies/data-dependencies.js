
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataDependencies extends DataTableKeyValue {
  constructor(name, version, dev, licenses) {
    super();
    this.name = name;
    this.version = version;
    this.dev = dev;
    this.licenses = licenses;
  }
}

module.exports = DataDependencies;
