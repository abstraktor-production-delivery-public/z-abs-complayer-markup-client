
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataTestDataSystemLocal extends DataTableKeyValue {
  constructor(name, sut, sutInstance, value, description) {
    super();
    this.name = name;
    this.sut = sut;
    this.sutInstance = sutInstance;
    this.value = value;
    this.description = description;
  }
}

module.exports = DataTestDataSystemLocal;
