
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataTestDataEnvironmentGlobal extends DataTableKeyValue {
  constructor(name, sut, sutInstance, value, description) {
    super();
    this.name = name;
    this.sut = sut;
    this.sutInstance = sutInstance;
    this.value = value;
    this.description = description;
  }
}

module.exports = DataTestDataEnvironmentGlobal;
