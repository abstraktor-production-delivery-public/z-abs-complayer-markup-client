
'use strict';


class DataTableKeyValue {
  constructor(defaultValues, allowedValues) {
    this._defaultValues = new Map(defaultValues);
    this._allowedValues = new Map(allowedValues);
  }
  
  getName() {
    return this.constructor.name.substr(4);
  }
  
  getHeadings() {
    const headings = Reflect.ownKeys(this);
    const filteredHeadings = headings.filter((value) => {
      return !value.startsWith('_');
    });    
    return filteredHeadings;
  }

  getValue(heading, rowData) {
    let value = Reflect.get(rowData, heading);
    let style = {color:'black'};
    if(undefined === value || '' === value) {
      let defaultFunction = this._defaultValues.get(heading);
      if(undefined !== defaultFunction) {
        value = defaultFunction(rowData);
        Reflect.set(rowData, heading, value);
        style = {color:'#009933'};
      }
    }
    let allowedFunction = this._allowedValues.get(heading);
    if(undefined !== allowedFunction) {
      switch(allowedFunction(rowData)) {
        case DataTableKeyValue.ALLOWED_NOK_VALUE:
          style = {color:'red'};
          break;
        case DataTableKeyValue.ALLOWED_NOK_COMBO:
          style = {color:'orange'};
          break;
      }
    }
    return {
      value: value,
      style: style
    }
  }
}

DataTableKeyValue.ALLOWED_OK = 0;
DataTableKeyValue.ALLOWED_NOK_VALUE = 1;
DataTableKeyValue.ALLOWED_NOK_COMBO = 2;

module.exports = DataTableKeyValue;
