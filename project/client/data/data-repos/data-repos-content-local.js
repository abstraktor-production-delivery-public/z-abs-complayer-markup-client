
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataReposContentLocal extends DataTableKeyValue {
  constructor(name, url) {
    super();
    this.name = name;
    this.url = url;
  }
}

module.exports = DataReposContentLocal;
