
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataReposDataLocal extends DataTableKeyValue {
  constructor(name, url) {
    super();
    this.name = name;
    this.url = url;
  }
}

module.exports = DataReposDataLocal;
