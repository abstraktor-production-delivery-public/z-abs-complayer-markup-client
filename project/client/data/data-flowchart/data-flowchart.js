
'use strict';


class DataFlowchart {
  constructor(title = '', blockRows = [], connections = []) {
    this.title = title;
    this.blockRows = blockRows;
    this.connections = connections;
  }
  
  setTitle(title) {
    this.title = title;
  }
  
  addBlockRow() {
    this.blockRows.push([]);
  }
  
  addBlock(block) {
    this.blockRows[this.blockRows.length - 1].push(block);
    block.addIndex(this.blockRows[this.blockRows.length - 1].length -1, this.blockRows.length - 1);
  }
  
  addConnection(connection) {
    this.connections.push(connection);
  }
}

module.exports = DataFlowchart;
