
'use strict';


class DataConstructor {
  static create(object) {
    const dataObject = JSON.parse(JSON.stringify(object, (key, value) => {
      if(key.startsWith('_') && '_commentOut_' !== key) {
        return undefined;
      }
      return value;
    }));
    return dataObject;
  }
}


module.exports = DataConstructor;
