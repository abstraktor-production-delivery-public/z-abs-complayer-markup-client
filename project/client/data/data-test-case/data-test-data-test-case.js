
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataTestDataTestCase extends DataTableKeyValue {
  constructor(key, value, description) {
    super();
    this.key = key;
    this.value = value;
    this.description = description;
  }
}

module.exports = DataTestDataTestCase;
