
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataVerificationTestCase extends DataTableKeyValue {
  constructor(key, value, operation, type, description) {
    super([
      ['type', (rowData) => {return 'string'}],
      ['operation', (rowData) => {return '==='}]
    ],
    [
      []
    ]);
    this.key = key;
    this.value = value;
    this.operation = operation;
    this.type = type;
    this.description = description;
  }
}

module.exports = DataVerificationTestCase;
