
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataTestSuiteAbstraction extends DataTableKeyValue {
  constructor(name, execution, iterationsTs) {
    super([
      ['execution', (rowData) => {return 'serial'}],
      ['iterations', (rowData) => {return 1}]
    ],
    [
      ['execution', (rowData) => {
        switch(rowData.execution) {
          case 'serial':
          case 'parallel':
            return DataTableKeyValue.ALLOWED_OK;
          default:
            return DataTableKeyValue.ALLOWED_NOK_VALUE;
        }
      }],
      ['iterations', (rowData) => {
        if(Number.isInteger(Number.parseFloat(rowData.iterations)) && rowData.iterations >= 1) {
          return DataTableKeyValue.ALLOWED_OK;
        }
        else {
          return DataTableKeyValue.ALLOWED_NOK_VALUE;
        }
      }]
    ]);
    this.name = name;
    this.execution = execution;
    this.iterationsTs = iterationsTs;
    this._commentOut_ = false;
  }
  
  commentOut() {
    this._commentOut_ = true;
  }
  
  getValue(heading, rowData) {
    if('name' !== heading) {
      return super.getValue(heading, rowData);
    }
    else {
      const value = super.getValue(heading, rowData);
      const paths = value.value.split('.');
      if(0 === paths.length) {
        value.link = '/';
        return value;
      }
      let link = '';
      if('tc' === paths[0]) {
        link = '/test-cases';
      }
      else if('ts' === paths[0]) {
        link = '/test-suites';
      }
      if(1 === paths.length) {
        value.link = link;
        return value;
      }
      paths.splice(0, 1);
      link += '/' + paths.join('/');
      value.link = link;
      return value;
    } 
  }
}

module.exports = DataTestSuiteAbstraction;
