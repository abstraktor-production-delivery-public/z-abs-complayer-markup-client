
'use strict';

const DataStateMachineDiagramConfig = require('./data-state-machine-config');


class DataStateMachine {
  constructor(title = '', stateRows = [], transitions = []) {
    this.title = title;
    this.dataStateMachineDiagramConfig = new DataStateMachineDiagramConfig();
    this.stateRows = [];
    this.transitions = transitions;
  }
  
  setTitle(title) {
    this.title = title;    
  }
  
  addConfig(name, value) {
    this.dataStateMachineDiagramConfig.addConfig(name, value);
  }
  
  addStateRow() {
    this.stateRows.push([]);
  }
  
  addState(state) {
    this.stateRows[this.stateRows.length - 1].push(state);
    state.addIndex(this.stateRows[this.stateRows.length - 1].length -1, this.stateRows.length - 1);
  }
  
  addTransition(transition) {
    this.transitions.push(transition);
  }
}

module.exports = DataStateMachine;
