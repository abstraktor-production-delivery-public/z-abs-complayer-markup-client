
'use strict';


class DataStateMachineTransition {
  constructor(data, type, transitionType, fromState, toState, comment) {
    this.data = data;
    this.type = type;
    this.transitionType = transitionType;
    this.fromStateName = fromState ? fromState.name : '';
    this.toStateName = toState ? toState.name : '';
    this.comment = comment;
    fromState && fromState.addOut();
    toState && toState.addIn();
  }
}

module.exports = DataStateMachineTransition;
