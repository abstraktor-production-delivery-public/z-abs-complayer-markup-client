
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataContentOtherLocal extends DataTableKeyValue {
  constructor(name, path, mime, size, properties, description) {
    super();
    this.name = name;
    this.path = path;
    this.mime = mime;
    this.size = size;
    this.properties = properties;
    this.description = description;
  }
}


module.exports = DataContentOtherLocal;
