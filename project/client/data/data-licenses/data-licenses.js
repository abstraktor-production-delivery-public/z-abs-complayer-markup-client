
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataLicenses extends DataTableKeyValue {
  constructor(name, category, link) {
    super();
    this.name = name;
    this.category = category;
    this.link = link;
  }
}

module.exports = DataLicenses;
