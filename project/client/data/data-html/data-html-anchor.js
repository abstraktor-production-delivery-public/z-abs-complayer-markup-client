
'use strict';


class DataHtmlAnchor {
  constructor(link) {
    this.type = 'anchor';
    this.link = link;
  }
}

module.exports = DataHtmlAnchor;
