
'use strict';


class DataNodeDiagramNode {
  constructor(name, type, corners, color) {
    this.name = name;
    this.type = type;
    this.corners = corners;
    this.color = color;
    this.xIndex = -1;
    this.yIndex = -1;
  }
  
  addIndex(xIndex, yIndex) {
    this.xIndex = xIndex;
    this.yIndex = yIndex;
  }
}


module.exports = DataNodeDiagramNode;
