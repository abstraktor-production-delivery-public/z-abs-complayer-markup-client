
'use strict';


class DataNodeDiagramGroup {
  constructor(name, leftStartIndex, leftStopIndex, topStartIndex, topStopIndex, color) {
    this.name = name;
    this.leftStartIndex = leftStartIndex;
    this.leftStopIndex = leftStopIndex;
    this.topStartIndex = topStartIndex;
    this.topStopIndex = topStopIndex;
    this.color = color;
  }
}

module.exports = DataNodeDiagramGroup;
