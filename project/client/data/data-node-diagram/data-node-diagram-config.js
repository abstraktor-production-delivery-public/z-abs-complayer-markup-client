
'use strict';

const DataConfig = require('../data-config');

  
class DataNodeDiagramConfig extends DataConfig {
  constructor(parameters) {
    super();
    this.nodeWidth = 70;
    this.nodeHeight = 70;
    this.nodeWidthBetween = 25;
    this.nodeHeightBetween = 25;
    this.widthBias = 30;
    this.heightBias = 30;
    this.border = true;
    this.backgroundColor = 'default';
  }
}

module.exports = DataNodeDiagramConfig;
