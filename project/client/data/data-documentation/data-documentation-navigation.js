
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataDocumentationNavigation extends DataTableKeyValue {
  constructor(navigation, link) {
    super([], []);
    this.navigation = navigation;
    this.link = link;
  }
}

module.exports = DataDocumentationNavigation;
