
'use strict';


class DataConfig {
  addConfig(name, value) {
    const member = Reflect.get(this, name);
    if(undefined !== member) {
      Reflect.set(this, name, value);
    }
  }
}

module.exports = DataConfig;
