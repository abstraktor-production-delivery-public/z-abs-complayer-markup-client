
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesSrcGlobal extends DataTableKeyValue {
  constructor(addressName, interfaceName, sut, labId, userId, port, page, incognitoBrowser) {
    super();
    this.addressName = addressName;
    this.interfaceName = interfaceName;
    this.sut = sut;
    this.labId = labId;
    this.userId = userId;
    this.port = port;
    this.page = page;
    this.incognitoBrowser = incognitoBrowser;
  }
}

module.exports = DataAddressesSrcGlobal;
