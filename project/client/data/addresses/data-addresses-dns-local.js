
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesDnsLocal extends DataTableKeyValue {
  constructor(uri, interfaceName, sut, labId, userId) {
    super();
    this.uri = uri;
    this.interfaceName = interfaceName;
    this.sut = sut;
    this.labId = labId;
    this.userId = userId;
  }
}


module.exports = DataAddressesDnsLocal;
