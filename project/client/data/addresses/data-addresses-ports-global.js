
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesPortsGlobal extends DataTableKeyValue {
  constructor(portName, sut, labId, userId, port) {
    super();
    this.portName = portName;
    this.sut = sut;
    this.labId = labId;
    this.userId = userId;
    this.port = port;
  }
}

module.exports = DataAddressesPortsGlobal;
