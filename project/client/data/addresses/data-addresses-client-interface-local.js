
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesClientInterfaceLocal extends DataTableKeyValue {
  constructor(interfaceName, sut, type, networkName) {
    super();
    this.interfaceName = interfaceName;
    this.sut = sut;
    this.type = type;
    this.networkName = networkName;
  }
}

module.exports = DataAddressesClientInterfaceLocal;
