
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataSystemUnderTestInstances extends DataTableKeyValue {
  constructor(name, description) {
    super();
    this.name = name;
    this.description = description;
  }
}

module.exports = DataSystemUnderTestInstances;
