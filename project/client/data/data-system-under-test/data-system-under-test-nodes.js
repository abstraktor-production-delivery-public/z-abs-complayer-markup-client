
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataSystemUnderTestNodes extends DataTableKeyValue {
  constructor(name, instanceName, criteriaType, criteria, description) {
    super();
    this.name = name;
    this.instanceName = instanceName;
    this.criteriaType = criteriaType;
    this.criteria = criteria;
    this.description = description;
  }
}

module.exports = DataSystemUnderTestNodes;
