
'use strict';

import { DataActionStyleGet } from '../actions/data-action-style';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class StyleStore extends StoreBaseData {
  constructor() {
    super({
      style: new Map([
        ['th.style_store_login_display_parents', {display:'none'}],
        ['td.style_store_login_display_parents', {display:'none'}],
        ['th.style_store_login_display_dependencies', {display:'none'}],
        ['td.style_store_login_display_dependencies', {display:'none'}]
      ]),
      stackStyles: new Map()
    });
    this.styleSheet = null;
    this.protocolStyleId = -1;
  }
  
  onInit() {
    const css = document.createElement('style');
    css.type = 'text/css';
    document.getElementsByTagName("head")[0].appendChild(css);
    this.protocolStyleId = document.styleSheets.length - 1;
    this.sendDataAction(new DataActionStyleGet());
  }
  
  onActionStyleUpdate(action) {
    const cssRules = this.styleSheet.cssRules;
    for(let i = cssRules.length - 1; i > 0; --i) {
      if(action.selectorText === cssRules[i].selectorText) {
        action.onChange(cssRules[i].style);
        this.updateState({style: (style) => {
          style.set(action.selectorText, {display: cssRules[i].style.display});
        }});
        break;
      }
    }
  }
  
  onDataActionStyleGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const stackStyles = new Map(response.data);
      this.setStyleSheet(stackStyles, document.styleSheets[this.protocolStyleId]);
      this.updateState({stackStyles: {$set: stackStyles}});
    }
  }
  
  setStyleSheet(stackStyles, styleSheet) {
    this.styleSheet = styleSheet;
    styleSheet.insertRule(`rect.stack_info_dimond { stroke:black;stroke-width:0.25;fill:White; }`, styleSheet.cssRules.length);
    stackStyles.forEach((protocol, protocolKey) => {
      const classLine = `seq_dia_protocol_${protocolKey}`;
      styleSheet.insertRule(`line.${classLine} { stroke:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`line.${classLine}_msg { stroke:${protocol.protocolColor};stroke-width:2; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`line.${classLine}_pause { stroke:black;stroke-width:2; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`line.${classLine}_info { background-color:${protocol.protocolColor};stroke:${protocol.textColor};stroke-width:0.5; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`text.${classLine} { fill:${protocol.textColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`text.${classLine}_protocol { fill:${protocol.textProtocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`rect.${classLine}_empty { stroke-width:0;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`rect.${classLine}_small { stroke:black;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`rect.${classLine}_big { stroke:black;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_small { stroke:black;stroke-width:0.5;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_small_fail_outer { r:3;stroke:black;stroke-width:0.5;fill:red; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_small_fail_inner { r:1.7;stroke:white;stroke-width:0.8;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_big_fail_outer { r:3.7;stroke:black;stroke-width:0.5;fill:red; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_big_fail_inner { r:2.4;stroke:white;stroke-width:0.8;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_big { stroke:black;stroke-width:2;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`div.${classLine}_inner { border:solid 2px ${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`div.${classLine}_header { background-color:${protocol.protocolBackgroundColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`div.${classLine}_dimond { background-color:${protocol.protocolColor};color:${protocol.textColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`rect.${classLine}_dimond { stroke:black;stroke-width:0.25;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`polygon.${classLine}_arrow { stroke:grey;stroke-width:1;fill:${protocol.protocolBackgroundColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`polygon.${classLine}_small { stroke:black;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);     
      styleSheet.insertRule(`polygon.${classLine}_small_fail_outer { stroke:black;stroke-width:0.5;fill:red; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`polygon.${classLine}_small_fail_inner { stroke:white;stroke-width:0.8;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`polygon.${classLine}_big { stroke:black;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`span.${classLine}_buffer_toolbar { position:relative;color:Black;background-color:${protocol.protocolBackgroundColor};font-family:"Glyphicons Halflings";font-size: x-small;left:-5px;top:-6px; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`stop.${classLine} { stop-color:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
    });
    styleSheet.insertRule(`th.style_store_login_display_parents {display:none;}`, styleSheet.cssRules.length);
    styleSheet.insertRule(`td.style_store_login_display_parents {display:none;}`, styleSheet.cssRules.length);
    styleSheet.insertRule(`th.style_store_login_display_dependencies {display:none;}`, styleSheet.cssRules.length);
    styleSheet.insertRule(`td.style_store_login_display_dependencies {display:none;}`, styleSheet.cssRules.length);
  }
}


module.exports = StyleStore.export(StyleStore);
